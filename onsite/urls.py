from django.views.generic import RedirectView
from django.conf.urls import patterns, include, url
from django.views.generic.base import TemplateView
from django.conf import settings
from django.contrib import admin
import autocomplete_light
# import pk


autocomplete_light.autodiscover()
admin.autodiscover()

auth_patterns = patterns ('',
	url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'onsite/login.html'}, name='login'),
	url(r'^logout/$', 'django.contrib.auth.views.logout',  {'template_name': 'onsite/logout.html'}, name='logout'),
	url(r'^password_change/$', 'django.contrib.auth.views.password_change', name='password_change'),
	url(r'^password_change/done/$', 'django.contrib.auth.views.password_change_done', name='password_change_done'),
	url(r'^password_reset/$', 'django.contrib.auth.views.password_reset', name='password_reset'),
	url(r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
	url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
		'django.contrib.auth.views.password_reset_confirm',
		name='password_reset_confirm'),
	url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),
)

onsite_patterns = patterns('',
	url(r'^chongoikham/$', 'onsite.middleware.chon_goi_kham', name='chon_goi_kham_force'),
	url(r'^chongoikham/(?P<goi_kham_id>\d+)/$', 'onsite.middleware.chon_goi_kham', name='chon_goi_kham'),
	url(r'^online/$', 'onsite.middleware.online', name='online'),
	url(r'^khambenh_ajax/(?P<action>\w+)/$', 'onsite.views.kham_benh_ajax', name= 'kham_benh_ajax'),
	url(r'^benhly_ajax/(?P<kqm_id>\d+)/$', 'onsite.views.benh_ly_ajax'),
	url(r'^benhly_ajax/$', 'onsite.views.benh_ly_ajax', name='benh_ly_ajax'),
	url(r'^benhly/(?P<muc_kham_id>[\d,]+)/(?P<phan_loai>\d)/$', 'onsite.views.benh_ly', name='benh_ly'),
	url(r'^khambenh/(?P<muc_kham_id>[\d,]+)/$', 'onsite.views.kham_benh', name='kham_benh'),
	url(r'^thiluc/$', 'onsite.views.thi_luc', name='thi_luc'),
	url(r'^huyetap/$', 'onsite.views.huyet_ap', name='huyet_ap'),
	url(r'^cando/$', 'onsite.views.can_do', name='can_do'),
	url(r'^benhnhan_ajax/(?P<stt>\d+)/$', 'onsite.views.benh_nhan_ajax'),
	url(r'^benhnhan_ajax/$', 'onsite.views.benh_nhan_ajax', name='benh_nhan_ajax'),
	url(r'^muckham/$', 'onsite.views.muc_kham', name='muc_kham'),
	url(r'^muckham/(?P<muc_kham_id>\d+)/$', 'onsite.views.muc_kham', name='muc_kham'),
	url(r'^benhnhan/(?P<benh_nhan_id>\d+)/$', 'onsite.views.benh_nhan', name='benh_nhan'),
	url(r'^benhnhan/$', 'onsite.views.benh_nhan', name='benh_nhan'),
	# url(r'^$', RedirectView.as_view(url='/accounts/login'), name='home'),
	url(r'^$','onsite.views.redirect_muc_kham', name='home'),
	url(r'^ketqua/$', 'onsite.views.ket_qua', name='ket_qua'),
	url(r'^ketqua/(?P<benh_nhan_id>\d+)/$', 'onsite.views.ket_qua', name='ket_qua'),
	url(r'^huyketqua/(?P<benh_nhan_id>\d+)/(?P<muc_kham_id>\d+)/$', 'onsite.views.huy_ket_qua', name='huy_ket_qua'),
	url(r'^xuatexcel/$', 'onsite.views.xuat_excel', name='xuat_excel'),
	url(r'^test/$', TemplateView.as_view(template_name= 'onsite/test.html'), name='test'),
	url(r'^ketqua_ajax/$', 'onsite.views.ket_qua_ajax', name='ket_qua_ajax'),
	url(r'^kyketqua/$', 'onsite.views.ky_ket_qua', name='ky_ket_qua'),
	url(r'^ketqua_ajax/(?P<chi_tiet_cua_id>\d+)/$', 'onsite.views.ket_qua_ajax', name='ket_qua_ajax_with_id'),
)

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	url(r'^accounts/', include(auth_patterns)),
	url(r'^', include(onsite_patterns)),
	url(r'^bv/', include('bv.urls')),
	url(r'^ac/', include('autocomplete_light.urls')),
	url(r'^report_builder/', include('report_builder.urls')),
	url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
	url(r'^int/$', TemplateView.as_view(template_name= 'int/trangchu.html'), name='int_trang_chu'),
)

if settings.DEBUG :
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
	)


