# -*- coding: utf-8 -*-
from xlsxwriter.workbook import Workbook
from django.db.models import Q
from django.conf import settings
import os
from django.core.exceptions import ObjectDoesNotExist


pl_nang = {'nam':[50,46,42,40], 'nu':[47,43,41,39]}
pl_cao = {'nam': [162,159,156,154], 'nu':[153,151,149,147]}
pl_thiluc = [18,17,16,15]

tieu_hoa_range=(349,395)
ho_hap_range=(396,447)
tuan_hoan_range=(448, 511)
co_xuong_khop_range =(512,520)
noi_tiet_range=(521,530)	
than_tiet_nieu_range=(531,534)

def phanloai_cando(gioi_tinh,kg,cm):
	if gioi_tinh in ['nam','nu']:
		phan_loai = 1
		for x in xrange(0,4):
			if kg <= pl_nang[gioi_tinh][x] and x+2 > phan_loai:
				phan_loai = x+2
			if cm <= pl_cao[gioi_tinh][x] and x+2 > phan_loai:
				phan_loai = x+2
		return phan_loai
	else:
		return 0

def tinh_bmi(kg,cm):
	bmi = kg * 10000 / pow (cm,2)
	return int (bmi)
	''' TEST
from onsite import admin
admin.tinh_bmi(70,164)
	'''

def phanloai_thiluc(mat_trai, mat_phai):
	tong = mat_trai + mat_phai
	phan_loai = 1
	for x in xrange(0,4):
		if tong <= pl_thiluc[x]:
			phan_loai = x+2
	return phan_loai
	''' TEST
from onsite import admin
admin.phanloai_thiluc(9,9)
	'''

def phan_loai(ket_qua_list):
	pl_tong = 1
	for kq in ket_qua_list:
		if kq.phan_loai:
			try:
				pl_benhly = int(kq.phan_loai[:1])
				if pl_benhly > pl_tong:
					pl_tong = pl_benhly
			except ValueError:
				pass
	return pl_tong

def get_or_empty(ket_qua_list, **kwargs):
	try:
		o = ket_qua_list.filter(**kwargs)[:1].get()
		gia_tri = o.gia_tri_original()
		return gia_tri
	except ObjectDoesNotExist:
		return ''

def trans_none(val, default=''):
	return default if val is None else val

def stt(benh_nhan=None, ket_qua_list=None): return benh_nhan.stt
def ho_ten(benh_nhan=None, ket_qua_list=None):
	ret = benh_nhan.ho_ten 
	if benh_nhan.ma_nv: ret = '%s-%s' % (ret,benh_nhan.ma_nv)
	if benh_nhan.bo_phan: ret = '%s-%s' % (ret,benh_nhan.bo_phan)
	return ret
def gioi_tinh(benh_nhan=None, ket_qua_list=None): return trans_none(benh_nhan.gioi_tinh )
def ngay_sinh(benh_nhan=None, ket_qua_list=None): return trans_none(benh_nhan.ngay_sinh)
def cmnd(benh_nhan=None, ket_qua_list=None): return benh_nhan.cmnd
def bhyt(benh_nhan=None, ket_qua_list=None): return benh_nhan.bhyt 
def cao(benh_nhan=None, ket_qua_list=None): return get_or_empty(ket_qua_list, ket_qua_mau_id=997)
def can_nang(benh_nhan=None, ket_qua_list=None): return get_or_empty(ket_qua_list, ket_qua_mau_id=998)
def bmi(benh_nhan=None, ket_qua_list=None):
	kg = can_nang(benh_nhan, ket_qua_list)
	cm = cao(benh_nhan, ket_qua_list)
	if kg and cm:
		return tinh_bmi(kg, cm)
	else:
		return ''
def mach(benh_nhan=None, ket_qua_list=None): return  get_or_empty(ket_qua_list, ket_qua_mau_id=999)
def huyet_ap(benh_nhan=None, ket_qua_list=None):
	ha_duoi =  get_or_empty(ket_qua_list, ket_qua_mau_id=1000)
	ha_tren =  get_or_empty(ket_qua_list, ket_qua_mau_id=1001)
	return '%s/%s' % (ha_tren, ha_duoi)
def pl_the_luc(benh_nhan=None, ket_qua_list=None): 
	kg = can_nang(benh_nhan, ket_qua_list)
	cm = cao(benh_nhan, ket_qua_list)
	gt = gioi_tinh(benh_nhan, ket_qua_list)
	if kg and cm and gt:
		return phanloai_cando(gt, kg, cm)
	else:
		return 1
def tuan_hoan(benh_nhan=None, ket_qua_list=None): 
	l = ket_qua_list.filter(ket_qua_mau_id__range=tuan_hoan_range)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_tuan_hoan(benh_nhan=None, ket_qua_list=None): 
	l = ket_qua_list.filter(ket_qua_mau_id__range=tuan_hoan_range)
	return phan_loai(l)
def ho_hap(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(ket_qua_mau_id__range=ho_hap_range)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_ho_hap(benh_nhan=None, ket_qua_list=None): 
	l = ket_qua_list.filter(ket_qua_mau_id__range=ho_hap_range)
	return phan_loai(l)
def tieu_hoa(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(ket_qua_mau_id__range=tieu_hoa_range)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_tieu_hoa(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(ket_qua_mau_id__range=tieu_hoa_range)
	return phan_loai(l)
def than_tiet_nieu(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(ket_qua_mau_id__range=than_tiet_nieu_range)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_than_tiet_nieu(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(ket_qua_mau_id__range=than_tiet_nieu_range)
	return phan_loai(l)
def noi_tiet(benh_nhan=None, ket_qua_list=None): 
	l = ket_qua_list.filter(ket_qua_mau_id__range=noi_tiet_range)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_noi_tiet(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(ket_qua_mau_id__range=noi_tiet_range)
	return phan_loai(l)
def co_xuong_khop(benh_nhan=None, ket_qua_list=None):
	l = ket_qua_list.filter(ket_qua_mau_id__range=co_xuong_khop_range)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_co_xuong_khop(benh_nhan=None, ket_qua_list=None): 
	l = ket_qua_list.filter(ket_qua_mau_id__range=co_xuong_khop_range)
	return phan_loai(l)
def noi_khoa_khac(benh_nhan=None, ket_qua_list=None):
	l = ket_qua_list.filter(Q(muc_kham_id=6), 
		~Q(ket_qua_mau_id__range=tieu_hoa_range), ~Q(ket_qua_mau_id__range=ho_hap_range), ~Q(ket_qua_mau_id__range=tuan_hoan_range), \
		~Q(ket_qua_mau_id__range=co_xuong_khop_range), ~Q(ket_qua_mau_id__range=noi_tiet_range), ~Q(ket_qua_mau_id__range=than_tiet_nieu_range), \
	)
	return ', '.join([o.get_full_name() for o in l])
def pl_noi_khoa_khac(benh_nhan=None, ket_qua_list=None):
	l = ket_qua_list.filter(Q(muc_kham_id=6), 
		~Q(ket_qua_mau_id__range=tieu_hoa_range), ~Q(ket_qua_mau_id__range=ho_hap_range), ~Q(ket_qua_mau_id__range=tuan_hoan_range), \
		~Q(ket_qua_mau_id__range=co_xuong_khop_range), ~Q(ket_qua_mau_id__range=noi_tiet_range), ~Q(ket_qua_mau_id__range=than_tiet_nieu_range), \
	)
	return phan_loai(l) if l else ''
def ngoai_khoa(benh_nhan=None, ket_qua_list=None):
	l = ket_qua_list.filter(muc_kham_id=8)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_ngoai_khoa(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(muc_kham_id=8)
	return phan_loai(l)
def phu_khoa(benh_nhan=None, ket_qua_list=None):
	l = ket_qua_list.filter(muc_kham_id=9)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_phu_khoa(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(muc_kham_id=9)
	return phan_loai(l)
def khong_kinh_mat_phai(benh_nhan=None, ket_qua_list=None): return get_or_empty(ket_qua_list, ket_qua_mau_id=2)
def khong_kinh_mat_trai(benh_nhan=None, ket_qua_list=None): return get_or_empty(ket_qua_list, ket_qua_mau_id=3)
def co_kinh_mat_phai(benh_nhan=None, ket_qua_list=None): return get_or_empty(ket_qua_list, ket_qua_mau_id=1011)
def co_kinh_mat_trai(benh_nhan=None, ket_qua_list=None): return get_or_empty(ket_qua_list, ket_qua_mau_id=1012)
def mat(benh_nhan=None, ket_qua_list=None):
	l = ket_qua_list.filter(muc_kham_id=2)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_mat(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(muc_kham_id=2)
	return phan_loai(l)
def tai_mui_hong(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(muc_kham_id=4)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_tai_mui_hong(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(muc_kham_id=4)
	return phan_loai(l)
def rang_ham_mat(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(muc_kham_id=3)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_rang_ham_mat(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(muc_kham_id=3)
	return phan_loai(l)
def da_lieu(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(muc_kham_id=7)
	return ', '.join([o.get_full_name() for o in l]) if l else u'Bình thường'
def pl_da_lieu(benh_nhan=None, ket_qua_list=None):  
	l = ket_qua_list.filter(muc_kham_id=7)
	return phan_loai(l)
def ket_luan(benh_nhan=None, ket_qua_list=None):  
	return benh_nhan.ket_luan
def pl_ket_luan(benh_nhan=None, ket_qua_list=None):  
	return benh_nhan.phan_loai

cot_list = [
	stt,
	ho_ten,
	gioi_tinh,
	ngay_sinh,
	cmnd,
	bhyt,
	cao,
	can_nang,
	bmi,
	mach,
	huyet_ap,
	pl_the_luc,
	tuan_hoan,
	pl_tuan_hoan,
	ho_hap,
	pl_ho_hap,
	tieu_hoa,
	pl_tieu_hoa,
	than_tiet_nieu,
	pl_than_tiet_nieu,
	noi_tiet,
	pl_noi_tiet,
	co_xuong_khop,
	pl_co_xuong_khop,
	noi_khoa_khac,
	pl_noi_khoa_khac,
	ngoai_khoa,
	pl_ngoai_khoa,
	phu_khoa,
	pl_phu_khoa,
	khong_kinh_mat_phai,
	khong_kinh_mat_trai,
	co_kinh_mat_phai,
	co_kinh_mat_trai,
	mat,
	pl_mat,
	tai_mui_hong,
	pl_tai_mui_hong,
	rang_ham_mat,
	pl_rang_ham_mat,
	da_lieu,
	pl_da_lieu,
	ket_luan,
	pl_ket_luan,
]

def export(benh_nhan_list, file_name=None):
	workbook = Workbook( os.path.join (settings.MEDIA_ROOT, file_name if file_name else "ket_qua.xlsx" ) )
	sheet1 = workbook.add_worksheet(u'ketquakham')
	l = len(cot_list)
	for x in xrange( 1,l+1 ):
		sheet1.write(0, x-1, cot_list[x-1].__name__)
	for y in xrange( 1, len(benh_nhan_list) + 1 ):
		benh_nhan = benh_nhan_list[y-1]
		ket_qua_list = benh_nhan.cac_ket_qua.filter(huy=False)
		for x in xrange( 1,l+1 ):
			v = cot_list[x-1]( benh_nhan, ket_qua_list )
			sheet1.write(y, x-1, v if type(v)==unicode or type(v)==str else str(v))
	workbook.close()

from onsite.models import BenhNhan
from django.db.models import Count
def test():
	bn = BenhNhan.objects.annotate(dk = Count('da_kham')).filter(dk__gt=1)
	export(bn)
""" TEST
from onsite.excel import test
test()

or 

from onsite.models import BenhNhan
from django.db.models import Count
bn = BenhNhan.objects.annotate(dk = Count('da_kham')).filter(dk__gt=1)
len(bn)

from onsite.models import BenhNhan
may10=BenhNhan.objects.filter(stt__gt=1000)
from onsite import excel
excel.export(may10,'may10.xlsx')

from onsite.models import BenhNhan
chantelle=BenhNhan.objects.filter(stt__lt=1000)
from onsite import excel
excel.export(chantelle,'chantelle.xlsx')

"""
