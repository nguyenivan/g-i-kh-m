# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponseServerError, HttpResponse
from onsite.models import MucKham, BenhNhan, KetQuaMau, KetQua, GoiKham
from django.core.urlresolvers import reverse
from django.shortcuts import render
import json, os
from django.conf import settings
from onsite.forms import BenhNhanForm
from django.views.generic.edit import FormView
from datetime import datetime, time, date
import re 
from django.db.models import Q
import traceback
from django.views.decorators.http import condition
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from django.template import Context, Template
from int.models import HuyetHoc

def format_date(d):
	if d is None:
		return '-'
	elif type(d) ==  date:
		return d.strftime("%d-%m-%Y")
	else:
		return u'data error'

def check_workflow(muc_kham_id_list,da_kham_id_list, quytrinh):
	chua_kham = []
	for mk_id in quytrinh:
		if mk_id in muc_kham_id_list:
			break
		if mk_id not in da_kham_id_list:
			chua_kham.append(mk_id)
	return chua_kham

@login_required
def redirect_muc_kham(request):
	groups = request.user.groups.all()
	mk_list = MucKham.objects.filter(nhom__in = groups)
	can_do = MucKham.objects.get(pk = 1)
	huyet_ap = MucKham.objects.get(pk = 10)
	thi_luc = MucKham.objects.get(pk = 11)
	if can_do in mk_list:
		return HttpResponseRedirect( reverse('can_do') )
	if huyet_ap in mk_list:
		return HttpResponseRedirect( reverse('huyet_ap') )
	if thi_luc in mk_list:
		return HttpResponseRedirect( reverse('thi_luc') )
	muc_kham_id = ','.join([ str(mk.pk) for mk in mk_list ])
	return HttpResponseRedirect( reverse('kham_benh', kwargs={'muc_kham_id':muc_kham_id} ) )

@login_required
def benh_nhan_ajax(request, stt = None):
	if request.method == "GET":
		try:
			muc_kham_list = request.session.get('muc_kham')
			muc_kham_id_list = [mk.id for mk in muc_kham_list]
			goikham = request.session.get('goikham')
			bn = cache.get('benh_nhan_%s_%s' % (stt, goikham.pk) )
			if bn is None:
				bn = BenhNhan.objects.select_related().get(stt=stt, goi_kham=goikham)
				cache.set('benh_nhan_%s_%s' % (stt, goikham.pk) , bn)
			request.session['dang_kham'] = bn
			da_kham_id_list =[mk.id for mk in bn.da_kham.all()]
			if set(da_kham_id_list) & set(muc_kham_id_list):
				message = u' <span style="color:red; font-weight:bold;">ĐÃ KHÁM:</span>'
				kq_list = bn.cac_ket_qua.filter(muc_kham_id__in=muc_kham_id_list, huy=False)
				#Full Name should be denormalized to get faster result
				message = message + ' / '.join( [kq.get_full_name() for kq in kq_list ] )
				out_list = [bn.ho_ten]
				if bn.gioi_tinh: out_list.append(bn.gioi_tinh)
				if bn.ngay_sinh: out_list.append(format_date(bn.ngay_sinh))
				if bn.bo_phan: out_list.append(bn.bo_phan)
				data = {
					'success': False,
					'data': ' / '.join(out_list) + message,
				}
			else: #BN chua kham muc nay
				message = ''
				(kham_mat, noi_khoa, huyet_ap, thi_luc) = (2, 6, 10, 11)
				# (kham_mat, noi_khoa, huyet_ap, thi_luc) = MucKham.objects.filter(pk__in =[2, 6, 10, 11]).order_by('pk')
				if noi_khoa in muc_kham_id_list and huyet_ap in da_kham_id_list:
					kq_list = KetQua.objects.filter( muc_kham_id=huyet_ap, huy=False, benh_nhan=bn)
					message = ' ' + ' / '.join( [kq.ket_qua + ' ' + str(kq.gia_tri()) for kq in kq_list ] )
				if kham_mat in muc_kham_id_list and thi_luc in da_kham_id_list:
					# Display thi_luc thi luc
					kq_list = KetQua.objects.filter( muc_kham_id=thi_luc, huy=False, benh_nhan_id=bn.pk)
					message = ' '+ ' / '.join( [kq.get_full_name() for kq in kq_list ] )
				cached_list = request.session.get('benh_ly')
				ket_qua_khac = request.session.get('ket_qua_khac')
				object_list = KetQuaMau.objects.filter(pk__in = cached_list) if cached_list else []
				message = message + "<p>%s</p>"  % ' / '.join([kqm.get_full_name() for kqm in object_list])
				if ket_qua_khac:
					message = message + "<p>%s PL %s</p>"  % (ket_qua_khac[1], ket_qua_khac[0])
				out_list = [bn.ho_ten]
				if bn.gioi_tinh: out_list.append(bn.gioi_tinh)
				if bn.ngay_sinh: out_list.append(format_date(bn.ngay_sinh))
				if bn.bo_phan: out_list.append(bn.bo_phan)
				data = {
					'success': True,
					'data': ' / '.join(out_list) + message,
				}
			
			# Kiểm tra quy trình
			quytrinh = request.session.get('quytrinh')
			muckham = request.session.get('muckham')
			chua_kham = check_workflow(muc_kham_id_list, da_kham_id_list, quytrinh)
			if chua_kham:
				data['data'] = u' <span style="color:red; font-weight:bold;">CHƯA KHÁM: %s</span> %s' % \
				(', '.join([muckham.get(mk_id).__unicode__() for mk_id in chua_kham]), data['data'])
		except ObjectDoesNotExist:
			data = {
				'success': False,
				'data': u'STT Bệnh Nhân không đúng.',
			}
		except Exception, e:
			print traceback.format_exc()
			data = {
				'success': False,
				'data': unicode(e),
			}
	json_string = json.dumps(data)
	return HttpResponse(json_string, mimetype="application/json")


@login_required
def benh_ly_ajax(request, kqm_id = None):
	if request.session.get('dang_kham') is None:
		data = {
			'success': False,
			'data': u'Chưa chọn bệnh nhân',
		}
	else: #Da co benh nhan
		if kqm_id is not None:
			cached_list = request.session.get('benh_ly')
			if cached_list is None: cached_list = []
			# Co the optimize cache modified here
			if not kqm_id in cached_list:
				cached_list.append(kqm_id)
				request.session['benh_ly'] = cached_list
			data = {
				'success': True,
			}
		else: #Ket qua khac
			phan_loai = request.REQUEST.get('phan_loai')
			ket_qua = request.REQUEST.get('ket_qua')
			muc_kham = int( request.REQUEST.get('muc_kham') )
			request.session['ket_qua_khac'] = (phan_loai, ket_qua, muc_kham)
			data = {
				'success': True,
			}
	json_string = json.dumps(data)
	return HttpResponse(json_string, mimetype="application/json")

@login_required
def can_do(request):
	if request.method == "GET":
		request.session['muc_kham'] = [MucKham.objects.get(pk=1)]
		return render(request, 'onsite/cando.html')
	else:
		stt = request.REQUEST.get('stt')
		val_list = ( int (request.REQUEST.get('chieucao') ), int( request.REQUEST.get('cannang') ) )
		try:
			bn = request.session.get('dang_kham')
			mk = request.session.get('muc_kham')
			kqmau_list  = KetQuaMau.objects.select_related('muc_kham').filter(pk__in=[997,998]).order_by('pk')
			x = 0
			for kqm in kqmau_list: #Evaluate the queryset
				KetQua.objects.create(benh_nhan = bn, ket_qua = kqm.ket_qua, muc_kham = kqm.muc_kham, \
					phan_loai = kqm.phan_loai, loai_ket_qua = kqm.loai_ket_qua, ket_qua_mau = kqm, \
					ket_qua_so_nguyen = val_list[x], bac_sy = request.user, slug = kqm.slug)
				x =x+1

			bn.da_kham.add(*mk)
			data = {
				'success': True,
				'data': u'GHI THÀNH CÔNG. ' +  ' / '.join((bn.ho_ten, bn.gioi_tinh, format_date(bn.ngay_sinh), bn.bo_phan, \
					u'Chiều cao: %s' % val_list[0], u'Cân nặng: %s' % val_list[1])),
			}

			# TEST
			if settings.BATCH_TEST:
				hit=request.session.get('hit')
				if hit is None:
					hit=len(request.user.ketqua_set.all())
					request.session['hit'] = hit
				request.session['hit'] = hit + 1
				data['data'] = '%s - %s/300' % (data['data'], hit)
			# END TEST
			
		except Exception, e:
			print traceback.format_exc()
			data = {
				'success': False,
				'data': str(e),
			}
		json_string = json.dumps(data)	
		return HttpResponse(json_string, mimetype="application/json")

@login_required
def huyet_ap(request):
	if request.method == "GET":
		request.session['muc_kham'] = [MucKham.objects.get(pk=10)]
		return render(request, 'onsite/huyetap.html')
	else:
		stt = request.REQUEST.get('stt')
		val_list = ( 
				int( request.REQUEST.get('machdap') ),
				int (request.REQUEST.get('huyetapduoi') ), 
				int( request.REQUEST.get('huyetaptren') ),
		)
		try:
			bn = request.session.get('dang_kham')
			mk = request.session.get('muc_kham')
			kqmau_list  = KetQuaMau.objects.select_related('muc_kham').filter(pk__in=[999, 1000, 1001]).order_by('pk')
			x = 0
			for kqm in kqmau_list: #Evaluate the queryset
				KetQua.objects.create(benh_nhan = bn, ket_qua = kqm.ket_qua, muc_kham = kqm.muc_kham, \
					phan_loai = kqm.phan_loai, loai_ket_qua = kqm.loai_ket_qua, ket_qua_mau = kqm, \
					ket_qua_so_nguyen = val_list[x], bac_sy = request.user, slug = kqm.slug)
				x =x+1

			bn.da_kham.add(*mk)

			data = {
				'success': True,
				'data': u'GHI THÀNH CÔNG. ' +  ' / '.join((bn.ho_ten, bn.gioi_tinh, format_date(bn.ngay_sinh), bn.bo_phan, \
					u'Huyết áp: %s - %s, Mạch đập %s' % (val_list[2], val_list[1], val_list[0]) )),
			}
			# TEST
			if settings.BATCH_TEST:
				hit=request.session.get('hit')
				if hit is None:
					hit=len(request.user.ketqua_set.all())
					request.session['hit'] = hit
				request.session['hit'] = hit + 1
				data['data'] = '%s - %s/300' % (data['data'], hit)
			# END TEST
		except Exception, e:
			print traceback.format_exc()
			data = {
				'success': False,
				'data': str(e),
			}
		json_string = json.dumps(data)
		return HttpResponse(json_string, mimetype="application/json")

	
@login_required
def thi_luc(request):
	if request.method == "GET":
		request.session['muc_kham'] = [MucKham.objects.get(pk=11)]
		return render(request, 'onsite/thiluc.html')
	else:
		stt = request.REQUEST.get('stt')
		val_list =  [int (request.REQUEST.get('mattrai') ), int( request.REQUEST.get('matphai') )]
		cokinh = request.REQUEST.get('mattraicokinh')  and request.REQUEST.get('matphaicokinh')
		if cokinh:
			val_list.extend( [int(request.REQUEST.get('mattraicokinh')) , int (request.REQUEST.get('matphaicokinh'))] )
		try:
			bn = request.session.get('dang_kham')
			mk = request.session.get('muc_kham')
			kqmau_list  = KetQuaMau.objects.select_related('muc_kham').filter(pk__in = \
				[2,3,1011,1012] if cokinh else [2,3]).order_by('pk')
			x = 0
			for kqm in kqmau_list: #Evaluate the queryset
				KetQua.objects.create(benh_nhan = bn, ket_qua = kqm.ket_qua, muc_kham = kqm.muc_kham, \
					phan_loai = kqm.phan_loai, loai_ket_qua = kqm.loai_ket_qua, ket_qua_mau = kqm, \
					ket_qua_so_nguyen = val_list[x], bac_sy = request.user, slug = kqm.slug)
				x =x+1

			bn.da_kham.add(*mk)

			message = ' / '.join((u'Mắt trái: %s' % val_list[0], u'Mắt phải: %s' % val_list[1] ))
			if cokinh:
				message = ' / '.join((message, u'Trái Có Kính: %s' % val_list[2], u'Phải Có Kính: %s' % val_list[3] ))
			message = ' / '.join((bn.ho_ten, bn.gioi_tinh, format_date(bn.ngay_sinh), bn.bo_phan, message ) )
			data = {
				'success': True,
				'data': u'GHI THÀNH CÔNG. ' +  message,
			}
			# TEST
			if settings.BATCH_TEST:
				hit=request.session.get('hit')
				if hit is None:
					hit=len(request.user.ketqua_set.all())
					request.session['hit'] = hit
				request.session['hit'] = hit + 1
				data['data'] = '%s - %s/300' % (data['data'], hit)
			# END TEST
		except Exception, e:
			print traceback.format_exc()
			data = {
				'success': False,
				'data': str(e),
			}
		json_string = json.dumps(data)
		return HttpResponse(json_string, mimetype="application/json")

def first_view(request, muc_kham_id):
	return muc_kham_id

@login_required
# @condition(etag_func=first_view)
def kham_benh(request, muc_kham_id):
	mk_list = muc_kham_id.split(',')
	request.session['muc_kham'] = [MucKham.objects.get(pk=int(mk)) for mk in mk_list]
	chi_tiet = []
	for x in xrange(2,6):
		chi_tiet.append( (x, reverse('benh_ly', kwargs ={ "muc_kham_id":muc_kham_id, "phan_loai" : x})) )
	dang_kham = request.session.get('dang_kham')
	cached_list = request.session.get('benh_ly')
	object_list = KetQuaMau.objects.filter(pk__in = cached_list) if cached_list else []
	return render(request, 'onsite/khambenh.html', {'chi_tiet': chi_tiet, 'object_list': object_list})

@login_required
def kham_benh_ajax(request, action):
	try:
		if action == "xoa":
			bn = request.session.get('dang_kham')
			muc_kham = request.session.get('muc_kham')
			if bn:
				bn.da_kham.remove(*muc_kham)
				bn.cac_ket_qua.filter(muc_kham__in = muc_kham, huy=False).update(huy=True)
			request.session['dang_kham'] = None
			request.session['benh_ly'] = None
			request.session['ket_qua_khac'] = None

			data = {
				'success': True,
				'data': u'Đã xoá kết quả'
			}
		if action == "ghi":
			bn = request.session.get('dang_kham')
			cached_list = request.session.get('benh_ly')
			muc_kham = request.session.get('muc_kham')
			object_list = KetQuaMau.objects.filter(pk__in = cached_list) if cached_list else []
			data = []

			for kqm in object_list:

				#TEST
				if settings.BATCH_TEST:
					hit_kqm = request.session.get('hit_%s' % kqm.pk)
					if hit_kqm is None:
						hit_kqm = len(request.user.ketqua_set.filter( \
							ket_qua_mau=kqm))
						request.session['hit_%s' % kqm.pk] = hit_kqm
					if hit_kqm > 5:
						data = {
							'success': False,
							'data': u'<span style="color:red; font-weight:bold;"> \
								LỖI: kết quả %s lặp lại quá 5 lần!</span>' % kqm,
						}
						json_string = json.dumps(data)
						return HttpResponse(json_string, mimetype="application/json")
					request.session['hit_%s' % kqm.pk] = hit_kqm + 1
				#END TEST

				kq = KetQua(benh_nhan = bn, ket_qua_mau = kqm )
				kq.bac_sy = request.user
				kq.save()
				if kq.phan_loai:
					data.extend([ '%s PL %s' % (kq.full_name, kq.phan_loai) ])
				else:
					data.extend([kq.full_name])
			if (request.session.get('ket_qua_khac')):
				kq = KetQua()
				kq.benh_nhan = bn
				kq.muc_kham = MucKham.objects.get(pk=request.session.get('ket_qua_khac')[2])
				kq.bac_sy = request.user
				kq.phan_loai = request.session.get('ket_qua_khac')[0]
				kq.ket_qua = request.session.get('ket_qua_khac')[1]
				kq.loai_ket_qua = 'ky_tu'
				kq.save()
				data.extend([ '%s PL %s' % (kq.ket_qua, kq.phan_loai) ])
			bn.da_kham.add(*muc_kham)
			request.session['dang_kham'] = None
			request.session['benh_ly'] = None
			request.session['ket_qua_khac'] = None
			data = {
				'success': True,
				'data': u'Đã ghi kết quả: %s cho bệnh nhân %s' % (' / '.join(data), bn.ho_ten),
			}

			# TEST
			if settings.BATCH_TEST:
				if cached_list:
					hit=request.session.get('hit')
					if hit is None:
						hit=len(request.user.ketqua_set.all())
						request.session['hit'] = hit
					request.session['hit'] = hit + 1
					data['data'] = '%s - %s/300' % (data['data'], hit)
			# END TEST

		if action == "doc":
			bn = request.session.get('dang_kham')
			message = ''
			if bn:
				cached_list = request.session.get('benh_ly')
				ket_qua_khac = request.session.get('ket_qua_khac')
				object_list = KetQuaMau.objects.filter(pk__in = cached_list) if cached_list else []
				message = "<p>%s</p>"  % ' / '.join((bn.ho_ten, bn.gioi_tinh, format_date(bn.ngay_sinh), bn.bo_phan))
				message = message + "<p>%s</p>"  % ' / '.join([kqm.get_full_name() for kqm in object_list])
				if ket_qua_khac:
					message = message + "<p>%s PL %s</p>"  % (ket_qua_khac[1], ket_qua_khac[0])
			data = {
				'success': True,
				'data': message,
			}

	except Exception, e:
		print traceback.format_exc()
		data = {
			'success': False,
			'data': str(e),
		}
	json_string = json.dumps(data)
	return HttpResponse(json_string, mimetype="application/json")

def benh_ly_latest(request, muc_kham_id, phan_loai):
	# return datetime.strptime("20000101", "%Y%m%d")
	mk_list = muc_kham_id.split(',')
	return KetQuaMau.objects.filter(muc_kham__pk__in = mk_list).latest("cap_nhat").cap_nhat

@login_required
@condition(last_modified_func=benh_ly_latest)
def benh_ly(request, muc_kham_id, phan_loai):
	mk_list = muc_kham_id.split(',')
	object_list = KetQuaMau.objects.filter( Q(muc_kham__pk__in = mk_list), \
		Q(phan_loai__startswith=str(phan_loai)) )
	title = u'Phân Loại %s' % (phan_loai)
	back_url = reverse('kham_benh', kwargs={'muc_kham_id': muc_kham_id})
	return render(request, 'onsite/benhly.html', {'object_list': object_list, \
		'muc_kham_id':muc_kham_id, 'muc_kham_list': mk_list, 'phan_loai': phan_loai, 'title': title})

@login_required
def muc_kham(request, muc_kham_id=None):
	if (muc_kham_id is None):
		group_list = request.user.groups.all()
		muckham_list=MucKham.objects.filter(nhom__in=group_list)
		return render(request, 'onsite/muckham.html', {'object_list' : muckham_list})
	else:
		request.session['muc_kham'] = MucKham.objects.get(pk=muc_kham_id)
		return HttpResponseRedirect(reverse('ket_qua'))

@login_required
def benh_nhan(request, benh_nhan_id=None):
	goikham = request.session.get('goikham')
	if (benh_nhan_id is None):
		object_list = None
		if request.method == 'POST': # If the form has been submitted...
			form = BenhNhanForm(request.POST) # A form bound to the POST data
			if form.is_valid():
				stt = form.cleaned_data['stt']
				if (stt is not None):
					object_list = BenhNhan.objects.filter(stt=stt, goi_kham=goikham)
				else:
					keyword = form.cleaned_data['ten']
					keyword = '+' + ' +'.join(keyword.split())
					object_list = BenhNhan.objects.search( '*%s*' % (keyword,), 'ho_ten' )[:10]
		else:
			form = BenhNhanForm()

		return render(request, 'onsite/benhnhan.html', 
			{'form': form, 'submit': u'Tìm Bệnh Nhân', 'object_list': object_list})
	else:
		request.session['benh_nhan'] = BenhNhan.objects.get(pk=benh_nhan_id, goikham=goikham)
		return HttpResponseRedirect(reverse('ket_qua'))

@login_required
def ky_ket_qua(request):
	benh_nhan_cached = request.session.get('benh_nhan')
	muc_kham_cached = request.session.get('muc_kham')
	if (muc_kham_cached is None or benh_nhan_cached is None):
		return HttpResponseServerError(u'<h1>Yêu cầu xác nhận không hợp lệ</h1>')
	if request.method == 'POST':
		object_id_list = json.loads(request.REQUEST.get('object_id_list'))
		object_list = KetQua.objects.filter (pk__in = object_id_list)
		for ketqua in object_list:
			ketqua.huy = False
			ketqua.save()
		benh_nhan_cached.da_kham.add(muc_kham_cached)
		return HttpResponseRedirect(reverse('ket_qua', kwargs={'benh_nhan_id': benh_nhan_cached.pk}))
	else:
		return HttpResponseServerError(u'<h1>Yêu cầu xác nhận không hợp lệ</h1>')

@login_required
def ket_qua(request, benh_nhan_id = None):
	benh_nhan_cached = request.session.get('benh_nhan')
	muc_kham_cached = request.session.get('muc_kham')
	if (muc_kham_cached is None):
		return HttpResponseRedirect(reverse('muc_kham'))
	if (benh_nhan_cached is None):
		return HttpResponseRedirect(reverse('benh_nhan'))	

	if (benh_nhan_id is None):
		if (request.method == 'POST'):
			# Danh sach ID cua cac ket
			object_id_list = [] 
			object_list = []
			for key in request.REQUEST:
				pk = None
				try:
					pk = int(key)
				except ValueError:
					pass
				else:
					if request.REQUEST.get(key).strip():
						val = request.REQUEST.get(key).strip()
						kqm = KetQuaMau.objects.get(pk=pk)
						# Tao ket qua tam thoi (huy=True)
						kq = KetQua(benh_nhan = benh_nhan_cached, ket_qua_mau = kqm, huy=True)
						if kq.loai_ket_qua == u'so_nguyen':
							kq.ket_qua_so_nguyen = int(val)
						elif kq.loai_ket_qua == u'so_thap_phan':
							kq.ket_qua_so_thap_phan = float(val)
						elif kq.loai_ket_qua == u'ky_tu':
							kq.ket_qua_ky_tu = val
							kq.phan_loai = request.REQUEST.get ('phan_loai_%s' % key)
						kq.save()
						kq.bac_sy= request.user
						object_id_list.append(kq.pk)
						object_list.append(kq)
			# benh_nhan_cached.da_kham.add(muc_kham_cached)
			benh_nhan_cached.save()
			return render(request, 'onsite/kyketqua.html', 
				{'object_id_list' : json.dumps(object_id_list), 'object_list': object_list})
		elif benh_nhan_cached.da_kham.filter(pk=muc_kham_cached.pk):
			return HttpResponseRedirect(reverse('ket_qua', kwargs = {'benh_nhan_id' : benh_nhan_cached.pk}))
		object_list = KetQuaMau.get_root_by_muc_kham_id(muc_kham_cached.pk)
		return render(request, 'onsite/ketqua.html', {'object_list': object_list})
	elif request.method == 'POST':
			return HttpResponseRedirect(reverse('ket_qua'))
	elif benh_nhan_cached.da_kham.filter(pk = muc_kham_cached.pk):
			kq_list = KetQua.objects.filter(benh_nhan = benh_nhan_cached, huy = False)
			return render(request,'onsite/ketqua_benhnhan.html', {'object_list': kq_list, 
					'benh_nhan_id': benh_nhan_id, 'co_ket_qua': True, 'muc_kham_id' : muc_kham_cached.pk })
	else:
		return HttpResponseRedirect(reverse('ket_qua'))

@login_required
def huy_ket_qua(request, benh_nhan_id, muc_kham_id):	
	ketqua_list = KetQua.objects.filter(benh_nhan__pk = benh_nhan_id)
	for ketqua in ketqua_list:
		ketqua.huy = True
		ketqua.save()
	bn = BenhNhan.objects.get(pk = benh_nhan_id)
	mk = MucKham.objects.get(pk=muc_kham_id)
	bn.da_kham.remove(mk)
	return HttpResponseRedirect(reverse('benh_nhan'))

@login_required
def xuat_excel(request):
	if request.REQUEST.get('object_id_list'):
		object_id_list = json.loads(request.REQUEST.get('object_id_list'))
		object_list = BenhNhan.objects.filter (pk__in = object_id_list)
		tmp = 'ket_qua.xlsx'
		BenhNhan.export_excel_list(object_list, os.path.join(settings.MEDIA_ROOT, tmp) )
		return HttpResponseRedirect(os.path.join(settings.MEDIA_URL,tmp))
	else:
		return HttpResponseRedirect(reverse('home'))

@login_required
def ket_qua_ajax(request, chi_tiet_cua_id=None):
	object_list = KetQuaMau.objects.filter(chi_tiet_cua__pk = int(chi_tiet_cua_id))
	return render(request, 'onsite/ketqua_ajax.html', {'object_list': object_list})
