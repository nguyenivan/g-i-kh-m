# -*- coding: utf-8 -*-
from django.db import models
from django.forms.models import model_to_dict
from django.db.models import Q
from django.contrib.auth.models import User, Group
from mysqlfulltextsearch.search_manager import SearchManager
from django.utils.translation import ugettext_lazy as _
import json
from django.conf import settings
from xlsxwriter.workbook import Workbook
# from onsite.utils import remove_accent
from django.utils.text import slugify
import datetime, os, re, traceback
from pytz import timezone
from django.core.cache import cache

#MUC_CHI_TIET_CHOICES = (
#		(1, _(u'Mức 1')),
#		(2, _(u'Mức 2')),
#)
TZ = timezone(settings.TIME_ZONE)

LKQ_CHOICES = (
	(u'danh_sach', _(u'Danh Sách')),
	(u'so_thap_phan', _(u'Số Thập Phân (10.05)')),
	(u'so_nguyen', _(u'Số Nguyên (1234)')),
	(u'ky_tu', _(u'Ký Tự (AaBbCc)')),
)
SEX_CHOICES = (
	(u'nam', _(u'Nam')),
	(u'nu', _(u'Nữ')),
)
HON_NHAN_CHOICES = (
	(u'dt', _(u'Độc Thân')),
	(u'gd', _(u'Có GĐ')),
	(u'ld', _(u'Ly Dị')),
)
SEX_CHOICES_DICT = {
	u'nam' : _(u'Nam'),
	u'nu' : _(u'Nữ'),
}

class MucKham(models.Model):
	class Meta:
		verbose_name = _(u'mục khám')
		verbose_name_plural = _(u'danh sách mục khám')

	def __unicode__(self):
		return self.ten
	
	stt = models.IntegerField (verbose_name=u'số thứ tự', blank=True, null=True, default=0, )
	ten = models.CharField (max_length = 50, verbose_name=u'tên')
	nhom = models.OneToOneField (Group, verbose_name=u'nhóm', blank = True, null = True)
	kich_hoat = models.BooleanField (verbose_name=u'kích hoạt', default=True)

"""
class LoaiKetQua(models.Model):
	
	class Meta:
		verbose_name = _(u'loại kết quả')
		verbose_name_plural = _(u'Các Loại Kết Quả')

	def __unicode__(self):
		return self.loai

	loai =  models.CharField (verbose_name=u'loại', max_length=20, primary_key=True, choices=LKQ_CHOICES)
"""
#from django.core.exceptions import ValidationError

class KetQuaMau(models.Model):
	class Meta:
		verbose_name = _(u'kết quả mẫu')
		verbose_name_plural = _(u'danh sách các kết quả mẫu')

	def __unicode__(self):
		return ', '.join( [self.muc_kham.ten , self.full_name] )

	def get_family_string (self):
		o = self
		family = u'<h3  style="font-size:14px;">%s</h3>' % (o.ket_qua,)
		for x in xrange(1,4): #to prevent infinite loop
			o = o.chi_tiet_cua
			if o is None:
				break
			family = '<p style="font-size:8px; font-weight:normal;">%s</p>' % (o.ket_qua,) + family
		return family
	def get_full_name (self):
		full_name = (self.full_name + ' PL ' +  self.phan_loai) if self.phan_loai else self.full_name
		return full_name

	@classmethod
	def get_leaf_by_muc_kham_id(cls, muc_kham_id):
		object_list = cls.objects.filter(
			Q(muc_kham__pk=muc_kham_id), Q(kich_hoat=True), 
			~Q(loai_ket_qua = u'danh_sach') | ~Q(phan_loai='')
		)
		return object_list

	@classmethod
	def get_root_by_muc_kham_id(cls, muc_kham_id):
		object_list = cls.objects.filter(
			Q(muc_kham__pk=muc_kham_id), Q(kich_hoat=True), 
			Q(chi_tiet_cua = None) 
		)
		return object_list

	def save(self, *args, **kwargs):
		o = self
		family = o.ket_qua
		for x in xrange(1,3): #to prevent infinite loop
			o = o.chi_tiet_cua
			if o is None:
				break
			family = ' '.join( [o.ket_qua, family] )
		self.slug = slugify(family).replace('-',' ')
		super(KetQuaMau, self).save(*args, **kwargs)

	ket_qua = models.CharField (max_length = 200, verbose_name = u'kết quả', blank=True)
	full_name = models.CharField (max_length = 200, verbose_name = u'full name', blank=True)
	muc_kham = models.ForeignKey(MucKham, verbose_name=u'mục khám')
	chi_tiet_cua = models.ForeignKey ('self', verbose_name=u'chi tiết của', blank = True, null = True)
	phan_loai = models.CharField(max_length = 100, verbose_name=u'phân loại', blank = True)
	loai_ket_qua = models.CharField (max_length=20, choices = LKQ_CHOICES, verbose_name=u'loại kết quả', default = u'danh_sach')	
	ket_qua_so_thap_phan = models.FloatField(verbose_name = u'giá trị mặc định', blank=True, null=True)
	ket_qua_so_nguyen = models.IntegerField (verbose_name = u'giá trị mặc định', blank=True, null=True)
	ket_qua_ky_tu = models.CharField (max_length = 100, verbose_name = u'giá trị mặc định', blank=True)
	kich_hoat = models.BooleanField (verbose_name=u'kích hoạt', default = True)
	gia_tri_min = models.FloatField (verbose_name = u'giá tri min', blank = True, null = True)
	gia_tri_max = models.FloatField (verbose_name = u'giá trị max', blank = True, null = True)
	chu_thich = models.TextField (verbose_name=u'chú thích', blank = True)
	sequence = models.IntegerField(verbose_name=u'thứ tự', blank = True, null=True)
	cap_nhat = models.DateTimeField (verbose_name=u'cập nhật', auto_now =True)
	slug = models.CharField(max_length=200, blank=True)

class KhachHang(models.Model):
	class Meta:
		verbose_name = _(u'khách hàng')
		verbose_name_plural = _(u'danh sách khách hàng')

	def __unicode__(self):
		return self.ten

	ten = models.CharField(max_length=100, verbose_name=u'Tên')
	dia_chi = models.TextField (verbose_name=u'địa chỉ', blank = True)

class GoiKham(models.Model):
	class Meta:
		verbose_name = _(u'gói khám')
		verbose_name_plural = _(u'các gói khám')

	def __unicode__(self):
		strftime = datetime.datetime.strftime
		return u'Từ %s đến %s: %s' % (
				strftime(self.bat_dau.astimezone(TZ), '%d/%m-%H:%M') if self.bat_dau else '-' , \
				strftime(self.ket_thuc.astimezone(TZ), '%d/%m-%H:%M') if self.ket_thuc else '-', \
				self.khach_hang,
			)

	def save(self, *args, **kwargs):
		"Save and auto-generate quy trinh"
		isnew = self.pk is None
		super(GoiKham, self).save(*args, **kwargs)
		if isnew:
			workflow = 	getattr(settings, 'WORKFLOW', None)
			if workflow is not None:
				for key in workflow.keys():
					QuyTrinh(goi_kham=self, muc_kham_id=workflow[key][0], thu_tu=key).save()

	khach_hang = models.ForeignKey('KhachHang', verbose_name=u'khách hàng')
	bat_dau = models.DateTimeField(verbose_name=u'bắt đầu', blank=True, null=True)
	ket_thuc = models.DateTimeField(verbose_name=u'kết thúc', blank=True, null=True)
	dia_diem = models.TextField (verbose_name=u'địa điểm', blank = True)
	huy = models.BooleanField(verbose_name=u'hủy', default=False)
	cho_phep = models.ManyToManyField(User, verbose_name=u'cho phép', blank=True, null=True)

class QuyTrinh(models.Model):
	class Meta:
		verbose_name = _(u'quy trình')
		verbose_name_plural = _(u'danh sách quy trình')
		unique_together = ["goi_kham", "muc_kham"]

	def __unicode__(self):
		return u'%s: %02d-%s' % (self.goi_kham, self.thu_tu, self.muc_kham)

	def save(self, *args, **kwargs):
		super(QuyTrinh, self).save(*args, **kwargs)
		# Invalidate cache for 'quytrinh'
		cache.set('quytrinh_last', datetime.datetime.now())

	goi_kham = models.ForeignKey('GoiKham', verbose_name=u'gói khám')
	muc_kham = models.ForeignKey('MucKham', verbose_name=u'mục khám')
	thu_tu = models.IntegerField(verbose_name=u'thứ tự')

# class BenhNhanManager(models.Manager):
# 	def get_query_set(self):
# 		goikham = cache.get('goikham')
# 		gk_id = goikham.pk if goikham is not None else -1
# 		return super(BenhNhanManager, self).get_query_set().filter(goi_kham_id=gk_id)


class BenhNhan(models.Model):
	class Meta:
		verbose_name = _(u'bệnh nhân')
		verbose_name_plural = _(u'danh sách bệnh nhân')
		unique_together = ['stt', 'goi_kham']

	def __unicode__(self):
		return ' : '.join( [str(self.stt), self.ho_ten])

	@classmethod	
	def next_stt(cls):
		top = cls.objects.order_by('-ngay_ghi')
		if (top):
			return top[0].stt+1
		else:
			return 1
	def export_row(self):
		row =  model_to_dict(self)
		if row.get('goi_tinh') != '': 
			row['gioi_tinh'] = SEX_CHOICES_DICT[row.get('gioi_tinh')].translate(settings.LANGUAGE_CODE)
		qs = self.cac_ket_qua.filter(huy=False)
		filtered = qs.filter(ket_qua_mau = 15)[:1]
		if filtered: row['cao'] = '%s cm' % (filtered.get().gia_tri(), )
		filtered = qs.filter(ket_qua_mau = 16)[:1]
		if filtered: row['can_nang'] = '%s kg' % (filtered.get().gia_tri(), )
		filtered = qs.filter(ket_qua_mau = 25)[:1]
		if filtered: row['mach'] = '%s' % (filtered.get().gia_tri(), )
		
		filtered = qs.filter(ket_qua_mau = 24)[:1]
		if filtered: row['huyet_ap'] = '%s mmHg' % (filtered.get().gia_tri(), )
		filtered = qs.filter(ket_qua_mau = 21)[:1]
		if filtered: row['huyet_ap'] = '%s / %s' % ( filtered.get().gia_tri(), row['huyet_ap'])

		filtered = qs.filter(ket_qua_mau = 18)[:1]
		if filtered: row['khong_kinh_mat_phai'] = '%s' % (filtered.get().gia_tri(), )
		filtered = qs.filter(ket_qua_mau = 19)[:1]
		if filtered: row['khong_kinh_mat_trai'] = '%s' % (filtered.get().gia_tri(), )
		filtered = qs.filter(ket_qua_mau = 22)[:1]
		if filtered: row['co_kinh_mat_phai'] = '%s' % (filtered.get().gia_tri(), )
		filtered = qs.filter(ket_qua_mau = 23)[:1]
		if filtered: row['co_kinh_mat_trai'] = '%s' % (filtered.get().gia_tri(), )
		filtered = qs.filter(ket_qua_mau = 14)[:1]
		if filtered: row['cac_benh_mat'] = '%s' % (filtered.get().gia_tri(), )

		return row

	@classmethod
	def export_excel_list(cls, object_list, file_name = u''):
		cot_list = ["stt" ,"ho_ten" ,"gioi_tinh" ,"ngay_sinh" ,"cmnd" ,"bhyt" ,"cao" ,"can_nang", "bmi" ,"mach" ,"huyet_ap" ,"phan_loai_the_luc" ,"khong_kinh_mat_phai" ,"khong_kinh_mat_trai" ,
				"co_kinh_mat_phai", "co_kinh_mat_trai" ,"cac_benh_mat"]
		workbook = Workbook( os.path.join (settings.MEDIA_ROOT, file_name if file_name else "ket_qua.xlsx" ) )
		sheet1 = workbook.add_worksheet(u'ketquakham')
		l = len(cot_list)
		for x in xrange( 1,l+1 ):
			sheet1.write(0, x-1, cot_list[x-1])
		for y in xrange( 1, len(object_list) + 1 ):
			row = object_list[y-1].export_row()
			for x in xrange( 1,l+1 ):
				v = row.get(cot_list[x-1], u'')
				sheet1.write(y, x-1, v if type(v)==unicode or type(v)==str else str(v))
		workbook.close()

	@classmethod
	def export_excel_range(cls, min_stt, max_stt, file_name = u''):
		pass

	# objects = models.Manager()
	# objects_goikham = BenhNhanManager()
	ket_luan = models.CharField (max_length = 200, verbose_name=u'kết luận', blank= True)
	phan_loai = models.CharField(max_length = 100, verbose_name=u'phân loại sk', blank = True)
	stt = models.IntegerField (verbose_name=u'số thứ tự', default = 0)
	ma_nv = models.CharField (max_length = 50, verbose_name=u'mã nhân viên', blank= True)
	ho_ten = models.CharField (max_length = 50, verbose_name=u'họ tên', db_index=True)
	gioi_tinh = models.CharField (max_length= 10, verbose_name=u'giới tính', choices=SEX_CHOICES, blank = True)
	ngay_sinh = models.DateField (verbose_name = u'ngày sinh', blank = True, null = True)
	bo_phan = models.CharField (max_length = 50, verbose_name=u'bộ phận', blank = True)
	cmnd = models.CharField (max_length=10, verbose_name=u'Số CMND', blank = True)
	bhyt = models.CharField (max_length = 10, verbose_name = u'Số Thẻ BHYT', blank=True)
	check_in = models.DateTimeField (verbose_name=u'giờ bắt đầu', blank = True, null = True)
	check_out = models.DateTimeField (verbose_name=u'giờ kết thúc', blank = True, null = True)
	huy_kham =models.BooleanField(verbose_name=u'huỷ khám', default = False)
	ghi_chu = models.TextField(verbose_name=u'ghi chú', blank = True)
	ngay_ghi = models.DateTimeField (verbose_name=u'ngày ghi', auto_now =True)
	da_kham = models.ManyToManyField (MucKham, verbose_name=u'đã khám', blank=True, null=True)
	hon_nhan = models.CharField (max_length=2, choices = HON_NHAN_CHOICES, verbose_name=u'tt hôn nhân', default = u'dt')
	goi_kham = models.ForeignKey('GoiKham', verbose_name=u'gói khám', blank=True, null=True)


class KetQua(models.Model):
	class Meta:
		verbose_name = _(u'kết quả')
		verbose_name_plural = _(u'kết quả')
	def __unicode__(self):
		if self.loai_ket_qua == u'danh_sach':
			return self.ket_qua
		else:
			return ': '.join([self.ket_qua, self.gia_tri() if type(self.gia_tri()) in [unicode, str] else str(self.gia_tri()) ])
		
	def __init__(self, *args, **kwargs):
		bn = kwargs.pop('benh_nhan', None)
		kqm = kwargs.pop('ket_qua_mau', None)
		super(KetQua, self).__init__(*args, **kwargs)
		if ( bn is not None and kqm is not None):
			self.benh_nhan = bn
			self.ket_qua = kqm.ket_qua
			self.muc_kham_id = kqm.muc_kham_id
			self.phan_loai = kqm.phan_loai
			self.loai_ket_qua = kqm.loai_ket_qua
			self.ket_qua_mau_id = kqm.id
			self.full_name = kqm.full_name

	def get_full_name(self):
		full_name = ('%s PL%s' % (self.full_name,self.phan_loai)) if self.phan_loai else self.full_name
		gt=self.gia_tri_manual()
		try:
			full_name = ('%s %s' % (full_name,gt)) if  self.loai_ket_qua != u'danh_sach' else full_name
		except Exception, e:
			print full_name, gt
			raise e

		return full_name

	def get_admin_html(self):
		full_name = self.full_name
		if self.loai_ket_qua != 'danh_sach':
			full_name = full_name + ' ' + self.gia_tri_manual()
		full_name = ('%s<strong> PL %s</strong>' % (full_name, self.phan_loai) )if self.phan_loai else full_name
		return '<div style="width:300px;">%s</div>' % full_name

	def gia_tri (self):
		tmp = self.ket_qua
		if self.loai_ket_qua == u'so_nguyen':
			tmp = self.ket_qua_so_nguyen
		elif self.loai_ket_qua == u'so_thap_phan':
			tmp = self.ket_qua_so_thap_phan
		elif self.loai_ket_qua == u'ky_tu':
			tmp = self.ket_qua
		return tmp

	def gia_tri_manual (self):
		tmp = ''
		if self.loai_ket_qua == u'so_nguyen':
			tmp = str('-' if self.ket_qua_so_nguyen is None else self.ket_qua_so_nguyen)
		elif self.loai_ket_qua == u'so_thap_phan':
			tmp = str('-' if self.ket_qua_so_thap_phan is None else self.ket_qua_so_thap_phan)
		elif self.loai_ket_qua == u'ky_tu':
			tmp = '-' if not self.ket_qua else self.ket_qua
		return tmp

	def gia_tri_original(self):
		tmp = None
		if self.loai_ket_qua == u'so_nguyen':
			tmp = self.ket_qua_so_nguyen
		elif self.loai_ket_qua == u'so_thap_phan':
			tmp = self.ket_qua_so_thap_phan
		elif self.loai_ket_qua == u'ky_tu':
			tmp = self.ket_qua_ky_tu
		return tmp

	benh_nhan = models.ForeignKey ('BenhNhan', verbose_name=u'bệnh nhân', related_name='cac_ket_qua')
	ket_qua = models.CharField (max_length = 200, verbose_name = u'kết quả', blank=True)
	full_name = models.CharField (max_length = 200, verbose_name = u'full name', blank=True)
	muc_kham = models.ForeignKey(MucKham, verbose_name=u'mục khám')
	phan_loai = models.CharField(max_length = 100, verbose_name=u'phân loại', blank = True)
	bac_sy = models.ForeignKey (User, verbose_name=u'bác sỹ')
	loai_ket_qua = models.CharField (max_length=20, choices = LKQ_CHOICES, verbose_name=u'loại kết quả', default = u'danh_sach')	
	ket_qua_so_thap_phan = models.FloatField(verbose_name = u'giá trị', blank=True, null=True)
	ket_qua_so_nguyen = models.IntegerField (verbose_name = u'giá trị', blank=True, null=True)
	ket_qua_ky_tu = models.CharField (max_length = 100, verbose_name = u'giá trị', blank=True)
	ghi_chu = models.TextField (verbose_name=u'ghi chú', blank = True)
	ngay_ghi = models.DateTimeField (verbose_name=u'ngày ghi', auto_now =True)
	huy = models.BooleanField( verbose_name=u'hủy', default = False)
	ket_qua_mau = models.ForeignKey('KetQuaMau', verbose_name=u'kết quả mẫu', blank=True, null=True)
	slug = models.CharField(max_length=200, blank=True)

# <<<<<<< HEAD
# # class BenhNhan_cayxanh(BenhNhan):
# # 	class Meta:
# # 		proxy = True
# # 		verbose_name = _(u'bệnh nhân')
# # 		verbose_name_plural = _(u'danh sách bệnh nhân cong ty cay xanh')
	

	



	



	
	

# =======
# >>>>>>> d6b1731bc98461b4b2233dd58040ae51759f22bd
