import os,sys

#
os.environ["DJANGO_SETTINGS_MODULE"] = "onsite.settings_production"

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "..") )
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onsite.settings_production")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
