# -*- coding: utf-8 -*-
from onsite.models import BenhNhan
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.forms.widgets import TextInput

class NumberInput(TextInput):
    input_type = 'number'

class BenhNhanForm(forms.Form):
	stt = forms.IntegerField(required = False, label = u'Số thứ tự', widget = NumberInput)
	ten = forms.CharField(required = False, max_length = 20, label = u'Tên')
	def clean(self):
		form_data = self.cleaned_data
		stt = form_data.get('stt')
		ten = form_data.get('ten')
		if stt is None and not ten.strip():
			raise forms.ValidationError( _(u'Xin nhập số thứ tự hoặc tên') )
		return form_data

class BenhNhanModelForm(forms.ModelForm):
	class Meta:
		model = BenhNhan

