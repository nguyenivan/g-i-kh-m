import os
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ROOT_URLCONF = 'onsite.urls'

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
		'NAME': 'goikham_test',                      # Or path to database file if using sqlite3.
		'USER': 'mysql',                      # Not used with sqlite3.
		'PASSWORD': 'nethouse',                  # Not used with sqlite3.
		'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
		'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
		'OPTIONS': {
			"init_command": "SET foreign_key_checks = 0;",
		},
	},
	# 'ibm': {
	# 	'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
	# 	'NAME': 'goikham_ibm',                      # Or path to database file if using sqlite3.
	# 	'USER': 'mysql',                      # Not used with sqlite3.
	# 	'PASSWORD': 'nethouse',                  # Not used with sqlite3.
	# 	'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
	# 	'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
	# 	'OPTIONS': {
	# 		"init_command": "SET foreign_key_checks = 0;",
	# 	},
	# },
	# 'hp': {
	# 	'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
	# 	'NAME': 'goikham_hp',                      # Or path to database file if using sqlite3.
	# 	'USER': 'mysql',                      # Not used with sqlite3.
	# 	'PASSWORD': 'nethouse',                  # Not used with sqlite3.
	# 	'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
	# 	'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
	# 	'OPTIONS': {
	# 		"init_command": "SET foreign_key_checks = 0;",
	# 	},
	# },
}

MIDDLEWARE_CLASSES = (
	'django.middleware.common.CommonMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'onsite.middleware.SetupGoiKham',
	'bv.middleware.AjaxRedirect',
	'debug_toolbar.middleware.DebugToolbarMiddleware',

	# Uncomment the next line for simple clickjacking protection:
	# 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

INTERNAL_IPS = ('127.0.0.1',)

DEBUG_TOOLBAR_CONFIG = {
	'INTERCEPT_REDIRECTS': False,
}

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

INSTALLED_APPS = (
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'south',
	'onsite',
	'bv',
	'int',
	'debug_toolbar',
	'debug_panel', # For debugging ajax calls 
	'autocomplete_light',
	'report_builder',
	'django.contrib.humanize',
	'grappelli',
	'django.contrib.admin',
)

from settings_core import *
