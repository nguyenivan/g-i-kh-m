var $dj=django.jQuery;
$dj(document).ready(function(){
	var hide_all_except = function(selector) {
		$dj('.form-row.ket_qua_so_thap_phan').hide();
		$dj('.form-row.ket_qua_so_nguyen').hide();
		$dj('.form-row.ket_qua_danh_sach').hide();
		$dj('.form-row.ket_qua_ky_tu').hide();
		$dj('.form-row.ket_qua_dung_sai').hide();
		$dj(selector).show();
	};
	hide_all_except('.form-row.ket_qua_danh_sach');

	$dj('#id_loai_ket_qua').change(function(event){
		switch (this.value) {
			case 'so_thap_phan':
				hide_all_except('.form-row.ket_qua_so_thap_phan');
				break;
			case 'so_nguyen':
				hide_all_except('.form-row.ket_qua_so_nguyen');
				break;
			case 'danh_sach':
				hide_all_except('.form-row.ket_qua_danh_sach');
				break;
			case 'ky_tu':
				hide_all_except('.form-row.ket_qua_ky_tu');
				break;
			case 'dung_sai':
				hide_all_except('.form-row.ket_qua_dung_sai');
				break;
		};
	});
	/*
	var eval_muc_chi_tiet = function() {
		level = $dj('#id_muc_chi_tiet')[0].value;
		if (level == '1') {
			$dj('.form-row.chi_tiet_cua').hide()
		} else {
			$dj('.form-row.chi_tiet_cua').show()
		};
	};
	eval_muc_chi_tiet()
	$dj('#id_muc_chi_tiet').change(function(event){
		eval_muc_chi_tiet();
	});
	*/

});
