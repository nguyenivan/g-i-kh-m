var $dj=django.jQuery;
$dj(document).ready(function(){
	var hide_all_except = function(selector) {
		$dj('.form-row.ket_qua_so_thap_phan').hide();
		$dj('.form-row.ket_qua_so_nguyen').hide();
		$dj('.form-row.ket_qua_danh_sach').hide();
		$dj('.form-row.ket_qua_ky_tu').hide();
		$dj('.form-row.ket_qua_dung_sai').hide();
		$dj(selector).show();
	};
	hide_all_except('.form-row.ket_qua_danh_sach');

	$dj('#id_loai_ket_qua').change(function(event){
		switch (this.value) {
			case 'so_thap_phan':
				hide_all_except('.form-row.ket_qua_so_thap_phan');
				break;
			case 'so_nguyen':
				hide_all_except('.form-row.ket_qua_so_nguyen');
				break;
			case 'danh_sach':
				hide_all_except('.form-row.ket_qua_danh_sach');
				break;
			case 'ky_tu':
				hide_all_except('.form-row.ket_qua_ky_tu');
				break;
			case 'dung_sai':
				hide_all_except('.form-row.ket_qua_dung_sai');
				break;
		};
	});
});
