from django.core.cache import cache
from django.http import HttpResponseRedirect, HttpResponse
from onsite.models import QuyTrinh, GoiKham, MucKham
from datetime import datetime
from django.db.models import Q
from django.forms import model_to_dict
from django.shortcuts import render
from django.core.urlresolvers import reverse


def chon_goi_kham(request, goi_kham_id=None):
	if goi_kham_id is None:
		gkl = GoiKham.objects.filter(huy=False)
		now = datetime.now()
		q = Q(bat_dau__lte=now, ket_thuc__gte=now)
		object_list_now = gkl.filter(q)
		object_list_other = gkl.filter(~q)
		return render(request, 'onsite/chongoikham.html', 
			{	'object_list_now': object_list_now,
				'object_list_other': object_list_other,
			}
		)
	else:
		try:
			goikham = GoiKham.objects.get(pk=goi_kham_id)
		except GoiKham.DoesNotExist:
			return HttpResponseRedirect(reverse('chon_goi_kham_force'))
		request.session['quytrinh'] = None
		request.session['muckham'] = None
		request.session['goikham'] = goikham
		return HttpResponseRedirect(reverse('home'))

def online(request):
	return HttpResponse("true")

class SetupGoiKham():
	def process_view(self, request, view_func, view_args, view_kwargs):		
		if view_func.__module__ != 'onsite.views':
			return None
		goikham = request.session.get('goikham')
		if goikham is None:
			return chon_goi_kham(request)

		if request.user.is_authenticated() and request.user not in goikham.cho_phep.all():
			return HttpResponseRedirect(reverse('chon_goi_kham_force'))

		quytrinh = request.session.get('quytrinh')
		quytrinh_last_session = request.session.get('quytrinh_last', datetime(2000, 1, 1) )
		quytrinh_last = cache.get('quytrinh_last', datetime.now() )
		if quytrinh is None or quytrinh_last>quytrinh_last_session:
			qtl = list(QuyTrinh.objects.filter(goi_kham_id=goikham).values())
			qtl.sort(key=lambda item: item['thu_tu'])
			quytrinh = [qt['muc_kham_id'] for qt in qtl]
			request.session['quytrinh'] = quytrinh
			request.session['quytrinh_last'] = datetime.now()

		muckham = request.session.get('muckham')
		if muckham is None:
			mkl = MucKham.objects.filter()
			muckham = {}
			for mk in mkl:
				muckham[mk.id] = mk
			request.session['muckham'] = muckham









