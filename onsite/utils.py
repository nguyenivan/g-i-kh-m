# -*- coding: utf-8 -*-
import base64, struct, math
# encode(1000001) = 'QUIP'
def encode(n):
	data = struct.pack('<Q', n).rstrip('\x00')
	if len(data) == 0:
		data = '\x00'
	s = base64.urlsafe_b64encode(data).rstrip('=')
	return s
	
# decode('QUIP') = 1000001
def decode(s):
	data = base64.urlsafe_b64decode(s + '==')
	n = struct.unpack('<Q', data + '\x00' * (8 - len(data)))
	return n[0]
	
def encode_tileid(x,y,z):
	'for given tile x,y,z return encoded tileid calculated by 000000X00000Y0Z '
	return encode( x * 10**8 + y * 10**2 + z)
	pass

def decode_tileid(tileid):
	decoded = decode(tileid)
	z = decoded % 10**2
	y = ( decoded // 10**2 ) % 10**6
	x = decoded // 10**8
	return x,y,z

def upper_vietnamese(s):
   if type(s) == type(u""):
      return s.upper()
   return unicode(s, "utf8").upper().encode("utf8")
  
import string
import re

VNCHAR = 'ạảãàáâậầấẩẫăắằặẳẵóòọõỏôộổỗồốơờớợởỡéèẻẹẽêếềệểễúùụủũưựữửừứíìịỉĩýỳỷỵỹđ'
VNCHAR += upper_vietnamese(VNCHAR)
INTAB = [ch.encode('utf8') for ch in unicode(VNCHAR, 'utf8')]
#INTAB = [ch.encode('utf8') for ch in VNCHAR]

OUTTAB = "a"*17 + "o"*17 + "e"*11 + "u"*11 + "i"*5 + "y"*5 + "d"
OUTTAB += OUTTAB.upper()
 
r = re.compile("|".join(INTAB))
replaces_dict = dict(zip(INTAB, OUTTAB))

def remove_accent(utf8_str):
	if type(utf8_str) == type(''):
		return r.sub(lambda m: replaces_dict[m.group(0)], utf8_str)
	else:
		raise (Exception('Input parameter must be str type.'))
   
def test_accent():
	print u'ThIÊn Hạ Vô ĐịCh', remove_accent('ThIÊn Hạ Vô ĐịCh')
	print u'THIÊN HẠ VÔ ĐỊCH', remove_accent('THIÊN HẠ VÔ ĐỊCH')
 	

import time
def random_date(start, end, prop, format=None):
	"""Get a time at a proportion of a range of two formatted times.
	start and end should be strings specifying times formated in the
	given format (strftime-style), giving an interval [start, end].
	prop specifies how a proportion of the interval to be taken after
	start.  The returned time will be in the specified format.
	"""
	if format is None:
		format =  '%m/%d/%Y %I:%M %p'
	stime = time.mktime(time.strptime(start, format))
	etime = time.mktime(time.strptime(end, format))
	ptime = stime + prop * (etime - stime)
	return time.localtime(ptime)


import urllib
def url_with_querystring(path, **kwargs):
    return path + '?' + urllib.urlencode(kwargs)


