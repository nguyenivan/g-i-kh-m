# -*- coding: utf-8 -*-
from django.contrib import admin
from onsite.models import BenhNhan,KhachHang, GoiKham, QuyTrinh, MucKham, KetQuaMau, KetQua
from django.db import models
from onsite.excel import phanloai_cando, phanloai_thiluc, tinh_bmi
from django.core.cache import cache


class QuyTrinhInline(admin.TabularInline):
	model = QuyTrinh

class GoiKhamAdmin(admin.ModelAdmin):
	inlines = [
		QuyTrinhInline,
	]

class KetLuanFilter(admin.SimpleListFilter):
	title = u'đã kết luận'

	# Parameter for the filter that will be used in the URL query.
	parameter_name = 'ketluan'

	def lookups(self, request, model_admin):
		"""
		Returns a list of tuples. The first element in each
		tuple is the coded value for the option that will
		appear in the URL query. The second element is the
		human-readable name for the option that will appear
		in the right sidebar.
		"""
		return (
			('yes', u'đã kết luận'),
			('no', u'chưa có kết luận'),
		)

	def queryset(self, request, queryset):
		"""
		Returns the filtered queryset based on the value
		provided in the query string and retrievable via
		`self.value()`.
		"""
		# Compare the requested value (either '80s' or 'other')
		# to decide how to filter the queryset.
		q = models.Q(ket_luan='')
		if self.value() == 'yes':
			return queryset.filter(~q)
		if self.value() == 'no':
			return queryset.filter(q)

class GoiKhamFilter(admin.SimpleListFilter):
	title = u'Các gói khám'

	# Parameter for the filter that will be used in the URL query.
	parameter_name = 'goikham'

	def lookups(self, request, model_admin):
		gkl = GoiKham.objects.all()
		ret = []
		for gk in gkl:
			ret.append( (gk.pk, gk.__unicode__() ))
		return ret

	def queryset(self, request, queryset):
		q = models.Q(goi_kham__id=self.value())
		return queryset.filter(q)

class BenhNhanAdmin(admin.ModelAdmin):
	list_display = ['id', 'stt', 'goi_kham', 'ho_ten', 'ket_luan', 'phan_loai', 'cac_muc_da_kham', 'ket_qua_tong_hop',  'ma_nv', 'bo_phan', 'gioi_tinh' , 'ngay_sinh', ]
	list_editable = ['ket_luan', 'ho_ten', 'phan_loai', 'stt', 'goi_kham']
	search_fields = ['stt', 'ho_ten', 'bo_phan']
	list_per_page = 10 

	list_filter = [KetLuanFilter, GoiKhamFilter,]

	def save_model(self, request, obj, form, change):
		super(BenhNhanAdmin, self).save_model(request, obj, form, change)
		if change:
			bn = cache.delete('benh_nhan_%s_%s' % (obj.stt, obj.goi_kham.pk) )

	def queryset(self, request):
		qs = super(BenhNhanAdmin, self).queryset(request).order_by('-da_kham__count')
		qs = qs.annotate(models.Count('da_kham'))
		return qs
		
	def get_form(self, request, obj=None, **kwargs):
		form = super(BenhNhanAdmin, self).get_form(request, obj, **kwargs)
		form.base_fields['stt'].initial = BenhNhan.next_stt()
		return form

	# def thoi_gian(self, obj):
	# 	return obj.cac_ket_qua.latest("ngay_ghi").ngay_ghi
	# thoi_gian.admin_order_field = 'cac_ket_qua__ngay_ghi__max'



	def ket_qua_tong_hop(self, obj):
		kql = obj.cac_ket_qua.filter(huy=False)
		ret  = []
		pll  = []
		for kq in kql:
			ret.append( kq.get_admin_html() )
			if kq.phan_loai:
				pll.append(kq.phan_loai)

		#Tinh BMI va phan loai can do
		kqmau_list  = KetQuaMau.objects.filter(pk__in = [997, 998]).order_by('pk')
		if len (kqmau_list) == 2:
			chieucao = kql.filter(ket_qua_mau = kqmau_list[0])
			cannang = kql.filter(ket_qua_mau = kqmau_list[1])
			if chieucao and cannang:
				cm = chieucao[0].gia_tri_original()
				kg = cannang[0].gia_tri_original()
				pl = phanloai_cando(obj.gioi_tinh, kg, cm)
				bmi = tinh_bmi(kg, cm)
				ret.append('<div>BMI: %s <strong>PL %s</strong></div>' % (bmi, pl))
				pll.append(str(pl))

		kqmau_list  = KetQuaMau.objects.filter(pk__in = [2, 3]).order_by('pk')
		if len (kqmau_list) == 2:
			matphai = kql.filter(ket_qua_mau = kqmau_list[0])
			mattrai = kql.filter(ket_qua_mau = kqmau_list[1])
			if matphai and mattrai:
				mp = matphai[0].gia_tri_original()
				mt = mattrai[0].gia_tri_original()
				pl = phanloai_thiluc(mt,mp)
				ret.append( u'<div>Thị Lực: <strong>PL %s</strong></div>' % pl )
				pll.append(str(pl))

		pl_tong = 1
		for pl in pll:
			try:
				pl_benhly = int(pl[:1])
				if pl_benhly > pl_tong:
					pl_tong = pl_benhly
			except ValueError:
				pass
		ret.append( u'<div><strong>PHÂN LOẠI %s</strong></div>' % pl_tong)
		return ''.join(ret)

	ket_qua_tong_hop.allow_tags = True
	ket_qua_tong_hop.short_description = u'Kết Quả Tổng Hợp'

	def cac_muc_da_kham(self, obj):
		return "<br/>".join([mk.ten for mk in obj.da_kham.all()])
	cac_muc_da_kham.allow_tags = True
	cac_muc_da_kham.short_description = u'Các Mục Đã Khám'
	cac_muc_da_kham.admin_order_field = 'da_kham__count'



class MucKhamAdmin(admin.ModelAdmin):
	list_display = [ 'pk','stt', 'ten', 'nhom', 'kich_hoat']
	list_editable = ['stt', 'ten', 'nhom', 'kich_hoat']

class KetQuaAdmin(admin.ModelAdmin):
	list_display = ['pk', 'ngay_ghi', 'ket_qua','muc_kham', 'full_name', 'phan_loai', 'loai_ket_qua', 'gia_tri', 'huy', 'benh_nhan']
	list_editable = ['ket_qua', 'muc_kham','full_name', 'phan_loai', 'loai_ket_qua', 'huy' ]
	search_fields = ['benh_nhan__stt']
	class Media:
		js = ('admin/ket_qua.js',)

class KetQuaMauAdmin(admin.ModelAdmin):
	list_display = ['pk', 'ket_qua','full_name', 'muc_kham', 'chi_tiet_cua', 'phan_loai', 'loai_ket_qua']
	list_editable = ['ket_qua','full_name', 'muc_kham', 'phan_loai', 'loai_ket_qua' ]
	search_fields = ['id','ket_qua']
	class Media:
		js = ('admin/ket_qua_mau.js',)
		css = {
			'all': ('admin/ket_qua_mau.css',)
		}


# class BenhNhanAdmin(admin.ModelAdmin):
#     """
#     Author Admin
#     """
#     form = AuthorForm

#     list_display = ['profile_photo', 'first_name', 'last_name', 'title']
#     search_fields = ['first_name', 'last_name', 'title', 'credential']
#     prepopulated_fields = {'slug': ('first_name', 'last_name', 'title')}

#     def profile_photo(self, obj) :
#         return '<img src="%s" title="%s" />' % (resize_image(obj.photo, '100x100'), obj.title)

#     profile_photo.allow_tags = True

admin.site.register(BenhNhan, BenhNhanAdmin)

admin.site.register(KhachHang)
admin.site.register(GoiKham, GoiKhamAdmin)

admin.site.register(MucKham, MucKhamAdmin)
admin.site.register(KetQuaMau, KetQuaMauAdmin)
admin.site.register(KetQua, KetQuaAdmin)
