# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'KetQua.full_name'
        db.add_column(u'onsite_ketqua', 'full_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'KetQua.full_name'
        db.delete_column(u'onsite_ketqua', 'full_name')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'onsite.benhnhan': {
            'Meta': {'object_name': 'BenhNhan'},
            'bhyt': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'bo_phan': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'check_in': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'check_out': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'cmnd': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'da_kham': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['onsite.MucKham']", 'null': 'True', 'blank': 'True'}),
            'ghi_chu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'gioi_tinh': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'ho_ten': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'hon_nhan': ('django.db.models.fields.CharField', [], {'default': "u'dt'", 'max_length': '2'}),
            'huy_kham': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ket_luan': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ma_nv': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'ngay_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'ngay_sinh': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'phan_loai': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'stt': ('django.db.models.fields.IntegerField', [], {'default': '0', 'unique': 'True'})
        },
        u'onsite.ketqua': {
            'Meta': {'object_name': 'KetQua'},
            'bac_sy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'benh_nhan': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cac_ket_qua'", 'to': u"orm['onsite.BenhNhan']"}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ghi_chu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'huy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ket_qua': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ket_qua_ky_tu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ket_qua_mau': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['onsite.KetQuaMau']", 'null': 'True', 'blank': 'True'}),
            'ket_qua_so_nguyen': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ket_qua_so_thap_phan': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'loai_ket_qua': ('django.db.models.fields.CharField', [], {'default': "u'danh_sach'", 'max_length': '20'}),
            'muc_kham': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['onsite.MucKham']"}),
            'ngay_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'phan_loai': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'onsite.ketquamau': {
            'Meta': {'object_name': 'KetQuaMau'},
            'cap_nhat': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'chi_tiet_cua': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['onsite.KetQuaMau']", 'null': 'True', 'blank': 'True'}),
            'chu_thich': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'gia_tri_max': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'gia_tri_min': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ket_qua': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ket_qua_ky_tu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ket_qua_so_nguyen': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ket_qua_so_thap_phan': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'kich_hoat': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'loai_ket_qua': ('django.db.models.fields.CharField', [], {'default': "u'danh_sach'", 'max_length': '20'}),
            'muc_kham': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['onsite.MucKham']"}),
            'phan_loai': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'onsite.muckham': {
            'Meta': {'object_name': 'MucKham'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kich_hoat': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'nhom': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.Group']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'stt': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'ten': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['onsite']