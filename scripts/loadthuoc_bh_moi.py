# -*- coding: utf-8 -*-
from datetime import datetime, date, timedelta
from django.utils import timezone
from django.contrib.auth.models import User
from bv.models import Thuoc
def dfs(s):

	try:
		d = datetime.strptime(s, "%d/%m/%Y").date()
	except ValueError:
		try:
			d = datetime.strptime(s, "%Y").date()
		except ValueError:
			d = None
	return d
l=[]
l.append(Thuoc(ma_phi=u"dv1".lower(),hoat_chat=u"Amoxicilin 500mg + clavulanic acid  62.5mg",dien_giai=u"Aumakim 562,5",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv2".lower(),hoat_chat=u"Amoxicilin 875mg, Clavulanic acid125mg",dien_giai=u"Claminat 1g",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv3".lower(),hoat_chat=u"Amoxicilin 500mg, Clavulanic acid125mg",dien_giai=u"Claminat 625mg ",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv4".lower(),hoat_chat=u"Amoxicilin 500mg + Potassium clavulanate 125mg",dien_giai=u"ACLE tablet 625mg",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv5".lower(),hoat_chat=u"Amoxicilin 500mg",dien_giai=u"Pharmox 500mg",nuoc_sx=u"Việt Nam",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv6".lower(),hoat_chat=u"Cefuroxime 250 mg",dien_giai=u"Doroxim",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv7".lower(),hoat_chat=u"Cefuroxime 250 mg",dien_giai=u"Vanmenol",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv8".lower(),hoat_chat=u"Cefuroxime Axetil USP 500mg",dien_giai=u"CEFUROXIME 500mg",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv9".lower(),hoat_chat=u"Cefuroxime 100mg",dien_giai=u"CEFICHEM",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv10".lower(),hoat_chat=u"Cefixime Capsules 200mg",dien_giai=u"Sagafixim 200mg",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv11".lower(),hoat_chat=u"Cefixime 200mg",dien_giai=u"Imexime 200mg",nuoc_sx=u"Việt Nam",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv12".lower(),hoat_chat=u"Cefpodoxime 200mg",dien_giai=u"OXIFIDIDE 200",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv13".lower(),hoat_chat=u"Tetracycline 500mg",dien_giai=u"Tetracycline 500",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv14".lower(),hoat_chat=u"Cefdinir 300mg",dien_giai=u"Trixenim 300",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv15".lower(),hoat_chat=u"Ofloxacin 200mg",dien_giai=u"Ofloxacin-pms",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv16".lower(),hoat_chat=u"Levofloxacin 500mg",dien_giai=u"Levmax",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv17".lower(),hoat_chat=u"Levofloxacin 250mg",dien_giai=u"Levofloxacin 250",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv18".lower(),hoat_chat=u"Levofloxacin 500mg",dien_giai=u"Vacoflox L",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv19".lower(),hoat_chat=u"Sulfamethoxazol 800mg, Trimethoprim 160mg",dien_giai=u"Cotrimfort 960",nuoc_sx=u"Stada",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv20".lower(),hoat_chat=u"Sulfamethoxazol 400mg, Trimethoprim 80mg",dien_giai=u"Cotrimfort 480",nuoc_sx=u"Stada",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv21".lower(),hoat_chat=u"Ciprofloxacin 500mg",dien_giai=u"Ciprofloxacin",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv22".lower(),hoat_chat=u"Gentamicin 80mg",dien_giai=u"Gentamicine kabi 80",nuoc_sx=u"Việt Nam",don_vi=u"ống",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv23".lower(),hoat_chat=u"Doxyclin 100mg",dien_giai=u"Doxycyclin",nuoc_sx=u"Việt Nam",don_vi=u"Viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv24".lower(),hoat_chat=u"Ibuprofen 200mg+ Codein 8mg",dien_giai=u"Fencecod",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv25".lower(),hoat_chat=u"Paracetamol 500mg+ Caffein 65mg",dien_giai=u"Paracold Extra",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv25.1".lower(),hoat_chat=u"Paracetamol 500mg+ Cafein 65mg",dien_giai=u"Panadol extra",nuoc_sx=u"VN",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv26".lower(),hoat_chat=u"Paracetamol 500mg+ Caffein 25mg",dien_giai=u"Paracold Flu",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv27".lower(),hoat_chat=u"Paracetamol 150mg+Vitamin C 75mg",dien_giai=u"Paracold (Kids)",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv28".lower(),hoat_chat=u"Oxytocin 5 I.U.",dien_giai=u"Oxytocin",nuoc_sx=u"Đài Loan",don_vi=u"tiêm",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv29".lower(),hoat_chat=u"Celecoxib 200mg",dien_giai=u"SAGACOXIB 200",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv30".lower(),hoat_chat=u"celecoxib 200mg",dien_giai=u"MICRO CELOCOXIB 100mg",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv31".lower(),hoat_chat=u"BP Meloxicam 7.5mg",dien_giai=u"EUROCAM",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv32".lower(),hoat_chat=u"Nefopam HCl 30mg",dien_giai=u"Nisidin",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv33".lower(),hoat_chat=u"Paracetemol 650mg, Ibuprofen 200mg",dien_giai=u"Podofen ",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv34".lower(),hoat_chat=u"Paracetamol 300mg, Ibuprofen 200mg, Cafein 20mg",dien_giai=u"Idoltavic (chai)",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv35".lower(),hoat_chat=u"Clonixine lysinate 125mg",dien_giai=u"Clone ",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv36".lower(),hoat_chat=u"Paracetemol 500mg",dien_giai=u"Paracetamol",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv37".lower(),hoat_chat=u"Alendronic Acid USP 70 mg",dien_giai=u"Graceflil",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv38".lower(),hoat_chat=u"Paracetamol 50mg",dien_giai=u"Vadol 5 (trắng)",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv39".lower(),hoat_chat=u"Paracetamol 650mg",dien_giai=u"Vadol 650mg ",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv40".lower(),hoat_chat=u"Paracetamol 500mg, Codein Phosphate 30mg",dien_giai=u"Paracold Codein effer Vescent",nuoc_sx=u"VN",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv41".lower(),hoat_chat=u"Paracetamol 500mg",dien_giai=u"Paracold 500 effervescent",nuoc_sx=u"VN",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv42".lower(),hoat_chat=u"Ibuprofen 400mg",dien_giai=u"Ibuprofen 400mg",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv43".lower(),hoat_chat=u"Meloxicam 7.5mg",dien_giai=u"Meloxicam",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv44".lower(),hoat_chat=u"Meloxicam 15mg",dien_giai=u"Meloxicam 15mg",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv45".lower(),hoat_chat=u"Natridiclofenac 75mg",dien_giai=u"Diclofenac 75 tím",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv46".lower(),hoat_chat=u"Alverin citrat 40mg",dien_giai=u"Spasmaverin",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv47".lower(),hoat_chat=u"Lysozyme chlorde 90mg",dien_giai=u"SYLAZYM",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv48".lower(),hoat_chat=u"Methylprednisolon 16mg",dien_giai=u"Vimethy",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv49".lower(),hoat_chat=u"Methyl prednisolone 16mg",dien_giai=u"Amedred ",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv49.1".lower(),hoat_chat=u"Methyprednisolon 16mg",dien_giai=u"Medexa 16mg",nuoc_sx=u"Indonesia",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv50".lower(),hoat_chat=u"Prednisolon 5mg",dien_giai=u"Prednisolone 5mg",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv51".lower(),hoat_chat=u"Chymostripsin 4200UI",dien_giai=u"Alfachymostripsin",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv52".lower(),hoat_chat=u"Tolperrisone Hydrochloride 150mg",dien_giai=u"Jeaxotil Tab.",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv53".lower(),hoat_chat=u"Eperisone HCl 50 mg",dien_giai=u"Epelax",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv54".lower(),hoat_chat=u"Glucosamin 1500mg",dien_giai=u"Vasomin ",nuoc_sx=u"Việt Nam",don_vi=u"gói",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv55".lower(),hoat_chat=u"Glucosamin 500mg",dien_giai=u"Glucosamin 500",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv56".lower(),hoat_chat=u"Calcitriol 0.25mg",dien_giai=u"Beedrafcin",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv57".lower(),hoat_chat=u"Diacerin 50mg",dien_giai=u"Diacezax",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv58".lower(),hoat_chat=u"Mephenesin 250mg",dien_giai=u"D-Constresine 250mg",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv59".lower(),hoat_chat=u"Mephenesin 500mg",dien_giai=u"Mephenesin 500",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv60".lower(),hoat_chat=u"Tolperrisone Hydrochloride 150mg",dien_giai=u"Mydocalm 150",nuoc_sx=u"Hungary",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))

l.append(Thuoc(ma_phi=u"dv61".lower(),hoat_chat=u"Atorvastatin 10mg",dien_giai=u"ATORVASTATIN 10mg",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv62".lower(),hoat_chat=u"Atorvastatin 20mg",dien_giai=u"ATORVASTATIN 20mg",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv63".lower(),hoat_chat=u"Kali Losartan 50mg",dien_giai=u"CHEMSTAT 50",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv64".lower(),hoat_chat=u"Clopidogrel 75mg",dien_giai=u"ABHIGREL75",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv65".lower(),hoat_chat=u"Diosmin micronised 450mg, Hesperidin micronised 50mg",dien_giai=u"SAVI DIMIN",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv66".lower(),hoat_chat=u"Meclofenoxat HCl 250mg",dien_giai=u"Liciril",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv67".lower(),hoat_chat=u"Lisinopril dihydrate 10mg/ 5mg",dien_giai=u"Zestril 10mg/ 5mg",nuoc_sx=u"Anh",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv68".lower(),hoat_chat=u"Isosorbid-5-monotrate 30mg",dien_giai=u"Imdur 30mg",nuoc_sx=u"Thụy Điển",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv68.1".lower(),hoat_chat=u"Nitroglycerin 2.5mg",dien_giai=u"Nitrostad retard 2.5",nuoc_sx=u"",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv68.2".lower(),hoat_chat=u"Captopril 25mg",dien_giai=u"Captopril ",nuoc_sx=u"",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv68.3".lower(),hoat_chat=u"Amlodipin 5mg",dien_giai=u"Amlodipin 5mg",nuoc_sx=u"",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv69".lower(),hoat_chat=u"Glimepiride 2mg",dien_giai=u"EVOPRIDE",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv70".lower(),hoat_chat=u"Indapamide 1.5mg",dien_giai=u"Natrilix SR 1.5mg",nuoc_sx=u"Pháp",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv71".lower(),hoat_chat=u"Olanzapine 5mg",dien_giai=u"Ozip-5",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv72".lower(),hoat_chat=u"Olanzapine 10mg",dien_giai=u"Oltha 10",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv73".lower(),hoat_chat=u"Pregabalin 75mg",dien_giai=u"Essividine",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv74".lower(),hoat_chat=u"Flunarizine 5mg",dien_giai=u"NEWCLEN cap",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv75".lower(),hoat_chat=u"Piracetam 800mg",dien_giai=u"Piracetam  800",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv76".lower(),hoat_chat=u"Piracetam 800mg",dien_giai=u"HUMICETA",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv77".lower(),hoat_chat=u"Piracetam 400mg",dien_giai=u"Vacetam 400",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv78".lower(),hoat_chat=u"Sulpirde 50mg",dien_giai=u"Sulpiride",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv79".lower(),hoat_chat=u"Magnesi lactat 2 H20 470mg; Pyridoxin HCL 5mg",dien_giai=u"Magnesi B6 ",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv80".lower(),hoat_chat=u"Gingkobilo 60mg",dien_giai=u"Ginkgobilobaybay",nuoc_sx=u"USA",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv81".lower(),hoat_chat=u"Thymomodulin 80mg",dien_giai=u"Kerodal ",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv82".lower(),hoat_chat=u"Trimebutine Maleate 100mg",dien_giai=u"Trimetinel Tab",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv83".lower(),hoat_chat=u"Alpha amylase 100mg, papain USP 100mg; Simethicone USP 30mg",dien_giai=u"Napepsin",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv84".lower(),hoat_chat=u"Pancreatin 175mg, Ox-bile Ext. 25mg, Dimethicone 25mg, Hemicellulase 50mg",dien_giai=u"Panthicone-F tab",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv85".lower(),hoat_chat=u"Natri Rabeprazole 20mg",dien_giai=u"RABENIS 20",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv86".lower(),hoat_chat=u"Omeprazole BP 20mg",dien_giai=u"OMLIFE",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv87".lower(),hoat_chat=u"Omeprazol 20mg",dien_giai=u"Vaco-Omez 20mg",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv88".lower(),hoat_chat=u"Omeprazol 40mg",dien_giai=u"Vacoomez 40mg",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv89".lower(),hoat_chat=u"Pantoprazole 40mg",dien_giai=u"DYPES",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv90".lower(),hoat_chat=u"Pancreatin 170mg, Simethicon 60mg",dien_giai=u"Zintizyme",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv91".lower(),hoat_chat=u"Levosulpirid 25mg",dien_giai=u"Kestolac",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv92".lower(),hoat_chat=u"Trimebutine Maleate 100mg",dien_giai=u"Malbutin",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv93".lower(),hoat_chat=u"Ranitidin 150mg",dien_giai=u"Ratacid 150",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv94".lower(),hoat_chat=u"Itopride HCl 50mg",dien_giai=u"Air-X",nuoc_sx=u"Nhat",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv95".lower(),hoat_chat=u"Hyoscine Butylbromide 10mg",dien_giai=u"Buscopan",nuoc_sx=u"Pháp",don_vi=u"Viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv96".lower(),hoat_chat=u"Biphenyl Dimethyl Dicarboxylate 25mg",dien_giai=u"ILSELO",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv97".lower(),hoat_chat=u"Biphenyl Dimethyl Dicarboxylate 25mg",dien_giai=u"Fortec",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv98".lower(),hoat_chat=u"Tenofovir disoproxil fumarat 300mg",dien_giai=u"Unicavir",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv99".lower(),hoat_chat=u"Phospholipids from soya-beans",dien_giai=u"Essentiale Forte 300mg",nuoc_sx=u"Đức",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv100".lower(),hoat_chat=u"Arginin hydroclorid 200mg",dien_giai=u"AMP-GININE",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv101".lower(),hoat_chat=u"Thymomodium 80mg",dien_giai=u"Kerodal Cap.",nuoc_sx=u"Hàn Quốc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv102".lower(),hoat_chat=u"Carbocysteine 1500mg + Salbutamol 12mg",dien_giai=u"Broncal",nuoc_sx=u"Việt Nam",don_vi=u"chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv103".lower(),hoat_chat=u"Betamethason 0,25mg; Dexclorphenicam maleat 2mg",dien_giai=u"Cestasin ",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv104".lower(),hoat_chat=u"Salbutamol 2mg",dien_giai=u"Salbutamol 2mg",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv104.1".lower(),hoat_chat=u"Salbutamol 4mg",dien_giai=u"Salbutamol 4mg",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv105".lower(),hoat_chat=u"Eprazinon.2HCl 50mg",dien_giai=u"Turanon ",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv106".lower(),hoat_chat=u"Terpinhydrat 100mg; Codeinbase 10mg",dien_giai=u"Terp-cod",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv107".lower(),hoat_chat=u"Promethazin HCl 15mg",dien_giai=u"Promethazin",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv107.1".lower(),hoat_chat=u"Cetirizin Hydroclorid 10mg",dien_giai=u"Cetirizin",nuoc_sx=u"",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv108".lower(),hoat_chat=u"Loratatine 10mg",dien_giai=u"Loratadin",nuoc_sx=u"Việt Nam",don_vi=u"Viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv108.1".lower(),hoat_chat=u"Fexofenadine 60mg",dien_giai=u"Fexalar",nuoc_sx=u"AD",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv109".lower(),hoat_chat=u"Calcium gluconat 500mg, vitamin D3 200 UI",dien_giai=u"Vacocalcium",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv110".lower(),hoat_chat=u"B1 125mg+B6 125mg +B12 125 mcg",dien_giai=u"Rousbevit",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv111".lower(),hoat_chat=u"Calcium gluconate monohydrate 500mg, vitamin D 200UI",dien_giai=u"Calci D (Imex pharma)",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv112".lower(),hoat_chat=u"Calcium gluconate monohydrate 500mg, vitamin D 200UI",dien_giai=u"Calci D (Tpharco)",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv113".lower(),hoat_chat=u"vitamin E (D-Alpha-Tocopheryl Acetate)",dien_giai=u"Enat 400UI",nuoc_sx=u"Thái Lan",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv114".lower(),hoat_chat=u"Zinc gluconate 70mg",dien_giai=u"Farzincol",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv115".lower(),hoat_chat=u"125mg B1+ 125mg B6 + 125mg B12",dien_giai=u"Vaco B-Neurin ",nuoc_sx=u"VN",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv116".lower(),hoat_chat=u"L-Cystine 500mg",dien_giai=u"L-Cystine",nuoc_sx=u"Han Quoc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv117".lower(),hoat_chat=u"Vitamin A,D,C,B1,B6,B12,PP,Fe",dien_giai=u"Obimin",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv118".lower(),hoat_chat=u"Vitamin C 500mg",dien_giai=u"Vit C",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv119".lower(),hoat_chat=u"Vitamin B1, B2, B6, PP, C",dien_giai=u"B Complec C",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv120".lower(),hoat_chat=u"Rutin, Vitamin C",dien_giai=u"Rutin C",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv120.1".lower(),hoat_chat=u"",dien_giai=u"Ferrovit",nuoc_sx=u"",don_vi=u"",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv121".lower(),hoat_chat=u"EPA 18%, DHA 12%, Vitamin E 50-150mg",dien_giai=u"Omega 3",nuoc_sx=u"Han Quoc",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv122".lower(),hoat_chat=u"Vitamin A 1900 IU, vitamin D3 150IU",dien_giai=u"Vitamin A-D",nuoc_sx=u"Việt Nam",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv123".lower(),hoat_chat=u"Loperamide 2mg",dien_giai=u"Loperamide",nuoc_sx=u"Ấn Độ",don_vi=u"viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv123".lower(),hoat_chat=u"Sorbitol 5g",dien_giai=u"Sorbitol",nuoc_sx=u"Việt Nam",don_vi=u"gói",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv124".lower(),hoat_chat=u"Tobramycin 15mg, Dexamethasone 5mg, Benzalkonium HCL 1mg",dien_giai=u"Clesspra DX",nuoc_sx=u"Ấn Độ",don_vi=u"chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv125".lower(),hoat_chat=u"Cloramphenicol 0,04g",dien_giai=u"Cloraxin (nhỏ mắt)",nuoc_sx=u"Việt Nam",don_vi=u"Chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv126".lower(),hoat_chat=u"natricloride 0.9%",dien_giai=u"Efticol",nuoc_sx=u"Việt Nam",don_vi=u"chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv127".lower(),hoat_chat=u"Cloramphenicol 20mg + Dexamathesone 5mg",dien_giai=u"Dexacol",nuoc_sx=u"Việt Nam",don_vi=u"chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv128".lower(),hoat_chat=u"natricloride 0.9%",dien_giai=u"Natri Mắt",nuoc_sx=u"Việt Nam",don_vi=u"chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv129".lower(),hoat_chat=u"natricloride 0.9%",dien_giai=u"Natri mắt mũi",nuoc_sx=u"",don_vi=u"chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv130".lower(),hoat_chat=u"Povidon iod 10%",dien_giai=u"Povidine 20ml",nuoc_sx=u"VN",don_vi=u"Chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv131".lower(),hoat_chat=u"oxy già",dien_giai=u"oxy già 60ml",nuoc_sx=u"VN",don_vi=u"Chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv132".lower(),hoat_chat=u"Betamethasone 6.4mg, Gentamicin 10mg",dien_giai=u"Gentrison",nuoc_sx=u"VN",don_vi=u"tuýp",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))


l.append(Thuoc(ma_phi=u"dv133".lower(),hoat_chat=u"clotrimazole 100mg",dien_giai=u"Clomaz",nuoc_sx=u"Thái Lan",don_vi=u"Viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv133.1".lower(),hoat_chat=u"",dien_giai=u"Clorifort",nuoc_sx=u"Ấn Độ",don_vi=u"Viên",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))
l.append(Thuoc(ma_phi=u"dv134".lower(),hoat_chat=u"Sterculia Foetida, tinh chất mủ trôm, Method, Copper Sulfat",dien_giai=u"Helife",nuoc_sx=u"Việt Nam",don_vi=u"chai",nha_cc=u"",don_gia=0, service_line=u'dich_vu'))

count = 0

for o in l:
	count = count + 1
	try:
		o.save()
		print count, o
	except Exception, error:
		print 'Data import error at row: ', count, error

