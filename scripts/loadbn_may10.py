# -*- coding: utf-8 -*-
from datetime import datetime, date
from exceptions import Exception
from onsite.models import BenhNhan
def dfs(s):

	try:
		d = datetime.strptime(s, "%Y%m%d").date()
	except ValueError:
		try:
			d = datetime.strptime(s, "%Y").date()
		except ValueError:
			d = None
	return d
l=[]

l.append(BenhNhan(stt = 1579, ho_ten = u'Võ Diễm Chi ', ma_nv = '1', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1580, ho_ten = u'Phạm Thị Tuyết ', ma_nv = '2', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1581, ho_ten = u'Triệu Thị Liên', ma_nv = '3', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1582, ho_ten = u'Nguyễn Thị Thanh Thủy', ma_nv = '4', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1583, ho_ten = u'Tăng Thị Thanh Thủy', ma_nv = '5', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1584, ho_ten = u'Hoàng Thị Duyên ', ma_nv = '6', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1585, ho_ten = u'Phạm Ngọc Hồng', ma_nv = '7', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1586, ho_ten = u'Phạm Thị Bảy', ma_nv = '8', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1587, ho_ten = u'Trương Thị Vân', ma_nv = '9', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1588, ho_ten = u'Nguyễn Thị Thắm', ma_nv = '10', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1589, ho_ten = u'Nguyễn Thị Len', ma_nv = '11', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1590, ho_ten = u'Nguyễn Thị Hiền', ma_nv = '12', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1591, ho_ten = u'Nguyễn Thị Gần', ma_nv = '13', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1592, ho_ten = u'Võ Thị Tương ', ma_nv = '14', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1593, ho_ten = u'Lê Thị Mộng Trinh', ma_nv = '15', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1594, ho_ten = u'Nguyễn Thị Tuyết', ma_nv = '16', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1595, ho_ten = u'Nguyễn Thị Duyên', ma_nv = '17', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1596, ho_ten = u'Nguyễn Thị Thanh Phượng', ma_nv = '18', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1597, ho_ten = u'Phan Thị Tình ', ma_nv = '19', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1598, ho_ten = u'Hà  Thị Tuất', ma_nv = '20', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1599, ho_ten = u'Nguyễn Thị Son ', ma_nv = '21', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1600, ho_ten = u'Lê Thị Minh Tâm', ma_nv = '22', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1601, ho_ten = u'Nguyễn Thị Hồng', ma_nv = '23', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1602, ho_ten = u'Đặng T Ngọc Trinh', ma_nv = '24', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1603, ho_ten = u'Lê Thị Phấn', ma_nv = '25', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1604, ho_ten = u'Nguyễn Thị Hồng 3', ma_nv = '26', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1605, ho_ten = u'Vũ Thị Loan', ma_nv = '27', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1606, ho_ten = u'Nguyễn Thị Thu Hà ', ma_nv = '28', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1607, ho_ten = u'Tạ Thị Kim Uyên ', ma_nv = '29', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1608, ho_ten = u'Nguyễn Thị Thượng Lan ', ma_nv = '30', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1609, ho_ten = u'Lê Thị Vinh', ma_nv = '31', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1610, ho_ten = u'Phan Thị Ngọc ', ma_nv = '32', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1611, ho_ten = u'Trần Thị Diệu', ma_nv = '33', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1612, ho_ten = u'Trương T Hồng Lĩnh', ma_nv = '34', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1613, ho_ten = u'Võ Thị Bích Loan ', ma_nv = '35', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1614, ho_ten = u'Nguyễn Thị  Xuân Thu', ma_nv = '36', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1615, ho_ten = u'Nguyễn Thị  Bé (1 )', ma_nv = '37', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1616, ho_ten = u'Nguyễn Thị  Bé (2 )', ma_nv = '38', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1617, ho_ten = u'Nguyển Kim Anh', ma_nv = '39', gioi_tinh = u'nu'))
l.append(BenhNhan(stt = 1618, ho_ten = u'Đinh Văn Lân', ma_nv = '40', gioi_tinh = u'nam'))

count = 0
for o in l:
	count = count + 1
	try:
		o.save()
	except Exception, error:
		print 'Data import error at row: ', count, error

