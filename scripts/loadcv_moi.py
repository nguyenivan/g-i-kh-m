# -*- coding: utf-8 -*-
from bv.models import CongViec
from django.utils import timezone

l = []
l.append(CongViec(ma_phi=u"CC-KH01", dien_giai=u"DV khám cấp cứu", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-bstn1", dien_giai=u"DV bác sĩ khám và đón bệnh nhân tại nhà '<' 5km", don_gia=200000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-bstn2", dien_giai=u"DV bác sĩ khám và đón bệnh nhân tại nhà '>' 5km", don_gia=300000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-dienform", dien_giai=u"DV điền form du học", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-ddtn1", dien_giai=u"DV điều dưỡng đón bệnh nhân tại nhà '<' 5km", don_gia=50000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-ddtn2", dien_giai=u"DV điều dưỡng đón bệnh nhân tại nhà '>' 5km", don_gia=100000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"KH-mat", dien_giai=u"DV khám mắt", don_gia=50000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-ngio", dien_giai=u"DV khám ngoài giờ", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-ngoai", dien_giai=u"DV khám ngoại TQ", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"KH-nha", dien_giai=u"DV khám nha", don_gia=50000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-noi", dien_giai=u"DV khám nội TQ", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-san", dien_giai=u"DV khám sản/ phụ khoa", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-tmh", dien_giai=u"DV khám tai mũi họng", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-YC", dien_giai=u"DV khám theo yêu cầu", don_gia=80000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-form", dien_giai=u"Khám bệnh theo form du học", don_gia=200000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-capgiay", dien_giai=u"Khám cấp giấy chứng thương", don_gia=80000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-hc2", dien_giai=u"Khám hội chẩn Ngoại Viện (Mời Bác Sĩ CKII)", don_gia=500000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-hcts", dien_giai=u"Khám hội chẩn Ngoại Viện (Mời Tiến Sĩ)", don_gia=700000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-mienphi", dien_giai=u"Khám miễn phí", don_gia=1, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"KH-nuocngoai", dien_giai=u"Khám sức khỏe để đi nước ngoài", don_gia=250000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"KH-tuyendung", dien_giai=u"Khám sức khỏe tuyển dụng, tuyển sinh", don_gia=80000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))

l.append(CongViec(ma_phi=u"DV-DAM500", dien_giai=u"DV truyền đạm chai lớn 500ml", don_gia=250000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"DV-tdtd", dien_giai=u"DV truyền đạm chai nhỏ + dịch 500ml", don_gia=250000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"DV-DAM250", dien_giai=u"DV truyền đạm chai nhỏ 250ml", don_gia=150000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"DV-TD1", dien_giai=u"DV truyền dịch 01 chai", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"DV-TD2", dien_giai=u"DV truyền dịch 02 chai", don_gia=200000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))

l.append(CongViec(ma_phi=u"sanbst", dien_giai=u"Bấm sinh thiết", don_gia=80000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanbtb", dien_giai=u"Bóc tách Bartholine", don_gia=500000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanctdnctc", dien_giai=u"Chọc thoát dịch Naboth CTC", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sandtad", dien_giai=u"Đặt thuốc âm đạo", don_gia=25000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"sandv", dien_giai=u"Đặt vòng", don_gia=200000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"sandmg", dien_giai=u"Đốt Condyloma âm hộ, âm đạo (Đốt mồng gà)", don_gia=0, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"sanddctc", dien_giai=u"Đốt điện CTC (đốt lộ tuyến CTC)", don_gia=330000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanlvcd", dien_giai=u"Lấy vòng có dây", don_gia=110000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanlvk", dien_giai=u"Lấy vòng kín", don_gia=200000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanmrcd", dien_giai=u"May rách cùng đồ", don_gia=500000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanmrtsm", dien_giai=u"May rách TSM", don_gia=500000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanmstmtsm", dien_giai=u"May sửa thẩm mỹ TSM", don_gia=2000000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanncmltcsst", dien_giai=u"Nạo cầm máu lòng TC sau sẩy thai", don_gia=500000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sannstltc", dien_giai=u"Nạo sinh thiết lòng TC", don_gia=500000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanptnk", dien_giai=u"Phá thai nội khoa", don_gia=800000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanab", dien_giai=u"Rạch Abecse Bartholine", don_gia=400000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"sansctc", dien_giai=u"Soi CTC", don_gia=100000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"sanxpctc", dien_giai=u"Xoắn Polype CTC (chảy máu)", don_gia=0, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"sanxpctck", dien_giai=u"Xoắn Polype CTC (không chảy máu)", don_gia=200000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))

l.append(
	CongViec(ma_phi=u"tmhkcmmbm1ben", dien_giai=u"Cầm máu mũi bằng Meroxeo 1 bên (đã bao gồm Meroxeo)", don_gia=250000,
		loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhcmmtbmt", dien_giai=u"Cầm máu mũi trước bằng meche thường 1 bên", don_gia=100000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhcpmdg", dien_giai=u"Cắt polype mũi đơn giản", don_gia=400000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhcrxh1ben", dien_giai=u"Chọc rửa xoang hàm 1 bên", don_gia=300000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhdcm1ben", dien_giai=u"Đốt cuốn mũi 1 bên", don_gia=500000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhdhh", dien_giai=u"Đột họng hạt", don_gia=200000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"tmhklm", dien_giai=u"Khâu VT lợi - môi (chưa gồm VTTH + thuốc)", don_gia=0, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhkvt", dien_giai=u"Khâu VT vành tai đơn giản (chưa gồm VTTH + thuốc)", don_gia=0,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhkvtpt", dien_giai=u"Khâu VT vành tai phức tạp (chưa gồm VTTH + thuốc)", don_gia=0,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhpro", dien_giai=u"Làm proetz (kê)", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhldvh", dien_giai=u"Lấy dị vật họng", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhldvm", dien_giai=u"Lấy dị vật mũi", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhldvt", dien_giai=u"Lấy dị vật tai", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhldvtmhqns", dien_giai=u"Lấy dị vật TMH qua nội soi", don_gia=250000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhlrt1ben", dien_giai=u"Lấy ráy tai (1 bên)", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"nshong-tq", dien_giai=u"Nội soi họng - thanh quản", don_gia=150000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"nsmx", dien_giai=u"Nội soi mũi xoang", don_gia=150000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"nstai", dien_giai=u"Nội soi tai", don_gia=100000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhptlddln1ben", dien_giai=u"PT lấy đường dò luân nhỉ 1 bên (chưa bao gồm VTTH + thuốc)",
	don_gia=800000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhrtltt", dien_giai=u"Rửa tai + làm thuốc tai", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhtvn", dien_giai=u"Thông vòi nhĩ (1 bên)", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhtraxa", dien_giai=u"Trích rạch áp xe Amidan", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhtrmn", dien_giai=u"Trích rạch màng nhỉ", don_gia=200000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"tmhtrtmvt", dien_giai=u"Trích rạch tụ máu vành tai (chưa gồm VTTH + thuốc)", don_gia=100000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"tmhxm", dien_giai=u"Xông mũi họng", don_gia=45000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))

l.append(CongViec(ma_phi=u"ECG", dien_giai=u"Đo điện tim (ECG)", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))

l.append(
	CongViec(ma_phi=u"XQ-bungdung", dien_giai=u"XQ bụng đứng tìm liềm hơi (ASP)", don_gia=50000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-bungkss", dien_giai=u"XQ bụng không sửa soạn (KUB)", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-cangchan2t", dien_giai=u"XQ cẳng chân 2 thế", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-canhtay2t", dien_giai=u"XQ cánh tay 2 thế", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-cochan2t", dien_giai=u"XQ cổ bàn chân 2 thế", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-cotay2t", dien_giai=u"XQ cổ tay 2 thế", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-coxuongdui2", dien_giai=u"XQ cổ xương đùi 2 thế", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-csco2t", dien_giai=u"XQ CS cổ 2 thế", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-csnguc2t", dien_giai=u"XQ CS ngực 2 thế", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-csthatlung", dien_giai=u"XQ CS thắt lưng", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-khopgoi2t", dien_giai=u"XQ khớp gối 2 thế", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-khopvaimt", dien_giai=u"XQ khớp vai một thế", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-khopvaiht", dien_giai=u"XQ khớp vai T+N", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-khungchau1", dien_giai=u"XQ khung chậu một thế", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-khuytay2t", dien_giai=u"XQ khủy tay 2 thế", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XQ-phoi", dien_giai=u"XQ phổi", don_gia=45000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-shuller", dien_giai=u"XQ Shuller hai tai", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XQ-so", dien_giai=u"XQ sọ T+N", don_gia=70000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XQ-xuonggot2t", dien_giai=u"XQ xương gót chân 2 thế", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))

l.append(CongViec(ma_phi=u"SA-b", dien_giai=u"SA bìu", don_gia=70000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"SA-doppler01", dien_giai=u"SA Doppler màu bụng tổng quát", don_gia=70000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-doppler05", dien_giai=u"SA Doppler màu các phần phụ mô bệnh", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-doppler03", dien_giai=u"SA Doppler màu mạch máu cổ và các chi", don_gia=140000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"SA-doppler08", dien_giai=u"SA Doppler màu qua ngã âm đạo", don_gia=80000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-doppler07", dien_giai=u"SA Doppler màu sản phụ khoa", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-doppler02", dien_giai=u"SA Doppler màu tim người lớn và trẻ em", don_gia=140000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-doppler04", dien_giai=u"SA Doppler màu tuyến giáp", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-doppler06", dien_giai=u"SA Doppler màu tuyến vú (tầm soát ung thư vú)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-m", dien_giai=u"SA mắt", don_gia=70000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-4D", dien_giai=u"SA màu 4D sản khoa có đĩa DVD - R", don_gia=220000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-2D", dien_giai=u"SA tổng quát 2 D", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"SA-ttl", dien_giai=u"SA tuyến tiền liệt", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"0", dien_giai=u"0", don_gia=0, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-acth", dien_giai=u"XN ACTH/MÁU", don_gia=110000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-adh", dien_giai=u"XN ADH (Anti Diuretic Hormone)", don_gia=180000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-afp", dien_giai=u"XN AFP", don_gia=90000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-alb", dien_giai=u"XN ALBUMINE", don_gia=30000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-alk", dien_giai=u"XN ALK-Phosphatase", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-amimau", dien_giai=u"XN Amibe (máu)", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-amiphan", dien_giai=u"XN Amibe (phân)", don_gia=80000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-amy", dien_giai=u"XN AMYLASE", don_gia=35000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-amynieu", dien_giai=u"XN AMYLASEN NIỆU (ĐỊNH LƯỢNG)", don_gia=35000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-ana", dien_giai=u"XN ANA Test", don_gia=90000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hav01", dien_giai=u"XN Anti HAV IgG", don_gia=110000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hav02", dien_giai=u"XN Anti HAV IgM", don_gia=110000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hav03", dien_giai=u"XN Anti HAV Total", don_gia=110000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hbc01", dien_giai=u"XN Anti HBc-IgG", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hbc02", dien_giai=u"XN Anti HBc-IgM", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tpoab", dien_giai=u"XN Anti Microsomal (TPO Ab)", don_gia=150000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tgab", dien_giai=u"XN Anti Thyoglobine (TG Ab)", don_gia=150000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-antihcv", dien_giai=u"XN Anti-HCV", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-aslo", dien_giai=u"XN ASLO", don_gia=40000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-bil01", dien_giai=u"XN Bilirubine (Total, Direct, Indirect)", don_gia=60000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-bil02", dien_giai=u"XN Bilirubine Direct", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-bil03", dien_giai=u"XN Bilirubine Indirect", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-bil04", dien_giai=u"XN Bilirubine Total", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-bk", dien_giai=u"XN BK", don_gia=30000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-bnp", dien_giai=u"XN BNP", don_gia=350000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-bun", dien_giai=u"XN BUN", don_gia=30000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-c3", dien_giai=u"XN C3", don_gia=70000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-c4", dien_giai=u"XN C4", don_gia=70000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-ca125", dien_giai=u"XN CA 125", don_gia=140000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ca153", dien_giai=u"XN CA 15-3", don_gia=140000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ca199", dien_giai=u"XN CA 19-9", don_gia=140000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-calc", dien_giai=u"XN Calcitionin", don_gia=140000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-adis", dien_giai=u"XN Cặn Addis", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cand01", dien_giai=u"XN Candida IgG", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cand02", dien_giai=u"XN Candida IgM", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-catotieu", dien_giai=u"XN Catochalamines (Nước tiểu)", don_gia=150000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-damksd", dien_giai=u"XN CẤY ĐÀM + KSĐ", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tieuksd", dien_giai=u"XN CẤY NƯỚC TIỂU + KSĐ", don_gia=260000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-phan", dien_giai=u"XN Cấy phân", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cea", dien_giai=u"XN CEA", don_gia=110000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ceru", dien_giai=u"XN Ceruloplasmin", don_gia=110000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cetnieu", dien_giai=u"XN Ceton niệu", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cho02", dien_giai=u"XN CHOLESTEROL", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cho03", dien_giai=u"XN CHOLESTEROL HDL", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cho04", dien_giai=u"XN CHOLESTEROL LDL", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cho01", dien_giai=u"XN CHOLESTEROL/ HDL-C/ LDL-C/ TRIGLYCERIDES", don_gia=120000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-ckmb", dien_giai=u"XN CK-MB", don_gia=80000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cmvigg", dien_giai=u"XN CMV IgG", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cmvigm", dien_giai=u"XN CMV IgM", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ctm", dien_giai=u"XN Công thức máu NGFL", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cormau", dien_giai=u"XN CORTISOL / MÁU", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cre", dien_giai=u"XN CREATININE", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-crp", dien_giai=u"XN CRP", don_gia=60000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cry211", dien_giai=u"XN CryFra 21_2", don_gia=130000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cry21", dien_giai=u"XN CyFra 2_1", don_gia=110000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-denigg", dien_giai=u"XN Dengue IgG", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-denigm", dien_giai=u"XN Dengue IgM", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-dethiv", dien_giai=u"XN Determine (HIV 1+2)", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tieucau", dien_giai=u"XN điếm tiểu cầu (Plt)", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ddpro", dien_giai=u"XN Điện di Protein", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-dlacet", dien_giai=u"XN Định lượng Acetaminophen", don_gia=140000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-dlalbu", dien_giai=u"XN Định lượng Albumine", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-dlcort", dien_giai=u"XN Định lượng Cortisol", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-dletha", dien_giai=u"XN Định lượng Ethanol", don_gia=140000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-dlglvs", dien_giai=u"XN Định lượng Glucose (Vi sinh/ dịch sinh học)", don_gia=30000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-dlins", dien_giai=u"XN Định lượng Insulin", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-thanhthai", dien_giai=u"XN Độ thanh thải Creatinine niệu", don_gia=70000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-dsdna", dien_giai=u"XN dsDNA", don_gia=260000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-elhiv", dien_giai=u"XN Elisa HIV", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-estra", dien_giai=u"XN Estradiol", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-feht", dien_giai=u"XN Fe huyết thanh", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-ferr", dien_giai=u"XN FERRITIN", don_gia=80000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-fibr", dien_giai=u"XN FIBRINOGEN", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-fola", dien_giai=u"XN Folate", don_gia=140000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-bhcg", dien_giai=u"XN FREE BHCG", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-fsh", dien_giai=u"XN FSH", don_gia=110000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ft3", dien_giai=u"XN FT3", don_gia=90000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ft4", dien_giai=u"XN FT4", don_gia=90000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XNg6pd", dien_giai=u"XN G6PD", don_gia=70000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ggt", dien_giai=u"XN GGT", don_gia=30000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-gaca", dien_giai=u"XN Giun Acarit (giun đũa)", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun01", dien_giai=u"XN Giun Angiostrongylus Cantonensis (giun xoắn)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun02", dien_giai=u"XN Giun Arcaris Lumbricoides (Giun đũa người)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun03", dien_giai=u"XN Giun Cysticercus Cellulosae (Gạo heo)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun04", dien_giai=u"XN Giun Echinococcus Granulosis (Sán dải chó)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun05", dien_giai=u"XN Giun Fasciola Sp. (Sán lá lớn ở gan)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun06", dien_giai=u"XN Giun Filariasis (Giun chỉ)", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun07", dien_giai=u"XN Giun Gnathostoma Spinigerum (Giun đầu gai)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun08", dien_giai=u"XN Giun Paragonimus Sp. (Sán lá phổi)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun09", dien_giai=u"XN Giun Schistosoma Mansoni (Sán máng)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun10", dien_giai=u"XN Giun Strongyloides Stercoralis (Giun lươn)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun11", dien_giai=u"XN Giun Toxocara Canis (Giun đũa chó)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-giun12", dien_giai=u"XN Giun Trichinella Spiralis (Giun bao)", don_gia=90000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-glob", dien_giai=u"XN Globuline", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-gluco", dien_giai=u"XN GLUCOSE (ĐO ĐƯỜNG HUYẾT)", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hpyl04", dien_giai=u"XN H. Pylory (phân)", don_gia=140000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hpy101", dien_giai=u"XN H.Pylory", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hpy102", dien_giai=u"XN H.Pylory IgG", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hpy103", dien_giai=u"XN H.Pylory IgM", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-hba1c", dien_giai=u"XN HbA1c", don_gia=100000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hbcab", dien_giai=u"XN HBcAb (Anti HBc)", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hbeab", dien_giai=u"XN HBeAb (Anti Hbe)", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-hbeag", dien_giai=u"XN HBeAg", don_gia=90000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hbsab", dien_giai=u"XN HBsAb (Anti HBs)", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-hbsag", dien_giai=u"XN HBsAg", don_gia=70000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hcgmau", dien_giai=u"XN HCG Máu", don_gia=110000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-quick", dien_giai=u"XN HCG-Quicktest-nước tiểu (định tính)", don_gia=30000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-hemocy", dien_giai=u"XN Hemocysteine", don_gia=140000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-dlheroinma", dien_giai=u"XN Heroin máu (định lượng)", don_gia=140000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-hsvigg", dien_giai=u"XN Herpes Simples Virus (HSV) IgG", don_gia=100000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-hsvigm", dien_giai=u"XN Herpes Simples Virus (HSV) IgM", don_gia=100000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-huyetsacto", dien_giai=u"XN Huyết sắc tố (máu ẩn) / phân (định lượng)", don_gia=50000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ica", dien_giai=u"XN ICA", don_gia=200000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-idr", dien_giai=u"XN IDR (Mantoux)", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-iga", dien_giai=u"XN IgA", don_gia=70000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-ige", dien_giai=u"XN IgE", don_gia=130000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-igg", dien_giai=u"XN IgG", don_gia=90000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-igm", dien_giai=u"XN IgM", don_gia=90000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-inr", dien_giai=u"XN INR", don_gia=30000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-iondo", dien_giai=u"XN ION ĐỒ (Na, K, Ca, Cl)", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-ionNT", dien_giai=u"XN ION ĐỒ nước tiểu (Na, K, Ca, Cl)", don_gia=60000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-iron", dien_giai=u"XN IRON (FE)", don_gia=25000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-japanigg", dien_giai=u"XN Japanese Encephalitis IgG", don_gia=140000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-japanigm", dien_giai=u"XN Japanese Encephalitis IgM", don_gia=140000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-sotret", dien_giai=u"XN KST Sốt rét", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-lactate", dien_giai=u"XN lactate (Acid lactic)", don_gia=45000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-LE", dien_giai=u"XN LE-Cells", don_gia=35000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-lh", dien_giai=u"XN LH", don_gia=120000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-lipids", dien_giai=u"XN Lipids", don_gia=30000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-apoa1", dien_giai=u"XN LipoProtein - APO - A1", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-apob", dien_giai=u"XN LipoProtein - APO - B", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-lipo", dien_giai=u"XN LipoProtetin", don_gia=130000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-magne", dien_giai=u"XN MAGNE", don_gia=30000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-albnieu", dien_giai=u"XN Micro Albumine niệu", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-mbhto", dien_giai=u"XN mô bệnh học 1 mẫu (lọ to)", don_gia=350000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-mbhtonho", dien_giai=u"XN mô bệnh học 1 mẫu lọ to + 1 lọ nhỏ", don_gia=410000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-mbhthemlo", dien_giai=u"XN mô bệnh học từ 2 lọ trở lên (đối với lọ nhỏ) thì thu thêm 1 lọ",
		don_gia=140000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-mophan", dien_giai=u"XN Mỡ trong phân", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-nh3", dien_giai=u"XN NH3", don_gia=70000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-mauabo", dien_giai=u"XN NHÓM MÁU ABO", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-maurh", dien_giai=u"XN nhóm máu Rh", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-gram", dien_giai=u"XN Nhuộm Gram", don_gia=40000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-digoxin", dien_giai=u"XN Nồng độ Digoxin", don_gia=140000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-pap", dien_giai=u"XN P.A.P", don_gia=110000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"0", dien_giai=u"XN PAP (U tiền liệt tuyến)", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-pmear", dien_giai=u"XN Paps Mear", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-dthcv", dien_giai=u"XN PCP HCV (ĐỊNH TÍNH)", don_gia=450000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-dthbv", dien_giai=u"XN PCR HBV (ĐỊNH TÍNH)", don_gia=250000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-phetmau", dien_giai=u"XN Phết máu ngoại biên", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-phospho", dien_giai=u"XN Phospho", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-proges", dien_giai=u"XN Progesterone", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-prolac", dien_giai=u"XN Prolactine", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-proteinnieu", dien_giai=u"XN PROTEIN NIỆU (ĐỊNH LƯỢNG)", don_gia=30000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-proteintota", dien_giai=u"XN Protein Total", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-psa", dien_giai=u"XN PSA", don_gia=120000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-pth", dien_giai=u"XN PTH", don_gia=130000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-rf", dien_giai=u"XN RF", don_gia=80000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-rubeigg", dien_giai=u"XN Rubella IgG", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-rubeigm", dien_giai=u"XN Rubella IgM", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-sgotsgpt", dien_giai=u"XN SGOT (AST) / SGPT (ALT)", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-sgot", dien_giai=u"XN SGOT / AST", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-sgpt", dien_giai=u"XN SGPT / ALT", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-soi01", dien_giai=u"XN soi phân tìm nấm", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-soi06", dien_giai=u"XN soi tìm lậu cầu", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-soi07", dien_giai=u"XN soi tươi dịch âm đạo", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-soi08", dien_giai=u"XN soi tươi huyết trắng", don_gia=60000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tck", dien_giai=u"XN TCK", don_gia=45000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tck", dien_giai=u"XN TCK", don_gia=35000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-mbhTbkim", dien_giai=u"XN tế bào chọc hút kim nhỏ FNA", don_gia=100000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-mbhTBmo", dien_giai=u"XN tế bào mới TT Ung bướu", don_gia=100000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-testo", dien_giai=u"XN Testosterol", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-ag", dien_giai=u"XN tỉ lệ A/G", don_gia=30000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-proth", dien_giai=u"XN TỈ LỆ PROTHROMBINE", don_gia=50000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-nhuombk", dien_giai=u"XN tìm BK (nhuộm soi, không cấy)", don_gia=40000, loai_cv=u"thu_thuat",
		service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-kst", dien_giai=u"XN tìm KST đường ruột/ phân", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tptnt", dien_giai=u"XN Tổng phân tích nước tiểu (10 thông số)", don_gia=40000,
	loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-toxoigg", dien_giai=u"XN Toxocara Gondii IgG", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-toxoigm", dien_giai=u"XN Toxocara Gondii IgM", don_gia=90000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tq", dien_giai=u"XN TQ", don_gia=35000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tqtck", dien_giai=u"XN TQ / TCK", don_gia=70000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-cho05", dien_giai=u"XN TRIGLYCERIDES", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-trop", dien_giai=u"XN TROPONIN I", don_gia=120000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-tstc", dien_giai=u"XN TS / TC", don_gia=50000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tsh", dien_giai=u"XN TSH", don_gia=90000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-tshr", dien_giai=u"XN TSH Receptor (Trab)", don_gia=200000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-urenieu", dien_giai=u"XN URE NIỆU (ĐỊNH LƯỢNG)", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-urea", dien_giai=u"XN UREA", don_gia=30000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-uric", dien_giai=u"XN URIC ACIDE", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-vdrl", dien_giai=u"XN VDRL", don_gia=60000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-vit", dien_giai=u"XN Vitamin B12", don_gia=140000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-vldl", dien_giai=u"XN VLDL Cholesterol", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(CongViec(ma_phi=u"XN-vs01", dien_giai=u"XN VS giờ thứ nhất", don_gia=30000, loai_cv=u"thu_thuat",
	service_line=u'dich_vu'))
l.append(
	CongViec(ma_phi=u"XN-widal", dien_giai=u"XN Widal", don_gia=60000, loai_cv=u"thu_thuat", service_line=u'dich_vu'))

count = 0
for o in l:
	count = count + 1
	try:
		o.save()
		print count, o
	except Exception, error:
		print 'Data import error at row: ', count, error
