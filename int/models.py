# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import html
from django import forms
from django.db.models import Q
from django.core.cache import cache
from django.utils import timezone
HH_CONFIG_CHOICES = (
	("field17","WBC Count"),
	("field19","LYM Count"),
	("field20","MID Count"),
	("field21","GRAN Count"),
	("field23","RBC Count"),
	("field24","HGB Value"),
	("field25","HCT Value"),
	("field26","MCV Value"),
	("field27","MCH Value"),
	("field28","MCHC Value"),
	("field29","RDW Value"),
	("field30","PLT Count"),
	("field31","MPV Value"),
	("field32","PCT Value"),
	("field33","PDW Value"),
	("field35","LYM % Value"),
	("field36","MID % Value"),
	("field37","GRAN % Value"),
	("field41","R4 WBC Flag"),
	("field42","GR3 WBC Flag"),
	("field43","MR3 WBC Flag"),
	("field44","MR2 WBC Flag"),
	("field45","LR2 WBC Flag"),
	("field46","R1 WBC Flag"),
	("field47","R0 WBC Flag"),
)

SH_CONFIG_CHOICES = (
	("field01","Glucose"),
	("field02","Ure"),
	("field03","Creatine"),
	("field04","Cholesterol  ( Cho )"),
	("field05","Triglycerid (Tri )"),
	("field06","AST ( SGOT )"),
	("field07","ALT (SGPT )"),
	("field09","LDL – C"),
	("field11","GGT"),
	("field12","Acid Uric ( A.U )"),
	("field19","HDL – C"),
)

class CauHinhHH(models.Model):
	class Meta:
		verbose_name = u'HH cấu hình'
		verbose_name_plural = u'HH danh sách cấu hình'

	def __unicode__(self):
		return self.ten

	def save(self, *args, **kwargs):
		super(CauHinhHH, self).save(*args, **kwargs)
		if self.mac_dinh:
			for o in CauHinhHH.objects.filter(~Q(pk=self.pk)):
				o.mac_dinh = False
				o.save()
	def delete(self, *args, **kwargs):
		super(CauHinhHH, self).delete(*args, **kwargs)
		qs = CauHinhHH.objects.all().order_by('-ngay_tao')[:1]
		if qs: 
			qs[0].mac_dinh = True
			qs[0].save()

	ten = models.CharField(max_length=50)
	mac_dinh = models.BooleanField(verbose_name=u'mặc định', default=True)
	ngay_tao = models.DateTimeField(verbose_name=u'cập nhật', auto_now_add=True)

class KhoangGiaTriHH(models.Model):
	class Meta:
		verbose_name = u'HH khoảng giá trị'
		verbose_name_plural = u'HH các khoảng giá trị'
		unique_together = ["gia_tri", "cau_hinh"]

	def __unicode__(self):
		return self.get_gia_tri_display()

	def save(self, *args, **kwargs):
		super(KhoangGiaTriHH, self).save(*args, **kwargs)
		# Invalidate cache
		cache.delete('cauhinh_%s' % self.cau_hinh_id)

	def delete(self, *args, **kwargs):
		# Invalidate cache
		cache.delete('cauhinh_%s' % self.cau_hinh_id)
		super(KhoangGiaTriHH, self).delete(*args, **kwargs)

	cau_hinh=models.ForeignKey('CauHinhHH', verbose_name=u'cấu hình')
	gia_tri=models.CharField(max_length=50, verbose_name=u'giá trị', choices=HH_CONFIG_CHOICES)
	ngay_ghi = models.DateTimeField(verbose_name=u'cập nhật', auto_now=True)
	max_value =models.FloatField(verbose_name=u'max value', blank=True, null=True)
	min_value =models.FloatField(verbose_name=u'min value', blank=True, null=True)

class CauHinhSH(models.Model):
	class Meta:
		verbose_name = u'SH cấu hình'
		verbose_name_plural = u'SH danh sách cấu hình'

	def __unicode__(self):
		return self.ten

	def save(self, *args, **kwargs):
		super(CauHinhSH, self).save(*args, **kwargs)
		if self.mac_dinh:
			for o in CauHinhSH.objects.filter(~Q(pk=self.pk)):
				o.mac_dinh = False
				o.save()

	def delete(self, *args, **kwargs):
		super(CauHinhSH, self).delete(*args, **kwargs)
		qs = CauHinhSH.objects.all().order_by('-ngay_tao')[:1]
		if qs: 
			qs[0].mac_dinh = True
			qs[0].save()

	ten = models.CharField(max_length=50)
	mac_dinh = models.BooleanField(verbose_name=u'mặc định', default=True)
	ngay_tao = models.DateTimeField(verbose_name=u'cập nhật', auto_now_add=True)

class KhoangGiaTriSH(models.Model):
	class Meta:
		verbose_name = u'SH khoảng giá trị'
		verbose_name_plural = u'SH các khoảng giá trị'
		unique_together = ["gia_tri", "cau_hinh"]

	def __unicode__(self):
		return self.get_gia_tri_display()

	def save(self, *args, **kwargs):
		super(KhoangGiaTriSH, self).save(*args, **kwargs)
		# Invalidate cache
		cache.delete('cauhinhsh_%s' % self.cau_hinh_id)

	def delete(self, *args, **kwargs):
		# Invalidate cache
		cache.delete('cauhinhsh_%s' % self.cau_hinh_id)
		super(KhoangGiaTriSH, self).delete(*args, **kwargs)


	cau_hinh=models.ForeignKey('CauHinhSH', verbose_name=u'cấu hình')
	gia_tri=models.CharField(max_length=50, verbose_name=u'giá trị', choices=SH_CONFIG_CHOICES)
	ngay_ghi = models.DateTimeField(verbose_name=u'cập nhật', auto_now=True)
	max_value =models.FloatField(verbose_name=u'max value', blank=True, null=True)
	min_value =models.FloatField(verbose_name=u'min value', blank=True, null=True)

class SinhHoa(models.Model):
	class Meta:
		verbose_name = _(u'sinh hóa')
		verbose_name_plural = _(u'các kết quả sinh hóa')

	@property
	def weeks_old(self):
		"""Return how many week old is this"""
		if self.ngay_tao is None:
			return None
		ngay_tao = self.ngay_tao
		if timezone.is_naive(ngay_tao):
			timezone.make_aware(ngay_tao, timezone.get_default_timezone())
		weeks = int( (timezone.now() - ngay_tao).days / 7 ) + 1
		return weeks

	def save(self, *args, **kwargs):
		if self.cau_hinh is None:
			qs=CauHinhSH.objects.filter(mac_dinh=True)[:1]
			if qs:
				self.cau_hinh = qs[0]

		super(SinhHoa, self).save(*args, **kwargs)
		if not self.specimen_id.strip(): # Empty ID
			return
		if self.accepted == True:
			SinhHoa.objects.filter(Q(specimen_id=self.specimen_id),\
				~Q(pk=self.pk)).update(accepted=False)
		elif self.accepted == None:
			SinhHoa.objects.filter(Q(specimen_id=self.specimen_id), \
				Q(accepted=False)).update(accepted=None)

	def __unicode__(self):
		return u"%s-%s-%s" % (self.disk, self.position, self.specimen_id)

	cau_hinh=models.ForeignKey('CauHinhSH', verbose_name=u'cấu hình', blank=True, null=True)
	accepted = models.NullBooleanField(verbose_name=u'Accepted', blank=True, null=True, default=None)
	sequence = models.IntegerField(blank= True,  null=True, verbose_name=u'sequence')
	disk = models.IntegerField(blank= True,  null=True, verbose_name=u'disk')
	position = models.IntegerField(blank= True,  null=True, verbose_name=u'position')
	specimen_id = models.CharField(max_length=20, blank= True, verbose_name=u'specimen id')
	nhan_xet = models.TextField(blank= True, verbose_name=u'nhận xét')
	ngay_tao = models.DateTimeField(verbose_name=u'ngày tạo', auto_now_add=True)
	ngay_ghi = models.DateTimeField(verbose_name=u'cập nhật', auto_now=True)
	field01 = models.FloatField(blank=True, null=True, verbose_name=u'Glucose') #1
	field02 = models.FloatField(blank=True, null=True, verbose_name=u'Ure')
	field03 = models.FloatField(blank=True, null=True, verbose_name=u'Creatine')
	field04 = models.FloatField(blank=True, null=True, verbose_name=u'Cholesterol  ( Cho )')
	field05 = models.FloatField(blank=True, null=True, verbose_name=u'Triglycerid (Tri )')
	field06 = models.FloatField(blank=True, null=True, verbose_name=u'AST ( SGOT )') # Gan 1
	field07 = models.FloatField(blank=True, null=True, verbose_name=u'ALT (SGPT )') # Gan 2
	field08 = models.FloatField(blank=True, null=True, verbose_name=u'field08')
	field09 = models.FloatField(blank=True, null=True, verbose_name=u'LDL – C')
	field10 = models.FloatField(blank=True, null=True, verbose_name=u'field10')
	field11 = models.FloatField(blank=True, null=True, verbose_name=u'GGT') # Gan 3
	field12 = models.FloatField(blank=True, null=True, verbose_name=u'Acid Uric ( A.U )') # Gout
	field13 = models.FloatField(blank=True, null=True, verbose_name=u'field13')
	field14 = models.FloatField(blank=True, null=True, verbose_name=u'field14')
	field15 = models.FloatField(blank=True, null=True, verbose_name=u'field15')
	field16 = models.FloatField(blank=True, null=True, verbose_name=u'field16')
	field17 = models.FloatField(blank=True, null=True, verbose_name=u'field17')
	field18 = models.FloatField(blank=True, null=True, verbose_name=u'field18')
	field19 = models.FloatField(blank=True, null=True, verbose_name=u'HDL – C')
	field20 = models.FloatField(blank=True, null=True, verbose_name=u'field20')
	field21 = models.FloatField(blank=True, null=True, verbose_name=u'field21')
	field22 = models.FloatField(blank=True, null=True, verbose_name=u'field22')
	field23 = models.FloatField(blank=True, null=True, verbose_name=u'field23')
	field24 = models.FloatField(blank=True, null=True, verbose_name=u'field24')
	field25 = models.FloatField(blank=True, null=True, verbose_name=u'field25')
	field26 = models.FloatField(blank=True, null=True, verbose_name=u'field26')
	field27 = models.FloatField(blank=True, null=True, verbose_name=u'field27')
	field28 = models.FloatField(blank=True, null=True, verbose_name=u'field28')
	field29 = models.FloatField(blank=True, null=True, verbose_name=u'field29')
	field30 = models.FloatField(blank=True, null=True, verbose_name=u'field30')
	field31 = models.FloatField(blank=True, null=True, verbose_name=u'field31')
	field32 = models.FloatField(blank=True, null=True, verbose_name=u'field32')
	field33 = models.FloatField(blank=True, null=True, verbose_name=u'field33')
	field34 = models.FloatField(blank=True, null=True, verbose_name=u'field34')
	field35 = models.FloatField(blank=True, null=True, verbose_name=u'field35')
	field36 = models.FloatField(blank=True, null=True, verbose_name=u'field36')
	field37 = models.FloatField(blank=True, null=True, verbose_name=u'field37')
	field38 = models.FloatField(blank=True, null=True, verbose_name=u'field38')
	field39 = models.FloatField(blank=True, null=True, verbose_name=u'field39')
	field40 = models.FloatField(blank=True, null=True, verbose_name=u'field40')
	field41 = models.FloatField(blank=True, null=True, verbose_name=u'field41')
	field42 = models.FloatField(blank=True, null=True, verbose_name=u'field42')
	field43 = models.FloatField(blank=True, null=True, verbose_name=u'field43')
	field44 = models.FloatField(blank=True, null=True, verbose_name=u'field44')
	field45 = models.FloatField(blank=True, null=True, verbose_name=u'field45')
	field46 = models.FloatField(blank=True, null=True, verbose_name=u'field46')
	original = models.CharField(max_length = 500, blank=True, null=True, verbose_name=u'original message')


class HuyetHoc(models.Model):
	class Meta:
			verbose_name = _(u'huyết học')
			verbose_name_plural = _(u'các kết quả huyết học')
			#ordering =['field04']

	@property
	def weeks_old(self):
		"""Return how many week old is this"""
		if self.ngay_tao is None:
			return None
		ngay_tao = self.ngay_tao
		if timezone.is_naive(ngay_tao):
			timezone.make_aware(ngay_tao, timezone.get_default_timezone())
		weeks = int( (timezone.now() - ngay_tao).days / 7 ) + 1
		return weeks

	def save(self, *args, **kwargs):
		if self.cau_hinh is None:
			qs=CauHinhHH.objects.filter(mac_dinh=True)[:1]
			if qs:
				self.cau_hinh = qs[0]

		super(HuyetHoc, self).save(*args, **kwargs)

		if self.field09.strip() == '':
			return
		if self.accepted == True:
			HuyetHoc.objects.filter(Q(field09=self.field09), \
				~Q(pk=self.pk)).update(accepted=False)
		elif self.accepted == None:
			HuyetHoc.objects.filter(Q(field09=self.field09), \
				Q(accepted=False)).update(accepted=None)


	def __unicode__(self):
			return "%s-%s" % (self.field02, self.field04)
	cau_hinh=models.ForeignKey('CauHinhHH', verbose_name=u'cấu hình', blank=True, null=True)
	accepted = models.NullBooleanField(verbose_name=u'Accepted', blank=True, null=True, default=None)
	nhan_xet = models.TextField(blank= True, verbose_name=u'nhận xét')
	ngay_tao = models.DateTimeField(verbose_name=u'ngày tạo', auto_now_add=True)
	ngay_ghi = models.DateTimeField(verbose_name=u'cập nhật', auto_now=True)
	field01 = models.CharField(max_length=50, blank=True, verbose_name=u'Message Type')
	field02 = models.CharField(max_length=50, blank=True, verbose_name=u'Instrument Type')
	field03 = models.CharField(max_length=50, blank=True, verbose_name=u'Serial Number')
	field04 = models.IntegerField(blank=True, null=True,verbose_name=u'Sequence Number')
	field05 = models.IntegerField(blank=True, null=True,verbose_name=u'Specimen Type')
	field06 = models.CharField(max_length=50, blank=True, verbose_name=u'Operator ID')
	field07 = models.CharField(max_length=50, blank=True, verbose_name=u'Specimen Date')
	field08 = models.CharField(max_length=50, blank=True, verbose_name=u'Specimen Time')
	field09 = models.CharField(max_length=50, blank=True, verbose_name=u'Specimen ID')
	field10 = models.CharField(max_length=50, blank=True, verbose_name=u'Specimen Name')
	field11 = models.CharField(max_length=50, blank=True, verbose_name=u'Specimen Sex')
	field12 = models.CharField(max_length=50, blank=True, verbose_name=u'Specimen DOE')
	field13 = models.CharField(max_length=50, blank=True, verbose_name=u'Dr Name')
	field14 = models.CharField(max_length=50, blank=True, verbose_name=u'Collect Date')
	field15 = models.CharField(max_length=50, blank=True, verbose_name=u'Collect Time')
	field16 = models.CharField(max_length=50, blank=True, verbose_name=u'Comment')
	field17 = models.FloatField(blank=True, null=True,verbose_name=u'WBC Count') #1
	field18 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Field')
	field19 = models.FloatField(blank=True, null=True,verbose_name=u'LYM Count')
	field20 = models.FloatField(blank=True, null=True,verbose_name=u'MID Count')
	field21 = models.FloatField(blank=True, null=True,verbose_name=u'GRAN Count')
	field22 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Field')
	field23 = models.FloatField(blank=True, null=True,verbose_name=u'RBC Count') #2
	field24 = models.FloatField(blank=True, null=True,verbose_name=u'HGB Value') #2.1
	field25 = models.FloatField(blank=True, null=True,verbose_name=u'HCT Value') #2.2
	field26 = models.FloatField(blank=True, null=True,verbose_name=u'MCV Value')
	field27 = models.FloatField(blank=True, null=True,verbose_name=u'MCH Value')
	field28 = models.FloatField(blank=True, null=True,verbose_name=u'MCHC Value')
	field29 = models.FloatField(blank=True, null=True,verbose_name=u'RDW Value')
	field30 = models.FloatField(blank=True, null=True,verbose_name=u'PLT Count')
	field31 = models.FloatField(blank=True, null=True,verbose_name=u'MPV Value')
	field32 = models.FloatField(blank=True, null=True,verbose_name=u'PCT Value') #3
	field33 = models.FloatField(blank=True, null=True,verbose_name=u'PDW Value')
	field34 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Field')
	field35 = models.FloatField(blank=True, null=True,verbose_name=u'LYM % Value') #1.1
	field36 = models.FloatField(blank=True, null=True,verbose_name=u'MID % Value') #1.2
	field37 = models.FloatField(blank=True, null=True,verbose_name=u'GRAN % Value') #1.3
	field38 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Field')
	field39 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Field')
	field40 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Field')
	field41 = models.IntegerField(blank=True, null=True,verbose_name=u'R4 WBC Flag')
	field42 = models.IntegerField(blank=True, null=True,verbose_name=u'GR3 WBC Flag')
	field43 = models.IntegerField(blank=True, null=True,verbose_name=u'MR3 WBC Flag')
	field44 = models.IntegerField(blank=True, null=True,verbose_name=u'MR2 WBC Flag')
	field45 = models.IntegerField(blank=True, null=True,verbose_name=u'LR2 WBC Flag')
	field46 = models.IntegerField(blank=True, null=True,verbose_name=u'R1 WBC Flag')
	field47 = models.IntegerField(blank=True, null=True,verbose_name=u'R0 WBC Flag')
	field48 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field49 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field50 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field51 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field52 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field53 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field54 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field55 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field56 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field57 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field58 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field59 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field60 = models.IntegerField(blank=True, null=True,verbose_name=u'Spare Flags')
	field61 = models.IntegerField(blank=True, null=True,verbose_name=u'LRI Flag')
	field62 = models.IntegerField(blank=True, null=True,verbose_name=u'URI Flag')
	field63 = models.IntegerField(blank=True, null=True,verbose_name=u'WBC Lower Meniscus Time')
	field64 = models.IntegerField(blank=True, null=True,verbose_name=u'WBC Upper Meniscus Time')
	field65 = models.IntegerField(blank=True, null=True,verbose_name=u'RBC Lower Meniscus Time')
	field66 = models.IntegerField(blank=True, null=True,verbose_name=u'RBC Upper Meniscus Time')
	field67 = models.IntegerField(blank=True, null=True,verbose_name=u'Recount RBC Lower Meniscus Time')
	field68 = models.IntegerField(blank=True, null=True,verbose_name=u'Recount RBC Upper Meniscus Time')
	field69 = models.IntegerField(blank=True, null=True,verbose_name=u'Limits Set')
	field70 = models.CharField(max_length=50, blank=True, verbose_name=u'Sample Mode')
	field71 = models.IntegerField(blank=True, null=True,verbose_name=u'RBC Meteri Fault Flag')
	field72 = models.IntegerField(blank=True, null=True,verbose_name=u'WBC Meteri Fault Flag')
	field73 = models.IntegerField(blank=True, null=True,verbose_name=u'Sampling Error/incomplete Aspiration Flag')
	field74 = models.IntegerField(blank=True, null=True,verbose_name=u'Units Set Field')
	field75 = models.CharField(max_length=2, blank=True, null=True)
	original = models.CharField(max_length = 500, blank=True, null=True, verbose_name=u'original message')

