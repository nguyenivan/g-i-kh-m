#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import serial
import traceback
import os,sys
import signal

DEBUG = True

PORT = '/dev/ttyS0'
LINE_START = chr(0x02)
LINE_END = chr(0x03)
FILE_NAME = 'ketqua.csv'
USAGE = "Cú pháp: python xetnghiem.py [serial port]\nVí dụ: python xetnghiem.py /dev/tty.usbserial (nếu không chỉ định port, default=/dev/ttyS0)"
HEADER_LIST =("Message Type","Instrument Type","Serial Number","Sequence Number","Specimen Type","Operator ID","Specimen Date","Specimen Time","Specimen ID","Specimen Name","Specimen Sex","Specimen DOE","Dr Name","Collect Date","Collect Time","Comment","WBC Count","Spare Field","LYM Count","MID Count","GRAN Count","Spare Field","RBC Count","HGB Value","HCT Value","MCV Value","MCH Value","MCHC Value","RDW Value","PLT Count","MPV Value","PCT Value","PDW Value","Spare Field","LYM % Value","MID % Value","GRAN % Value","Spare Field","Spare Field","Spare Field","R4 WBC Flag","GR3 WBC Flag","MR3 WBC Flag","MR2 WBC Flag","LR2 WBC Flag","R1 WBC Flag","R0 WBC Flag","Spare Flags","LRI Flag","URI Flag","WBC Lower Meniscus Time","WBC Upper Meniscus Time","RBC Lower Meniscus Time","RBC Upper Meniscus Time","Recount RBC Lower Meniscus Time","Recount RBC Upper Meniscus Time","Limits Set","Sample Mode","RBC Meteri Fault Flag","WBC Meteri Fault Flag","Sampling Error/incomplete Aspiration Flag","Units Set Field")

port = serial.Serial(timeout=0.5,rtscts=True)

def clean_up(signum, frame):
	global port
	try:
		port.close()
	except Exception:
		errLogger.error(traceback.format_exc())
	exit(1)

signal.signal(signal.SIGTERM, clean_up)
signal.signal(signal.SIGINT, clean_up)
def dau_dong(f):
	for x in xrange(0,len(HEADER_LIST)):
		if x>0:
			f.write('\t')
		f.write(HEADER_LIST[x])

def xu_ly(s,f):
	tokens = s.split(',')
	for x in xrange(0,len(tokens)):
		if x>0:
			f.write('\t')
		f.write(tokens[x])

def main_program(serial_port):
		with open(FILE_NAME,'w') as f:
			dau_dong(f)
			buf = ''
			connected = True
			port.port = serial_port
			while True:
				try:
					if not port.isOpen():
						port.open()
						print(u'Cổng COM vừa được kết nối')
						connected = True
					c = port.read()
					if c != LINE_END:
						buf = buf + c
					else:
						s = buf.split(LINE_START)[-1]
						print(s)
						f.write('\n')
						xu_ly(s,f)
						buf = ''
				except serial.SerialException:
					if port.isOpen():
						port.close()
					if connected:
						connected = False
						print(u'Lỗi kết nối cổng COM')
						print(traceback.format_exc())
				except Exception:
					print(traceback.format_exc())
					time.sleep(3)

"""
To Test 
socat -d -d pty,raw,echo=0, pty,raw,echo=0
cat sample.txt > /dev/ttys003
"""

"""
To Install The Script: tao file /etc/init/xn.conf

description "Dich Vu Ket Noi Data"
author  "Le D Nguyen<nguyen@qhoach.com>"

start on runlevel [234]
stop on runlevel [0156]

chdir /var/www/onsite
exec /var/www/onsite/int/xn.py
respawn

"""

if __name__ == "__main__":
	if len(sys.argv) > 2:
		print USAGE
		sys.exit(1)
	serial_port = PORT if len(sys.argv) == 1 else sys.argv[1]
	print serial_port
	main_program(serial_port)