# -*- coding: utf-8 -*-
"""
field01 = models.CharField(max_length=50, blank=True)
field01 = models.IntegerField(blank=True, null=True)
"""
default_s='"   "," CD1700","------------",0530,00,"---","09/09/13","10:18","     1800","    NG TH K THOA"," ","  /  /  ","                      ","  /  ","  :  ","                ",00070,-----,00031,00011,00028,-----,00489,00135,00423,00865,00276,00319,00131,00427,00089,00038,00154,-----,00438,00156,00406,-----,-,-,0,1,0,0,0,0,0,-,-,-,-,-,-,-,-,-,-,-,-,-,00,00,04560,01834,06999,05057,00000,00000,1,"O",0,0,0,1,52'
ten_list =("Message Type","Instrument Type","Serial Number","Sequence Number","Specimen Type","Operator ID","Specimen Date","Specimen Time","Specimen ID","Specimen Name","Specimen Sex","Specimen DOE","Dr Name","Collect Date","Collect Time","Comment","WBC Count","Spare Field","LYM Count","MID Count","GRAN Count","Spare Field","RBC Count","HGB Value","HCT Value","MCV Value","MCH Value","MCHC Value","RDW Value","PLT Count","MPV Value","PCT Value","PDW Value","Spare Field","LYM % Value","MID % Value","GRAN % Value","Spare Field","Spare Field","Spare Field","R4 WBC Flag","GR3 WBC Flag","MR3 WBC Flag","MR2 WBC Flag","LR2 WBC Flag","R1 WBC Flag","R0 WBC Flag","Spare Flags","Spare Flags","Spare Flags","Spare Flags","Spare Flags","Spare Flags","Spare Flags","Spare Flags","Spare Flags","Spare Flags","Spare Flags","Spare Flags","Spare Flags","LRI Flag","URI Flag","WBC Lower Meniscus Time","WBC Upper Meniscus Time","RBC Lower Meniscus Time","RBC Upper Meniscus Time","Recount RBC Lower Meniscus Time","Recount RBC Upper Meniscus Time","Limits Set","Sample Mode","RBC Meteri Fault Flag","WBC Meteri Fault Flag","Sampling Error/incomplete Aspiration Flag","Units Set Field")
SHIFT_COUNT_UNIT_LABEL={
	17: (1,'K/uL'),
	19: (1,'K/ul'),
	20: (1,'K/ul'),
	21: (1,'K/ul'),
	23: (2,'M/uL'),
	24: (1,'g/dL'),
	25: (1,'%'),
	26: (1,'fL'),
	27: (1,'pg'),
	28: (1,'g/dL'),
	29: (1,'%'),
	30: (0,'K/ul'),
	31: (1,'fL'),
	32: (2,'%'),
	33: (1,'10(GSD)'),
	35: (1,'%'),
	36: (1,'%'),
	37: (1,'%')
}

l = len(ten_list)
def gen_fields(s=default_s):
	count = 1
	for t in s.split(','):
		if t.startswith('"') and t.endswith('"'):
			if count < l+1:
				print "field%02d = models.CharField(max_length=50, blank=True, verbose_name=u'%s')" % (count, ten_list[count-1])
			else:
				print "field%02d = models.CharField(max_length=50, blank=True)"% (count,)
		else:
			field_type = 'FloatField' if count in SHIFT_COUNT_UNIT_LABEL.keys() else 'IntegerField'

			if count < l+1:
				print "field%02d = models.%s(blank=True, null=True,verbose_name=u'%s')" % (count, field_type, ten_list[count-1])
			else:
				print "field%02d = models.%s(blank=True, null=True)"% (count, field_type)
		count = count + 1

LIST_HITACHI717 = {
	1:"Glucose",
	2:"Ure",
	3:"Creatine",
	4:"Cholesterol  ( Cho )",
	5:"Triglycerid (Tri )",
	6:"AST ( SGOT )",
	7:"ALT (SGPT )",
	9:"LDL – C",
	11:"GGT",
	12:"Acid Uric ( A.U )",
	19:"HDL – C",
}
def gen_hitachi717():
	for x in xrange(1,47):
		verbose_name = LIST_HITACHI717.get(x) if LIST_HITACHI717.get(x) else 'field%02d' % x 
		print "field%02d = models.FloatField(blank=True, null=True, verbose_name=u'%s')" % (x, verbose_name)

gen_hitachi717()



