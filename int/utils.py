from int.models import HuyetHoc, SinhHoa
from copy import deepcopy
from django.db.models import CharField, IntegerField, FloatField
# 75 Fields
TYPE_LIST = [str, str, str, int, int, str, str, str, str, str, str, str, str, str, str, str, float, int, float, float, float, int, float, float, float, float, float, float, float, float, float, float, float, int, float, float, float, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, int, str, int, int, int, int, str]
SHIFT_COUNT_UNIT_LABEL={
	17: (1,'K/uL'),
	19: (1,'K/ul'),
	20: (1,'K/ul'),
	21: (1,'K/ul'),
	23: (2,'M/uL'),
	24: (1,'g/dL'),
	25: (1,'%'),
	26: (1,'fL'),
	27: (1,'pg'),
	28: (1,'g/dL'),
	29: (1,'%'),
	30: (0,'K/ul'),
	31: (1,'fL'),
	32: (2,'%'),
	33: (1,'10(GSD)'),
	35: (1,'%'),
	36: (1,'%'),
	37: (1,'%')
}

SH_AUTO_TX_SPLIT = {
	'sequence':(3,7),
	'disk':(8,9),
	'position': (9,11),
	'specimen_id': (12,23),
}

SH_AUTO_TX_RESULTS = (24,1000)
SH_RESULT_LENGTH = 9

SH_TYPE_DICT = {}
for field in SinhHoa._meta.fields:
	SH_TYPE_DICT[field.name] = field

def create_sh(s, errLogger):
	sh_dict = deepcopy(SH_AUTO_TX_SPLIT)
	results = s[SH_AUTO_TX_RESULTS[0]: SH_AUTO_TX_RESULTS[1]]
	for x in xrange(0,len(results)/SH_RESULT_LENGTH):
		r = results[x*9: (x+1)*9]
		test_code = int(r[0:2])
		# test_result = float(r[2,8])
		# REMEMBER TO HANDLE THE LAST CHARACTER ERROR CODE HERE
		sh_dict['field%02d' % test_code] = (x*9+2+24, (x+1)*9-1+24)
	sh = SinhHoa()
	sh.original=s
	for key,value in sh_dict.iteritems():
		t = s[value[0]:value[1]].strip()
		if not t: continue
		field = SH_TYPE_DICT[key]
		try:
			if type(field) in (IntegerField, FloatField):
				negative = t[:1] == '-'
				if negative: t = t[1:]
				sh.__dict__[key] =  field.to_python(t) * (-1 if negative else 1)
			else:
				sh.__dict__[key] = field.to_python(t)
		except ValueError:
				errLogger.error('Error while trying to convert value "%s" to a number.\n%s' % (t,s))
		except Exception, e:
				errLogger.error('Unhandled error %s in utils.create_sh. Value %s' % (e, t))
	return sh
	"""Test"
from int import utils
import sys, logging
errLogger = logging.getLogger('Module SH')
handler = logging.StreamHandler(sys.stdout)
errLogger.addHandler(handler)
sl = ['02N   1 0 1              1   4.2 ', \
'03N   1 0 1        PKAP  1   4.9  2   5.4  3    61  4   4.2  5   2.9  6    59  7    54']
shl = [utils.create_sh(s, errLogger) for s in sl]
for sh in shl: print (sh, sh.sequence, sh.disk, sh.specimen_id, sh.position, sh.field01)
	"""

SH_FUNCTION_CODES = {
	2:create_sh,
}

def create_hh(s, errLogger):
	tokens = s.split(',')
	hh = HuyetHoc()
	hh.original=s
	for x in xrange(0,len(TYPE_LIST)):
		t = tokens[x].strip('"').strip('-').strip()
		if not t: continue
		if TYPE_LIST[x] is str:
			hh.__dict__['field%02d' % (x+1,)]=t
		elif TYPE_LIST[x] is int:
			try:
				hh.__dict__['field%02d' % (x+1,)]=int(t)
			except ValueError:
				errLogger.error('Error while trying to convert value "%s" to a number.\n%s' % (t,s))
		elif TYPE_LIST[x] is float:
			try:
				f=1
				if x+1 in SHIFT_COUNT_UNIT_LABEL.keys():
					f = pow(0.1, SHIFT_COUNT_UNIT_LABEL[x+1][0])
				hh.__dict__['field%02d' % (x+1,)]=float(t)*f
			except ValueError:
				errLogger.error('Error while trying to convert value "%s" to a number.\n%s' % (t,s))
	return hh
	"""Test"
from int import utils
import sys, logging
errLogger = logging.getLogger('Module XN')
errLogger.addHandler(sys.stdout)
hh=utils.create_hh('"   "," CD1700","------------",0530,00,"---","09/09/13","10:18","     1800","    NG TH K THOA"," ","  /  /  ","                      ","  /  ","  :  ","                ",00070,-----,00031,00011,00028,-----,00489,00135,00423,00865,00276,00319,00131,00427,00089,00038,00154,-----,00438,00156,00406,-----,-,-,0,1,0,0,0,0,0,-,-,-,-,-,-,-,-,-,-,-,-,-,00,00,04560,01834,06999,05057,00000,00000,1,"O",0,0,0,1,52', errLogger)
print hh.field01
	"""