$( document ).bind( 'mobileinit', function(){
  $.mobile.loader.prototype.options.text = "loading";
  $.mobile.loader.prototype.options.textVisible = true;
  $.mobile.loader.prototype.options.theme = "c";
  $.mobile.loader.prototype.options.html = "";
  $.mobile.defaultPageTransition = "none";
  $.mobile.defaultDialogTransition = "none";
});

yourlabs.TextWidget.prototype.getValue = function(choice) {
    return choice.data('value');
}
