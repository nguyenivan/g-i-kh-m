from django.views.generic import RedirectView
from django.views.generic.base import TemplateView
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy

admin.autodiscover()

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls), name='admin'),
	url(r'^$', TemplateView.as_view(template_name= 'int/trangchu.html'), name='int_trang_chu'),
	url(r'^report_builder/', include('report_builder.urls')),
	url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
)

if settings.DEBUG :
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
	)


