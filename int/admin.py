from django.contrib import admin
from django import forms
from django.utils.safestring import mark_safe
from int.models import HuyetHoc, SinhHoa, \
	CauHinhHH, KhoangGiaTriHH, CauHinhSH, KhoangGiaTriSH
from django.db.models import Q
from django.core.cache import cache
from copy import deepcopy

HUYET_HOC_COL_ATTR = {
	'field10': {'size':'10'}, #Specimen Name
	'_default': {'size':'5'},
	'accepted': {'class': 'accept_checkbox', 'disabled':'disabled'},
}

SH_COL_ATTR = {
	'_default': {'size':'5'},
	'accepted': {'class': 'accept_checkbox', 'disabled':'disabled'},
}

def get_config_sh(cau_hinh_id=None):
	if cau_hinh_id is None:
		qs=CauHinhSH.objects.filter(mac_dinh=True)[:1]
		if qs:
			cau_hinh_id = qs[0].pk
		else:
			return {}
	c = cache.get('cauhinhsh_%s' % cau_hinh_id) 
	if c is not None:
		return c
	else:
		c = {}
		for k in KhoangGiaTriSH.objects.filter(cau_hinh_id=cau_hinh_id):
			c[k.gia_tri]=k
		cache.set('cauhinhsh_%s' % cau_hinh_id, c)
		return c 

def get_config(cau_hinh_id=None):
	if cau_hinh_id is None:
		qs=CauHinhHH.objects.filter(mac_dinh=True)[:1]
		if qs:
			cau_hinh_id = qs[0].pk
		else:
			return {}
	c = cache.get('cauhinh_%s' % cau_hinh_id) 
	if c is not None:
		return c
	else:
		c = {}
		for k in KhoangGiaTriHH.objects.filter(cau_hinh_id=cau_hinh_id):
			c[k.gia_tri]=k
		cache.set('cauhinh_%s' % cau_hinh_id, c)
		return c 

def check_config(val, khoang):
	if khoang.min_value is not None and khoang.min_value > val:
		return False
	if khoang.max_value is not None and khoang.max_value < val:
		return False
	return True

class HuyetHocAdminForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(HuyetHocAdminForm, self).__init__(*args, **kwargs)
		d = HUYET_HOC_COL_ATTR
		config = get_config(self.instance.cau_hinh_id)
		for key in self.fields:
			a = d.get(key)
			if a is None: 
				a = d.get('_default')
			self.fields[key].widget.attrs = deepcopy(a)
			if key in config:
				if not check_config(self[key].value(),config[key]):
					self.fields[key].widget.attrs['style']='color:red;'
	class Meta:
		model = HuyetHoc

class SinhHoaAdminForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(SinhHoaAdminForm, self).__init__(*args, **kwargs)
		d = SH_COL_ATTR
		config = get_config_sh(self.instance.cau_hinh_id)
		for key in self.fields:
			a = d.get(key)
			if a is None: 
				a = d.get('_default')
			self.fields[key].widget.attrs = deepcopy(a)
			if key in config:
				if not check_config(self[key].value(),config[key]):
					self.fields[key].widget.attrs['style']='color:red;'
	class Meta:
		model = SinhHoa

class HuyetHocAdmin(admin.ModelAdmin):

	def __init__(self, *args, **kwargs):
		super(HuyetHocAdmin, self).__init__(*args, **kwargs)
		config = get_config()
		list_temp = []
		for key in config:
			list_temp.append(key)

		list_display_temp = ['cau_hinh', 'accept_button',  'nhan_xet', 'id','ngay_tao','ngay_ghi', 'field09', 'field10']	
		list_display_temp.extend(list_temp)
		self.list_display = list_display_temp
		self.list_display.append('accepted')

		self.list_display_links = ['id']

		list_editable_temp=['cau_hinh', 'accepted',  'nhan_xet', 'field09', 'field10']
		list_editable_temp.extend(list_temp)
		self.list_editable = list_editable_temp

	# Search Specimen ID, Specimen Name
		self.search_fields = ["field09", "field10"]
		self.list_per_page = 100

	def queryset(self, request):
		qs = super(HuyetHocAdmin, self).queryset(request).filter(~Q(accepted=False))
		return qs

	def get_changelist_form(self, request, **kwargs):
		return HuyetHocAdminForm

	def accept_button(self, obj):
		return mark_safe('<input class="accept_button" type="button" value="%s" data-pk="%s">' %\
			("Reject " if obj.accepted else "Accept",obj.pk) )

	def get_search_results(self, request, queryset, search_term):
		queryset, use_distinct = super(HuyetHocAdmin, self).get_search_results(request, queryset, search_term)
		try:
			search_term_as_int = int(search_term)
			queryset |= self.model.objects.filter(field09=search_term_as_int)
		except ValueError:
			pass
		return queryset, use_distinct

	class Media:
		js = ('admin/xn.js',)

class SinhHoaAdmin(admin.ModelAdmin):

	config = get_config_sh()
	list_temp = []
	for key in config:
		list_temp.append(key)

	list_display_temp = ['cau_hinh', 'accept_button', 'nhan_xet', 'id', 'sequence','position', 'ngay_tao', 'ngay_ghi',  'specimen_id']	
	list_display_temp.extend(list_temp)
	list_display = list_display_temp
	list_display.append('accepted')

	list_display_links = ['id']

	list_editable_temp=['cau_hinh', 'specimen_id', 'accepted',  'nhan_xet',]
	list_editable_temp.extend(list_temp)
	list_editable = list_editable_temp
	# Search Specimen ID, Specimen Name
	search_fields = ["sequence", "specimen_id"]
	list_per_page = 100
	def queryset(self, request):
		qs = super(SinhHoaAdmin, self).queryset(request).filter(~Q(accepted=False))
		return qs

	def get_changelist_form(self, request, **kwargs):
		return SinhHoaAdminForm

	def accept_button(self, obj):
		return mark_safe('<input class="accept_button" type="button" value="%s" data-pk="%s">' %\
			("Reject " if obj.accepted else "Accept",obj.pk) )


	def get_search_results(self, request, queryset, search_term):
		queryset, use_distinct = super(HuyetHocAdmin, self).get_search_results(request, queryset, search_term)
		try:
			search_term_as_int = int(search_term)
			queryset |= self.model.objects.filter(sequence=search_term_as_int) | \
					self.model.objects.filter(specimen_id=search_term_as_int)
		except ValueError:
			pass
		return queryset, use_distinct
	class Media:
		js = ('admin/xn.js',)

class KhoangGiaTriSHInline(admin.TabularInline):
	model = KhoangGiaTriSH

class CauHinhSHAdmin(admin.ModelAdmin):
	list_display = ['ten', 'mac_dinh', 'ngay_tao']
	inlines = [
		KhoangGiaTriSHInline,
	]

class KhoangGiaTriHHInline(admin.TabularInline):
	model = KhoangGiaTriHH

class CauHinhHHAdmin(admin.ModelAdmin):
	list_display = ['ten', 'mac_dinh', 'ngay_tao']
	inlines = [
		KhoangGiaTriHHInline,
	]

admin.site.register(HuyetHoc, HuyetHocAdmin)
admin.site.register(SinhHoa, SinhHoaAdmin)
admin.site.register(CauHinhHH, CauHinhHHAdmin)
admin.site.register(CauHinhSH, CauHinhSHAdmin)
