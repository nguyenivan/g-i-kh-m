# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CauHinhSH'
        db.create_table(u'int_cauhinhsh', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ten', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('mac_dinh', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('ngay_tao', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'int', ['CauHinhSH'])

        # Adding model 'KhoangGiaTriSH'
        db.create_table(u'int_khoanggiatrish', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cau_hinh', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['int.CauHinhSH'])),
            ('gia_tri', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ngay_ghi', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('max_value', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('min_value', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'int', ['KhoangGiaTriSH'])

        # Adding unique constraint on 'KhoangGiaTriSH', fields ['gia_tri', 'cau_hinh']
        db.create_unique(u'int_khoanggiatrish', ['gia_tri', 'cau_hinh_id'])

        # Adding field 'SinhHoa.cau_hinh'
        db.add_column(u'int_sinhhoa', 'cau_hinh',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['int.CauHinhSH'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Removing unique constraint on 'KhoangGiaTriSH', fields ['gia_tri', 'cau_hinh']
        db.delete_unique(u'int_khoanggiatrish', ['gia_tri', 'cau_hinh_id'])

        # Deleting model 'CauHinhSH'
        db.delete_table(u'int_cauhinhsh')

        # Deleting model 'KhoangGiaTriSH'
        db.delete_table(u'int_khoanggiatrish')

        # Deleting field 'SinhHoa.cau_hinh'
        db.delete_column(u'int_sinhhoa', 'cau_hinh_id')


    models = {
        u'int.cauhinhhh': {
            'Meta': {'object_name': 'CauHinhHH'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mac_dinh': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ngay_tao': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ten': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'int.cauhinhsh': {
            'Meta': {'object_name': 'CauHinhSH'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mac_dinh': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ngay_tao': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ten': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'int.huyethoc': {
            'Meta': {'object_name': 'HuyetHoc'},
            'accepted': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'cau_hinh': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['int.CauHinhHH']", 'null': 'True', 'blank': 'True'}),
            'field01': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field02': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field03': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field04': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field05': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field06': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field07': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field08': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field09': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field10': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field11': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field12': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field13': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field14': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field15': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field16': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field17': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field18': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field19': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field20': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field21': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field22': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field23': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field24': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field25': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field26': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field27': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field28': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field29': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field30': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field31': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field32': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field33': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field34': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field35': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field36': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field37': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field38': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field39': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field40': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field41': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field42': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field43': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field44': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field45': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field46': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field47': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field48': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field49': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field50': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field51': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field52': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field53': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field54': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field55': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field56': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field57': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field58': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field59': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field60': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field61': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field62': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field63': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field64': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field65': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field66': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field67': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field68': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field69': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field70': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'field71': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field72': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field73': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field74': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field75': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'int.khoanggiatrihh': {
            'Meta': {'unique_together': "(['gia_tri', 'cau_hinh'],)", 'object_name': 'KhoangGiaTriHH'},
            'cau_hinh': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['int.CauHinhHH']"}),
            'gia_tri': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_value': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'min_value': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'ngay_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'int.khoanggiatrish': {
            'Meta': {'unique_together': "(['gia_tri', 'cau_hinh'],)", 'object_name': 'KhoangGiaTriSH'},
            'cau_hinh': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['int.CauHinhSH']"}),
            'gia_tri': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_value': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'min_value': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'ngay_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'int.sinhhoa': {
            'Meta': {'object_name': 'SinhHoa'},
            'accepted': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'cau_hinh': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['int.CauHinhSH']", 'null': 'True', 'blank': 'True'}),
            'disk': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'field01': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field02': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field03': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field04': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field05': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field06': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field07': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field08': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field09': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field10': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field11': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field12': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field13': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field14': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field15': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field16': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field17': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field18': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field19': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field20': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field21': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field22': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field23': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field24': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field25': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field26': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field27': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field28': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field29': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field30': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field31': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field32': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field33': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field34': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field35': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field36': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field37': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field38': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field39': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field40': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field41': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field42': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field43': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field44': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field45': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'field46': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'original': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'specimen_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['int']