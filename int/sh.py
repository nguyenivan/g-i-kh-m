#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import logging
import serial
import traceback
import signal
from sys import exit
import os,sys

DEBUG = False
current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(current_path,"..") )
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onsite.settings_development" if DEBUG else "onsite.settings_xn")
from int.utils import create_sh
from int.models import SinhHoa

PORT = '/dev/ttyS0'
LINE_START = chr(0x02)
LINE_END = chr(0x03)
ACK = chr(0x06)
NAK = chr(0x15)


port = serial.Serial(timeout=0.5, baudrate=2400, bytesize=serial.SEVENBITS, \
	parity=serial.PARITY_EVEN, stopbits=serial.STOPBITS_TWO, rtscts=True)
# port = serial.Serial(timeout=0.5, rtscts=True)


def clean_up(signum, frame):
	global port
	try:
		port.close()
	except Exception:
		errLogger.error(traceback.format_exc())
	exit(1)

signal.signal(signal.SIGTERM, clean_up)
signal.signal(signal.SIGINT, clean_up)

def main_program(serial_port):
	formatter = logging.Formatter('%(asctime)s - ' + serial_port + ' - %(message)s')
	infLogger = logging.getLogger('Ket Qua XN')
	errLogger = logging.getLogger('Module XN')

	infLogger.setLevel(logging.INFO)
	errLogger.setLevel(logging.INFO)

	file_log_handler = logging.FileHandler(os.path.join(current_path,'xn.log') if DEBUG else '/var/log/xn.log')
	stdout = sys.stdout
	stderr_log_handler = logging.StreamHandler(stdout)

	errLogger.addHandler(stderr_log_handler)
	infLogger.addHandler(file_log_handler)
	errLogger.addHandler(file_log_handler)

	file_log_handler.setFormatter(formatter)
	stderr_log_handler.setFormatter(formatter)
	buf = ''
	connected = True
	port.port = serial_port
	while True:
		try:
			if not port.isOpen():
				port.open()
				errLogger.info(u'Cổng COM vừa được kết nối')
				connected = True
				time.sleep(1)
			if port.inWaiting():
				c = port.read()
				if c != LINE_END:
					buf = buf + c
				else:
					bcc = port.read() # BCC
					port.write(ACK)
					s = buf.split(LINE_START)[-1]
					infLogger.info(s)
					sh= create_sh(s, errLogger)
					sh.save()
					buf = ''
		except serial.SerialException:
			if connected:
				connected = False
				errLogger.error(u'Lỗi kết nối cổng COM')
				infLogger.error(traceback.format_exc())
		except Exception:
			errLogger.error(traceback.format_exc())
			time.sleep(3)

"""
To Test 
socat -d -d pty,raw,echo=0, pty,raw,echo=0
/Users/nguyenivan/Envs/production/bin/python int/sh.py /dev/ttys004
cat int/data/hitachi717.txt > /dev/ttys005
"""

"""
To Install The Script: tao file /etc/init/xn.conf

description "Dich Vu Ket Noi Data"
author  "Le D Nguyen<nguyen@qhoach.com>"

start on runlevel [234]
stop on runlevel [0156]

chdir /var/www/onsite
exec /var/www/onsite/int/xn.py
respawn

"""

"""
import serial
with serial.Serial('/dev/tty.usbserial', timeout=0.5, rtscts=True) as p:
	for x in xrange(0,5):
		with open('/Users/nguyenivan/Projects/onsite/int/data/sample.txt','r') as a:
				while True:
					s=a.read()
					if not s:
						break
					else:
						p.write(s)
				p.flush()
"""

if __name__ == "__main__":
	if len(sys.argv) > 2:
		print USAGE
		sys.exit(1)
	serial_port = PORT if len(sys.argv) == 1 else sys.argv[1]
	print serial_port
	main_program(serial_port)
