#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import logging
import serial
import traceback
import signal
from sys import exit
import os,sys

DEBUG = False
current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(current_path,"..") )
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onsite.settings_development" if DEBUG else "onsite.settings_xn")
from int.utils import create_hh
from int.models import HuyetHoc

USAGE = "Cú pháp: python xn.py [serial port]\nVí dụ: python xn.py /dev/tty.usbserial (nếu không chỉ định port, default=/dev/ttyS0)"
PORT = '/dev/ttyS0'
LINE_START = chr(0x02)
LINE_END = chr(0x03)

port = serial.Serial(timeout=0.5,rtscts=True)

def clean_up(signum, frame):
	global port
	try:
		port.close()
	except Exception:
		errLogger.error(traceback.format_exc())
	exit(1)

signal.signal(signal.SIGTERM, clean_up)
signal.signal(signal.SIGINT, clean_up)

def main_program(serial_port):
	formatter = logging.Formatter('%(asctime)s - ' + serial_port + ' - %(message)s')
	infLogger = logging.getLogger('Ket Qua XN')
	errLogger = logging.getLogger('Module XN')

	infLogger.setLevel(logging.INFO)
	errLogger.setLevel(logging.INFO)

	file_log_handler = logging.FileHandler(os.path.join(current_path,'xn.log') if DEBUG else '/var/log/xn.log')
	stdout = sys.stdout
	stderr_log_handler = logging.StreamHandler(stdout)

	errLogger.addHandler(stderr_log_handler)
	infLogger.addHandler(file_log_handler)
	errLogger.addHandler(file_log_handler)

	file_log_handler.setFormatter(formatter)
	stderr_log_handler.setFormatter(formatter)

	buf = ''
	connected = True
	port.port = serial_port
	while True:
		try:
			if not port.isOpen():
				port.open()
				errLogger.info(u'Cổng COM vừa được kết nối')
				connected = True
			c = port.read()
			if c != LINE_END:
				buf = buf + c
			else:
				s = buf.split(LINE_START)[-1]
				infLogger.info(s)
				hh= create_hh(s, errLogger)
				hh.save()
				buf = ''
		except serial.SerialException:
			if port.isOpen():
				port.close()
			if connected:
				connected = False
				errLogger.error(u'Lỗi kết nối cổng COM')
				infLogger.error(traceback.format_exc())
		except Exception:
			errLogger.error(traceback.format_exc())
			time.sleep(3)

"""
To Test 
socat -d -d pty,raw,echo=0, pty,raw,echo=0
cat /var/www/onsite/int/data/sample.txt > /dev/pts/10
"""

"""
To Install The Script: tao file /etc/init/xn.conf

description "Dich Vu Ket Noi Data"
author  "Le D Nguyen<nguyen@qhoach.com>"

start on runlevel [234]
stop on runlevel [0156]

chdir /var/www/onsite
exec /var/www/onsite/int/xn.py
respawn

command: 
	initctl list
	initctl start [job]
	initctl stop [job]

"""

"""
sudo /Users/nguyenivan/Envs/production/bin/python int/xn.py
"""

if __name__ == "__main__":
	if len(sys.argv) > 2:
		print USAGE
		sys.exit(1)
	serial_port = PORT if len(sys.argv) == 1 else sys.argv[1]
	print serial_port
	main_program(serial_port)
