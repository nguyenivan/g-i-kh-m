# -*- coding: utf-8 -*-
from onsite.models import KetQuaMau

def genfullname():
	for kqm in KetQuaMau.objects.all():
		o=kqm
		family = o.ket_qua
		for x in xrange(1,4): #to prevent infinite loop
			o = o.chi_tiet_cua
			if o is None:
				break
			family =  '%s - %s' % (o.ket_qua, family)
		# print family
		kqm.full_name = family
		kqm.save()
		for kq in kqm.ketqua_set.all():
			kq.full_name = kqm.full_name
			kq.save()
	"""TEST"
import test.adjkqm
	"""
import re
def t(s,r):
	count = 0
	for kqm in KetQuaMau.objects.all():
		fn=kqm.full_name
		if re.match(s,fn):
			nfn = re.sub(s,r,fn)
			print fn, " -> ", nfn
			count=count+1
	print count
"""
from test.adjkqm import t,g
"""

def g(s,r):
	count = 0
	for kqm in KetQuaMau.objects.all():
		fn=kqm.full_name
		if re.match(s,fn):
			nfn = re.sub(s,r,fn)
			print fn, " -> ", nfn
			kqm.full_name = nfn
			kqm.save()
			count=count+1
	print count

def printkqm():
	with open('ketquamau.csv', 'w') as f: 
		f.write(('%s,%s,%s,%s\n' % (u'ID',u'Kết Quả Mẫu', u'Mục Khám', u'Mục Khám ID')).encode('utf8'))
		for kqml in KetQuaMau.objects.all():
			f.write(('%s,"%s","%s",%s\n' % (kqml.id, kqml.full_name, kqml.muc_kham, kqml.muc_kham_id)).encode('utf8'))
"""
from test.adjkqm import printkqm
printkqm()
"""







