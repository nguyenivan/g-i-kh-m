#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os,sys
current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(current_path,"..") )
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onsite.settings_development")

from onsite.models import GoiKham, KhachHang, MucKham, BenhNhan
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from django.db import transaction
import traceback

@transaction.commit_manually
def create_it():
	try:
		kh= KhachHang(ten="Khach Hang Test")
		kh.save()
		gk = GoiKham(khach_hang=kh, bat_dau=datetime.now(), \
			ket_thuc= datetime.now() + timedelta(hours=10))
		gk.save()

		for x in xrange(1,10):
			mk=MucKham.objects.get(pk=x)
			u1 = User.objects.create_user('bs%s1' % x, password='1234')
			u2 = User.objects.create_user('bs%s2' % x, password='1234')
			mk.nhom.user_set.add(*[u1,u2])
			gk.cho_phep.add(*[u1,u2])
		transaction.commit()
	except Exception, e:
		traceback.print_exc()
		print "ROLLING BACK!!!"
		transaction.rollback()

@transaction.commit_manually
def create_bn():
	try:
		gk = GoiKham.objects.filter(khach_hang__ten='Khach Hang Test')[0]
		for x in xrange(1,5001):
			bn=BenhNhan(stt=x, ho_ten='Benh Nhan %s' % x, goi_kham=gk)
			bn.save()
			print bn		
		transaction.commit()
	except Exception, e:
		traceback.print_exc()
		print "ROLLING BACK!!!"
		transaction.rollback()


if __name__ == "__main__":
	create_it()
	create_bn()




