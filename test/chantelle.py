#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ID cua bang onsite_ketqua BEFORE=4533L, AFTER=4786L
# Da kham: BEFORE 2775 muc, AFTER: 2985

# insert da_kham to BenhNhan
# insert ket_qua to KetQua
# export excel may 10
# export excel chantelle

import os,sys
current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(current_path,"..") )
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onsite.settings_development")

from onsite.models import KetQuaMau, BenhNhan

SRC = 'ibm'
DST = 'hp'
TEST = True
USAGE = "Cú pháp: python chantelle.py [go]"
def update_dakham():
	count = 0
	errcount  = 0
	for bn in BenhNhan.objects.using(SRC).all():
		mk_src = [mk.pk for mk in bn.da_kham.all()]
		try:
			bndst = BenhNhan.objects.using(DST).get(pk=bn.pk)
		except BenhNhan.DoesNotExist:
			print u'Bệnh nhân %s from connection %s does not exist' % (bn, SRC)
		else:
			try:
				mk_dst = [mk.pk for mk in bndst.da_kham.all()]
				print 'Muc kham(s): %s are to be updated for benh nhan : %s' % \
					(set(mk_src) - set(mk_dst) , bn)
				if set(mk_src) - set(mk_dst):
					count = count + 1
				if not TEST:
					bndst.da_kham.add(*mk_src)
					pass
			except Exception,e :
				print "Error while trying to update muc kham(s) for bn %s. Detail: %s" % (bn, e)
				errcount = errcount + 1
	print '*'*10, '\n%s of BenhNhan has been updated with new MucKham(s) with %s error(s)\n' % (count, errcount), '*'*10

def update_ketqua():
	count = 0
	errcount  = 0
	trung = []
	for bn in BenhNhan.objects.using(SRC).filter(stt__lt=1000):
		kq_src = [kq for kq in bn.cac_ket_qua.filter(huy=False)]
		tobeup = []
		try:
			bndst = BenhNhan.objects.using(DST).get(pk=bn.pk)
		except BenhNhan.DoesNotExist:
			print u'Bệnh nhân %s from connection %s does not exist' % (bn, DST)
		else:
			kqm_dst_id = [kq.ket_qua_mau_id for kq in bndst.cac_ket_qua.filter(huy=False)]
			for x in xrange(0, len(kq_src)):
				if kq_src[x].ket_qua_mau_id not in kqm_dst_id:
					tobeup.append(kq_src[x])
				else:
					trung.append(kq_src[x])
			print 'Ket qua(s): %s are to be updated for benh nhan : %s' % \
				(tobeup , bn)
			count = count + len (tobeup)
			if not TEST:
				for kq in tobeup:
					try:
						kq.id = None
						kq.save(using=DST)
					except Exception,e :
						print "Error while trying to update muc kham(s) for bn %s. Detail: %s" % (bn, e)
						errcount = errcount + 1

	print '*'*10, '\n%s of KetQua has been updated added to BenhNhan(s) with %s error(s)\n' % (count, errcount), '*'*10
	print 'Ket qua trung (%s): ' % len(trung), (', '.join([' '.join([str(kq.id),kq.__unicode__()]) for kq in trung])).encode('utf-8')

if __name__ == "__main__":
	if len(sys.argv) > 2:
		print USAGE
		sys.exit(1)
	TEST = not(len(sys.argv) == 2 and sys.argv[1] == 'go')
	if TEST:
		print 'Running in TEST mode'
	else:
		print 'Running REAL!!!'
	# update_dakham()
	update_ketqua()