# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from bv.models import CongViec, LoThuoc, Thuoc
from django.db import models
import autocomplete_light
from django import forms

# class UserAdmin(AdminSite):
# 	def has_permission(self, request):
# 		return True
# user_admin_site = UserAdmin(name='usersadmin')
# user_admin_site.register(Thuoc)

class CongViecAdmin(admin.ModelAdmin):
	list_display = ['id', 'ma_phi', 'dien_giai', 'don_gia', 'phu_thu', 'service_line', 'loai_cv', 'station']
	list_editable = ['ma_phi', 'dien_giai', 'don_gia', 'phu_thu', 'service_line', 'loai_cv', 'station']
	search_fields = ['ma_phi', 'dien_giai']
	list_per_page = 100


class LoThuocAdminForm(forms.ModelForm):
	class Meta:
		model = LoThuoc
		widgets = {
			'dien_giai': autocomplete_light.TextWidget('ThuocAutocomplete'),
		}


class LoThuocAdmin(admin.ModelAdmin):
	form = LoThuocAdminForm
	list_display = ['id', 'ma_phi', 'dien_giai', 'so_luong', 'het_han', 'ngay_sx', 'ngay_nhap', 'nguoi_nhap',
		'cung_cap', 'san_xuat', 'gia_nhap', 'ghi_chu']
	list_editable = ['ma_phi', 'dien_giai', 'so_luong', 'het_han', 'ngay_sx', 'ngay_nhap', 'nguoi_nhap', 'cung_cap',
		'san_xuat', 'gia_nhap', 'ghi_chu']
	list_display_links = ['id']

	def __init__(self, *args, **kwargs):
		super(LoThuocAdmin, self).__init__(*args, **kwargs)


class ThuocAdmin(admin.ModelAdmin):
	list_display = ['id', 'ma_phi', 'dien_giai', 'don_gia', 'phu_thu', 'service_line', 'don_vi', 'ham_luong', 'ton_kho',
		'dv_hl', 'hoat_chat', 'nuoc_sx', 'nha_sx', 'nha_cc']
	list_editable = ['ma_phi', 'dien_giai', 'don_gia', 'phu_thu', 'service_line', 'don_vi', 'ham_luong', 'ton_kho',
		'dv_hl', 'hoat_chat', 'nuoc_sx', 'nha_sx', 'nha_cc']
	list_display_links = ['id']
	search_fields = ['ma_phi', 'dien_giai', 'hoat_chat']

	def __init__(self, *args, **kwargs):
		super(ThuocAdmin, self).__init__(*args, **kwargs)


admin.site.register(CongViec, CongViecAdmin)
admin.site.register(LoThuoc, LoThuocAdmin)
admin.site.register(Thuoc, ThuocAdmin)
