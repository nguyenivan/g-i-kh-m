# -*- coding: utf-8 -*-
import base64, struct, math
# encode(1000001) = 'QUIP'

def encode(n):
	data = struct.pack('<Q', n).rstrip('\x00')
	if len(data) == 0:
		data = '\x00'
	s = base64.urlsafe_b64encode(data).rstrip('=')
	return s
	
# decode('QUIP') = 1000001
def decode(s):
	data = base64.urlsafe_b64decode(s + '==')
	n = struct.unpack('<Q', data + '\x00' * (8 - len(data)))
	return n[0]
	
def encode_tileid(x,y,z):
	'for given tile x,y,z return encoded tileid calculated by 000000X00000Y0Z '
	return encode( x * 10**8 + y * 10**2 + z)
	pass

def decode_tileid(tileid):
	decoded = decode(tileid)
	z = decoded % 10**2
	y = ( decoded // 10**2 ) % 10**6
	x = decoded // 10**8
	return x,y,z

def upper_vietnamese(s):
	if type(s) == type(u""):
		return s.upper()
	return unicode(s, "utf8").upper().encode("utf8")

import string
import re

VNCHAR = 'ạảãàáâậầấẩẫăắằặẳẵóòọõỏôộổỗồốơờớợởỡéèẻẹẽêếềệểễúùụủũưựữửừứíìịỉĩýỳỷỵỹđ'
VNCHAR += upper_vietnamese(VNCHAR)
INTAB = [ch.encode('utf8') for ch in unicode(VNCHAR, 'utf8')]
#INTAB = [ch.encode('utf8') for ch in VNCHAR]

OUTTAB = "a"*17 + "o"*17 + "e"*11 + "u"*11 + "i"*5 + "y"*5 + "d"
OUTTAB += OUTTAB.upper()

r = re.compile("|".join(INTAB))
replaces_dict = dict(zip(INTAB, OUTTAB))

def remove_accent(utf8_str):
	if type(utf8_str) == type(''):
		return r.sub(lambda m: replaces_dict[m.group(0)], utf8_str)
	else:
		raise (Exception('Input parameter must be str type.'))

def test_accent():
	print u'ThIÊn Hạ Vô ĐịCh', remove_accent('ThIÊn Hạ Vô ĐịCh')
	print u'THIÊN HẠ VÔ ĐỊCH', remove_accent('THIÊN HẠ VÔ ĐỊCH')
	
import string
from datetime import datetime
# ALPHABET = string.ascii_uppercase + string.ascii_lowercase + \
#            string.digits + '-_'

ALPHABET = string.ascii_uppercase

START_OF_TIME = datetime(1990,1,1)

ALPHABET_REVERSE = dict((c, i) for (i, c) in enumerate(ALPHABET))
BASE = len(ALPHABET)
SIGN_CHARACTER = '$'

def num_encode(n):
	if n < 0:
		return SIGN_CHARACTER + num_encode(-n)
	s = []
	while True:
		n, r = divmod(n, BASE)
		s.append(ALPHABET[r])
		if n == 0: break
	return ''.join(reversed(s))

def num_decode(s):
	if s[0] == SIGN_CHARACTER:
		return -num_decode(s[1:])
	n = 0
	for c in s:
		n = n * BASE + ALPHABET_REVERSE[c]
	return n

def month_encode(dt):
	"Lấy mã code của tháng tính từ 1-1-1990"
	num_of_month = dt.month + dt.year*12 - START_OF_TIME.month - START_OF_TIME.year*12
	return num_encode(num_of_month)

def he_so (val, hs=None):
	if not hs:
		return val
	return val*hs

MA_DIA_PHUONG = '79'
MA_BAO_HIEM = '79486'
LUONG_TOI_THIEU = 1170000.00
MAX_THUOC = 300000
FIFTEEN_PERCENT = 0.15*LUONG_TOI_THIEU
FOURTY = 40*LUONG_TOI_THIEU
BANG_BHYT = (
	#Mức thanh toán 100%, mức thanh toán co-pay, mức thanh toán KTC
	(lambda x: x, lambda x: x, None, None, u'Thanh toán 100%'),
	(lambda x: x, lambda x: he_so(x,None), None, None, u'Thanh toán 100%% KCB. Đối với DV kỹ thuật cao không vượt quá 40 tháng lương tối thiểu (%s).' % LUONG_TOI_THIEU),
	(lambda x: x, lambda x: he_so(x,None), None, None, u'Thanh toán 100%% KCB. Đối với DV kỹ thuật cao không vượt quá 40 tháng lương tối thiểu (%s).' % LUONG_TOI_THIEU),
	(lambda x: he_so(x, 0.95), lambda x: he_so(x, 0.95), FIFTEEN_PERCENT, FOURTY, u'Thanh toán 95%% tiền KCB nếu trên 15%% tháng lương tối thiểu. Đối với DV kỹ thuật cao thanh toán 95%% không vượt quá 40 tháng lương tối thiểu (%s).' % LUONG_TOI_THIEU),
	(lambda x: he_so(x, 0.95), lambda x: he_so(x, 0.95), FIFTEEN_PERCENT, FOURTY, u'Thanh toán 95%% tiền KCB nếu trên 15%% tháng lương tối thiểu. Đối với DV kỹ thuật cao thanh toán 95%% không vượt quá 40 tháng lương tối thiểu (%s).' % LUONG_TOI_THIEU),
	(lambda x: he_so(x, 0.80), lambda x: he_so(x, 0.80), FIFTEEN_PERCENT, FOURTY, u'Thanh toán 80%% tiền KCB nếu trên 15%% tháng lương tối thiểu. Đối với DV kỹ thuật cao thanh toán 80%% không vượt quá 40 tháng lương tối thiểu (%s).' % LUONG_TOI_THIEU),
	(lambda x: he_so(x, 0.80), lambda x: he_so(x, 0.80), FIFTEEN_PERCENT, FOURTY, u'Thanh toán 80%% tiền KCB nếu trên 15%% tháng lương tối thiểu. Đối với DV kỹ thuật cao thanh toán 80%% không vượt quá 40 tháng lương tối thiểu (%s).' % LUONG_TOI_THIEU),
 )

def tien_chu(number):
	d = int(number)
	if d == 0:
		return u'không đồng'
	so = [u"không", u"một", u"hai", u"ba", u"bốn", u"năm", u"sáu", u"bảy", u"tám", u"chín"]
	hang = [u"", u"nghìn", u"triệu", u"tỷ"]
	# int i, j, donvi, chuc, tram;
	res = " "
	booAm = False
	s = str(d)
	if (d < 0):
		d = -d
		s = str(d)
		booAm = True
	i = len(s)
	if (i == 0):
		res = so[0] + res
	else:
		j = 0
		while (i > 0):
			donvi = int(s[i-1:i])
			i-=1
			if (i > 0):
				chuc = int(s[i-1:i])
			else:
				chuc = -1
			i-=1
			if (i > 0):
				tram = int(s[i - 1:i])
			else:
				tram = -1;
			i-=1
			if ((donvi > 0) or (chuc > 0) or (tram > 0) or (j == 3)):
				res = hang[j] + res
			j+=1
			if (j > 3): j = 1
			if ((donvi == 1) and (chuc > 1)):
				res = u"một " + res
			else:
				if ((donvi == 5) and (chuc > 0)):
					res = u"lăm " + res
				elif (donvi > 0):
					res = so[donvi] + " " + res
			if (chuc < 0):
				break
			else:
				if ((chuc == 0) and (donvi > 0)): res = u"lẻ " + res
				if (chuc == 1): res = u"mười " + res
				if (chuc > 1): res = so[chuc] + u" mươi " + res
			if (tram < 0): 
				break
			elif ((tram > 0) or (chuc > 0) or (donvi > 0)):
				res = so[tram] + u" trăm " + res
			res = " " + res
	if (booAm): res = u"Âm " + res
	return res + u"đồng"
"""TEST"
from bv.utils import tien_chu
print tien_chu(1234), u'Một ngàn hai trăm ba mươi bốn'
print tien_chu(55555), u'Năm mươi lăm ngàn năm trăm năm mươi lăm'
print tien_chu(0), u'Không'
print tien_chu(10), u'Mười'
print tien_chu(100), u'Một trăm'
print tien_chu(1000), u'Một ngàn'
print tien_chu(05), u'Năm'
"""


def generate_superbill(superbill_qs, bhyt='AA0000000000000', noi_dk='00000'):
	"""Tiền thuốc không quá 300,000 (warning)"""
	# superbill_list = superbill_qs.filter(loai_phi='thuoc')
	tong_bh = 0.0
	tong_thuoc = 0.0
	tong_kt = 0.0
	tong_kcb = 0.0
	tong_dv = 0.0

	bhyt_kcb = 0
	bhyt_kt = 0
	max_kcb = 0
	max_kt = 0
	# noi_dk = bhyt[5:10]
	dung_tuyen = noi_dk == MA_BAO_HIEM
	if dung_tuyen:
		muc_huong=int(bhyt[2:3])
		cong_thuc = BANG_BHYT[muc_huong-1]
	else:
		muc_huong=None
		cong_thuc = (
			lambda x: he_so(x, 0.70), 
			lambda x: he_so(x, 0.70),
			0,
			FOURTY,
			u'Thanh toán 70%% tiền KCB nếu trên 15%% tháng lương tối thiểu. Đối với DV kỹ thuật cao thanh toán 70%% không vượt quá 40 tháng lương tối thiểu (%s).' % LUONG_TOI_THIEU,
		)
	if dung_tuyen:
		yield ('message', u'Nơi đăng ký: %s, đúng tuyến.' % noi_dk)
		yield('message', u'Mức hưởng %s. %s' % (muc_huong, cong_thuc[4]))
	else:
		yield ('message',u'Nơi đăng ký: %s, trái tuyến.' % noi_dk)
		yield ('message', cong_thuc[4])

	for superbill in superbill_qs.filter(service_line='bao_hiem'):
		if superbill.loai_phi != 'ky_thuat':
			max_kcb += superbill.thanh_tien()
		else:
			max_kt += superbill.thanh_tien()

	if max_kcb  < cong_thuc[2]:
		"""Huong 100%"""
		yield ('message',u'Dưới mức tối thiểu, người bệnh hưởng 100%')
		for superbill in superbill_qs.filter(service_line='bao_hiem').exclude(loai_phi='ky_thuat'):
			superbill.quy_bhyt = superbill.thanh_tien()
			superbill.save()
	else:
		for superbill in superbill_qs.filter(service_line='bao_hiem').exclude(loai_phi='ky_thuat'):
			superbill.quy_bhyt = cong_thuc[0](superbill.thanh_tien())
			superbill.save()

	if cong_thuc[3] is not None and max_kt > cong_thuc[3]:
		yield ('message',u'Kỹ thuật cao vượt quá %s' % cong_thuc[3])
		max_kt = cong_thuc[3]
	
	for superbill in superbill_qs.filter(service_line='bao_hiem',loai_phi='ky_thuat'):
		print max_kt, superbill.thanh_tien()
		quy_bhyt = cong_thuc[1](superbill.thanh_tien())
		if max_kt > quy_bhyt-1:
			superbill.quy_bhyt = quy_bhyt
			max_kt -= quy_bhyt
			superbill.save()


	for superbill in superbill_qs:
		if superbill.service_line == 'bao_hiem':
			tong_bh += superbill.thanh_tien()
			if superbill.loai_phi == 'ky_thuat':
				bhyt_kt += superbill.quy_bhyt
			elif superbill.loai_phi=='thuoc':
				tong_thuoc += superbill.quy_bhyt
				bhyt_kcb += superbill.quy_bhyt
			else:
				bhyt_kcb += superbill.quy_bhyt
		else:
			tong_dv += superbill.thanh_tien()



	yield ('message', u'Tổng số tiền trong danh mục dịch vụ: %s.' % tong_dv)
	yield ('message', u'Tổng số tiền trong danh mục bảo hiểm: %s.' % tong_bh)
	yield ('message', u'BHYT khám chữa bệnh: %s, kỹ thuật cao: %s, tổng số %s.' % (bhyt_kcb, bhyt_kt, bhyt_kcb + bhyt_kt))		
	
	if tong_thuoc > 300000:
		yield ('warning' ,u'Tổng tiền thuốc bảo hiểm là %s, vượt quá 300,000VNĐ.' % tong_thuoc)
	
	yield ('value', bhyt_kcb + bhyt_kt)	
	

"""TEST"
from bv.models import BenhNhan, SuperBill, CuocHen
from bv.utils import generate_superbill
BenhNhan=BenhNhan.objects.get(ma_bn='KZ0039')
c = CuocHen.objects.get(pk=39)
superbill_qs = c.superbill_set.all()
bhyt='AA0000000000000'
def bhyt_test():
  for res in generate_superbill(superbill_qs,bhyt):
	print res

bhyt_test()

"""

"TEST""""
from datetime import datetime
from bv.utils import month_encode
month_encode(datetime(2011,01,01))
"JS"""
