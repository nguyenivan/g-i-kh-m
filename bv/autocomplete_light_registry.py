# -*- coding: utf-8 -*-
import autocomplete_light
from bv.models import CongViec, BenhLy, Thuoc, CoSo

class CongViecAutocomplete(autocomplete_light.AutocompleteModelTemplate):
	# model = CongViec
	choice_template = 'bv/congviec_choice.html'
	search_fields=['^ma_phi', 'dien_giai']
	autocomplete_js_attributes = {
		'minimum_character': 4,
		# 'choice_selector': '[data-ma_phi]',
	}
	widget_js_attributes = {
		'max_values': 6,
	}

class BenhLyAutocomplete(autocomplete_light.AutocompleteModelTemplate):
	# model = CongViec
	choice_template = 'bv/benhly_choice.html'
	search_fields=['^ma_benh', 'ten_benh']
	autocomplete_js_attributes = {
		'minimum_character': 4,
	}
	widget_js_attributes = {
		'max_values': 6,
	}

class ThuocAutocomplete(autocomplete_light.AutocompleteModelTemplate):
	# model = Thuoc
	choice_template = 'bv/thuoc_choice.html'
	search_fields=['^ma_phi', 'dien_giai']
	autocomplete_js_attributes = {
		'minimum_character': 4,
	}
	widget_js_attributes = {
		'max_values': 6,
	}
class CoSoAutocomplete(autocomplete_light.AutocompleteModelTemplate):
	# model = Thuoc
	choice_template = 'bv/coso_choice.html'
	search_fields=['^ma_so', 'ten_goi']
	autocomplete_js_attributes = {
		'minimum_character': 4,
	}
	widget_js_attributes = {
		'max_values': 6,
	}
class GhiChuAutocomplete(autocomplete_light.AutocompleteListBase):
	choices = [
		u'Trước khi ăn',
		u'Sau khi ăn',
		u'Trước khi ngủ',
		u'Khi cần',
	]

autocomplete_light.register(GhiChuAutocomplete)

autocomplete_light.register(CongViec, CongViecAutocomplete)
autocomplete_light.register(BenhLy, BenhLyAutocomplete)
autocomplete_light.register(Thuoc, ThuocAutocomplete)
autocomplete_light.register(CoSo, CoSoAutocomplete)

# This will generate a CongViecAutocomplete class
# autocomplete_light.register(CongViec,
#     search_fields=['^ma_phi', 'dien_giai'],
#     autocomplete_js_attributes={'placeholder': u'Nhập mã/tên',},
#     template_name = 'bv/congviec_choice.html',
# )