# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from bv.utils import month_encode
from mysqlfulltextsearch.search_manager import SearchManager
from django.template.defaultfilters import date
from django.utils import timezone
from jsonfield import JSONField
from django.utils.text import slugify
from django.db.models import Sum
import logging

SEX_CHOICES = (
(u'nam', _(u'Nam')),
(u'nu', _(u'Nữ')),
)

MODALITY_LIST = (
u'noi_khoa', u'phu_khoa', u'da_lieu', u'ngoai_khoa', u'tai_mui_hong', u'rang_ham_mat',
u'khoa_nhi', u'khoa_mat',
)

PROCEDURE_LIST = (
u'sieu_am', u'x_quang', u'xet_nghiem', u'tieu_phau', u'luu_benh',
)


STATION_CLASS = {
u'tiep_tan': (u'tiep_tan',),
u'thu_ngan': (u'thu_ngan',),
u'noi_khoa': (u'noi_khoa',),
u'cap_cuu': (u'cap_cuu',),
u'phu_khoa': (u'phu_khoa',),
u'da_lieu': (u'da_lieu',),
u'nha_thuoc': (u'nha_thuoc',),
u'sieu_am': (u'sieu_am',),
u'x_quang': (u'x_quang',),
u'xet_nghiem': (u'xet_nghiem',),
u'ngoai_khoa': (u'ngoai_khoa',),
u'tai_mui_hong': (u'tai_mui_hong',),
u'rang_ham_mat': (u'rang_ham_mat',),
u'khoa_nhi': (u'khoa_nhi',),
u'khoa_mat': (u'khoa_mat',),
u'tieu_phau': (u'tieu_phau',),
u'luu_benh': (u'luu_benh',),
}

STATION_CHOICES = (
(u'tiep_tan', u'Tiếp Tân'),
(u'thu_ngan', u'Thu Ngân'),
(u'noi_khoa', u'Khoa Nội'),
(u'cap_cuu', u'Cấp Cứu'),
(u'phu_khoa', u'Phụ Khoa'),
(u'da_lieu', u'Da Liễu'),
(u'nha_thuoc', u'Nhà Thuốc'),
(u'sieu_am', u'Siêu Âm'),
(u'x_quang', u'X-Quang'),
(u'xet_nghiem', u'Xét Nghiệm'),
(u'ngoai_khoa', u'Khoa Ngoại'),
(u'tai_mui_hong', u'Tai Mũi Họng'),
(u'rang_ham_mat', u'Răng Hàm Mặt'),
(u'khoa_nhi', u'Khoa Nhi'),
(u'khoa_mat', u'Khoa Mắt'),
(u'tieu_phau', u'Tiểu Phẫu'),
(u'luu_benh', u'Lưu Bệnh'),
)

# CV_CHOICES = {
# 	(u'kham_benh', u'
# 	(u'thu_thuat', u'Thủ Thuật'),
# 	(u'kham_ls', u'Khám Lâm Sàng'),
# 	(u'goi_kham', u'Gói Khám'),
# 	(u'chinh_hd', u'Chỉnh Hoá Đơn'),
# }
CV_CHOICES = {
(u'kham_benh', u'Khám Bệnh'),
(u'xet_nghiem', u'Xét Nghiệm'),
(u'hinh_anh', u'Chẩn Đoán Hình Ảnh'),
(u'tham_do', u'Thăm Dò Chức Năng'),
(u'thu_thuat', u'Thủ Thuật/Phẫu Thuật'),
(u'ky_thuat', u'Kỹ Thuật Cao'),
(u'khac', u'Khac'),
}

LOAI_PHI_CHOICES = {
(u'thuoc', u'Thuốc'),
}

LOAI_PHI_CHOICES.update(CV_CHOICES)

SUPERBILL_STATUS = {
(u'cho_duyet', u'Chờ Duyệt'),
(u'cho_tra', u'Chờ Thanh Toán'),
(u'dang_cho', u'Đang Chờ'),
(u'bat_dau', u'Bắt Đầu'),
(u'ket_thuc', u'Kết Thúc'),
(u'huy_bo', u'Hủy Bỏ'),
}

TOATHUOC_STATUS = {
(u'cho_duyet', u'Chờ Duyệt'),
(u'dang_cho', u'Đang Chờ'),
(u'kiem_tra', u'Đã Kiểm Tra'),
(u'thanh_toan', u'Đã Thanh Toán'),
(u'ket_thuc', u'Kết Thúc'),
(u'huy_bo', u'Hủy Bỏ'),
}

LOTHUOC_STATUS = {
(u'chua_nhap', u'Chưa Nhập'),
(u'da_nhap', u'Đã Nhập'),
}

VT_CHOICES = {
(u'kho', u'kho'),
}
DRUGFORM_CHOICES = {
(u'vien', u'Viên'),
(u'chai', u'Chai'),
(u'tuyp', u'Tuýp'),
(u'lo', u'lọ'),
(u'ong', u'ống'),
(u'goi', u'gói'),
}
DRUGUNIT_CHOICES = {
(u'mg', u'mg'),
(u'gram', u'gram'),
(u'ml', u'ml'),
}
DRUGROUTE_CHOICES = {
(u'uong', u'Uống'),
(u'boi', u'Bôi'),
(u'xit', u'Xịt'),
(u'hit', u'Hít'),
(u'tiem', u'Tiêm'),
(u'tiem_tinh_mach', u'Tiêm tĩnh mạch'),
(u'tiem_duoi_da', u'Tiêm dưới da'),
(u'tiem_bap_thit', u'tiêm bắp thịt'),
(u'nhet', u'Nhét hậu môn'),


}
DRUGLIST_CHOICES = {
(u'dich_vu', u'Dịch Vụ'),
(u'bao_hiem', u'Bảo Hiểm'),
}
DRUGFREQ_CHOICES = {
(u'ngay_mot', u'ngày 1 lần'),
(u'ngay_hai', u'ngày 2 lần'),
(u'ngay_ba', u'ngày 3 lần'),
(u'gio_mot', u'1 giờ/lần'),
(u'gio_hai', u'2 giờ/lần'),
(u'gio_ba', u'3 giờ/lần'),
(u'gio_bon', u'4 giờ/lần'),
(u'gio_nam', u'5 giờ/lần'),
(u'gio_sau', u'6 giờ/lần'),
(u'khi_can', u'khi cần'),
}

DRUGTIME_CHOICES = {
(u'sau_an', u'sau khi ăn'),
(u'truoc_an', u'trước khi ăn'),
(u'sang', u'sáng'),
(u'chieu', u'chiều'),
(u'toi', u'tối'),
(u'sang_toi', u'sáng tối'),
}

DRUGUSE_CHOICES = {
(u'vien', u'viên'),
(u'muong_cp', u'muỗng cafe'),
(u'ong', u'ống'),
(u'lan', u'lần'),
}


def get_stations(cls_list):
	set1 = set()
	for cls in STATION_CLASS:
		if cls in cls_list:
			set1 |= set(STATION_CLASS[cls])
	return [(name, title) for name, title in STATION_CHOICES if name in set1]


MODALITY_CHOICES = get_stations(MODALITY_LIST)
PROCEDURE_CHOICES = get_stations(PROCEDURE_LIST)
MODALITY_STATIONS = [x[0] for x in MODALITY_CHOICES]
PROCEDURE_STATIONS = [x[0] for x in PROCEDURE_CHOICES]

STATION_STATUS = (
(u'cho_duyet', u'Chờ Duyệt'),
(u'dang_cho', u'Đang Chờ'),
(u'bat_dau', u'Bắt Đầu'),
(u'ket_thuc', u'Kết Thúc'),
(u'huy_bo', u'Hủy Bỏ'),
)

HD_STATUS = (
(u'dang_mo', u'Đang Mở'),
(u'thanh_toan', u'Đã Thanh Toán'),
(u'huy_bo', u'Hủy Bỏ'),
)

HD_CHOICES = (
(u'tam_tinh', u'Tạm Tính'),
(u'thanh_toan', u'Thanh Toán'),
)

UNIT_CHOICES = (
(u'lan', u'Lần'),
(u'cai', u'Cái'),
)

DISCOUNT_CHOICES = (
(u'phan_tram', u'%'),
(u'so_tien', u'VND'),
# (u'phan_tram', u'Phần Trăm'),
# (u'so_tien', u'Số Tiền'),
)

DC_CHOICES = {
(u'icd10', u'ICD10'),
(u'an_phu', u'An Phú'),
}

SERVICE_LINES = {
(u'dich_vu', u'Dịch Vụ'),
(u'bao_hiem', u'Bảo Hiểm'),
}


class KetQuaBase(models.Model):
	class Meta:
		pass

	# To be subclassed for different procedures
	cuoc_hen = models.ForeignKey('CuocHen', verbose_name=u'cuộc hẹn')
	bac_sy = models.ForeignKey(User, verbose_name=u'bác sỹ', default=1)
	ghi_chu = models.TextField(verbose_name=u'kết luận của bác sỹ', blank=True)
	station = models.CharField(max_length=20, choices=STATION_CHOICES, blank=True)
	# trang_thai = models.CharField(max_length=20, choices = KQ_STATUS)
	huy = models.BooleanField(verbose_name=u'hủy', default=False)
	luc_ghi = models.DateTimeField(auto_now_add=True)
	cap_nhat = models.DateTimeField(auto_now=True)
	superbill = models.IntegerField(verbose_name=u'superbill ref', blank=True, null=True)


class KetQuaLamSang(KetQuaBase):
	class Meta:
		verbose_name = u'lâm sàng'
		verbose_name_plural = u'danh sách kết quả lâm sàng'

	def __unicode__(self):
		return '-'.join(filter(None, (self.cuoc_hen.__unicode__(), self.ma_benh, self.benh_ly)))

	# Ket luan cua bac sy, co the duoc subclassed cho nhieu khoa khac nhau
	benh_ly = models.TextField(verbose_name=u'chẩn đoán')
	ma_benh = models.CharField(verbose_name=u'mã bệnh', max_length=20, blank=True)


class KetQuaXetNghiem(KetQuaBase):
	class Meta:
		verbose_name = u'kết quả xét nghiệm'
		verbose_name_plural = u'danh sách kết quả xét nghiệm'

	# Ket luan cua bac sy, co the duoc subclassed cho nhieu khoa khac nhau
	huyet_hoc = JSONField(blank=True)
	sinh_hoa = JSONField(blank=True)
	nuoc_tieu = JSONField(blank=True)


class KetQuaXQuang(KetQuaBase):
	class Meta:
		verbose_name = u'chụp x-quang'
		verbose_name_plural = u'các kết quả x-quang'


class KetQuaSieuAm(KetQuaBase):
	class Meta:
		verbose_name = u'siêu âm'
		verbose_name_plural = u'các kết quả siêu âm'


class DiUng(KetQuaBase):
	class Meta:
		verbose_name = u'dị ứng'
		verbose_name_plural = u'các ghi chú dị ứng'

	ma_thuoc = models.CharField(verbose_name=u'mã thuốc', max_length=50, blank=True)
	ten_thuoc = models.CharField(verbose_name=u'tên thuốc', max_length=100, blank=True)


class ICD10Chapter(models.Model):
	class Meta:
		verbose_name = u'nhóm icd10'
		verbose_name_plural = u'các nhóm icd10'

	code = models.CharField(max_length=20)
	vi_name = models.CharField(max_length=200)
	en_name = models.CharField(max_length=200, blank=True)


class ICD10Group(models.Model):
	class Meta:
		verbose_name = u'nhóm icd10'
		verbose_name_plural = u'các nhóm icd10'

	code = models.CharField(max_length=20)
	chapter = models.ForeignKey(ICD10Chapter)
	vi_name = models.CharField(max_length=200)
	en_name = models.CharField(max_length=200, blank=True)


class BenhLy(models.Model):
	class Meta:
		verbose_name = u'bệnh lý'
		verbose_name_plural = u'danh sách bệnh lý'

	def __unicode__(self):
		return '%s:%s' % (self.ma_benh, self.ten_benh)

	ma_benh = models.CharField(max_length=50)
	ten_benh = models.CharField(max_length=200)
	tieng_anh = models.CharField(max_length=200, blank=True)
	icd10_group = models.ForeignKey(ICD10Group, blank=True, null=True)
	loai_ma = models.CharField(max_length=10, choices=DC_CHOICES)


class SuperBill(models.Model):
	def __unicode__(self):
		return '(%s)%s' % (self.service_line, '-'.join(
			filter(None, (self.item_description, str(self.unit_price), str(self.item_qty), self.item_unit))) )

	def thanh_tien(self):
		return thanh_tien(unit_price=self.unit_price,
						  item_qty=self.item_qty,
						  item_discount=self.item_discount,
						  discount_base=self.discount_base)

	cuoc_hen = models.ForeignKey('CuocHen')
	# item_code link toi Billable.ma_phi
	item_code = models.CharField(max_length=20, verbose_name=u'mã phí', blank=True, db_index=True)
	item_description = models.CharField(max_length=100, verbose_name=u'diễn giải')
	unit_price = models.FloatField(verbose_name=u'đơn giá', default=0)
	item_qty = models.IntegerField(verbose_name=u'số lượng', default=1)
	item_unit = models.CharField(max_length=20, verbose_name=u'đơn vị', choices=UNIT_CHOICES, blank=True)
	item_discount = models.IntegerField(verbose_name=u'giảm giá', default=0)
	discount_base = models.CharField(max_length=20, verbose_name=u'đv giảm', choices=DISCOUNT_CHOICES,
									default=u'phan_tram')
	phu_thu = models.FloatField(verbose_name=u'phụ phu', default=0)
	service_line = models.CharField(max_length=20, choices=SERVICE_LINES, default='dich_vu')
	station = models.CharField(max_length=20, choices=STATION_CHOICES, blank=True)
	tien_trien = models.ForeignKey('Station', blank=True, null=True)
	huy = models.BooleanField(verbose_name=u'huỷ', default=False)
	ketquabase_id = models.IntegerField(blank=True, null=True)
	loai_phi = models.CharField(max_length=20, verbose_name=u'loại phí', choices=LOAI_PHI_CHOICES, default='kham_benh')
	trang_thai = models.CharField(verbose_name=u'trạng thái', max_length=20, choices=SUPERBILL_STATUS,
								default=u'cho_duyet')
	quy_bhyt = models.FloatField(verbose_name=u'quỹ BHYT', default=0)


class Billable(models.Model):
	class Meta:
		verbose_name = u'billable'
		verbose_name_plural = u'billable'

	def __unicode__(self):
		return '(%s)%s:%s' % (self.service_line, self.ma_phi, self.dien_giai)

	ma_phi = models.CharField(verbose_name=u'mã phí', max_length=20, unique=True, db_index=True)
	dien_giai = models.CharField(max_length=100, verbose_name=u'diễn giải')
	don_gia = models.FloatField(verbose_name=u'đơn giá', default=0.0)
	phu_thu = models.FloatField(verbose_name=u'phụ thu', default=0.0)
	service_line = models.CharField(max_length=20, choices=SERVICE_LINES, default='dich_vu')


class CongViec(Billable):
	class Meta:
		verbose_name = u'dịch vụ'
		verbose_name_plural = u'danh sách dịch vụ'

	# ma_cv = models.CharField(verbose_name=u'mã dịch vụ', max_length=50)
	loai_cv = models.CharField(default='kham_benh', verbose_name=u'loại công việc', max_length=10, choices=CV_CHOICES)
	station = models.CharField(max_length=20, choices=STATION_CHOICES, blank=True)

class VatTu(Billable):
	class Meta:
		verbose_name = u'vật tư'
		verbose_name_plural = u'danh sách vật tư'

	ma_vt = models.CharField(max_length=50)
	ten_vt = models.CharField(max_length=200)
	loai_vt = models.CharField(max_length=10, choices=VT_CHOICES, default=u'kho')


class Thuoc(Billable):
	class Meta:
		verbose_name = u'thuốc'
		verbose_name_plural = u'danh mục thuốc'

	def __unicode__(self):
		return '(%s)%s' % (self.service_line, '-'.join(
			filter(None, (self.ma_phi, self.dien_giai, self.don_vi, self.hoat_chat, str(self.don_gia) ))) )

	don_vi = models.CharField(max_length=20, choices=DRUGFORM_CHOICES, default=u'vien')
	ham_luong = models.FloatField(default=0)
	ton_kho = models.IntegerField(verbose_name=u'tồn kho', default=0)
	dv_hl = models.CharField(max_length=20, choices=DRUGUNIT_CHOICES, default=u'mg', verbose_name=u'đơn vị hàm lượng')
	hoat_chat = models.CharField(max_length=50, blank=True, null=True, verbose_name=u'hoạt chất')
	nuoc_sx = models.CharField(max_length=20, blank=True, null=True, verbose_name=u'nước sx')
	nha_sx = models.CharField(max_length=50, blank=True, null=True, verbose_name=u'nhà sx')
	nha_cc = models.CharField(max_length=50, blank=True, null=True, verbose_name=u'nhà cung cấp')
# danh_muc = models.CharField(max_length=10, choices=DRUGLIST_CHOICES, default=u'dich_vu', verbose_name=u'danh mục')
#
# class BoSung(models.Model):
#
# 	class Meta:

# 		verbose_name = u'Bổ Sung'
# 		verbose_name_plural = u'Các Khoản Bổ Sung'
#
# 	@property
# 	def thanh_tien(self):
# 		if self.billable:
# 			if self.don_vi == 'phan_tram':
# 				return (self.phu_thu + 100) * self.billable.don_gia / 100
# 			else:
# 				return self.phu_thu
# 		else:
# 			return 0
#
# 	billable = models.OneToOneField(Billable, blank=True, null=True, verbose_name=u'khoản thu')
# 	phu_thu = models.FloatField(verbose_name=u'phụ thu')
# 	don_vi = models.CharField(verbose_name=u'đơn vị', max_length=20, choices=DISCOUNT_CHOICES, default='phan_tram')
# 	dien_giai = models.CharField(verbose_name=u'diễn giải', max_length=100, default=u'phụ thu')
# 	kich_hoat = models.BooleanField(verbose_name=u'kích hoạt', default=True)


class LoThuoc(models.Model):

	class Meta:
		verbose_name = u'lô thuốc'
		verbose_name_plural = u'danh sách các lô thuốc'

	def __unicode__(self):
		return u'%s:%s, số lượng %s, nhập ngày %s' % (self.ma_phi, self.dien_giai, self.so_luong, self.ngay_nhap)

	def save(self, *args, **kwargs):
		if self.trang_thai == 'chua_nhap': #Update thuoc quantity
			thuoc = Thuoc.objects.filter(ma_phi=self.ma_phi)
			if not thuoc:
				thuoc = Thuoc(
					ma_phi=self.ma_phi,
					dien_giai=self.dien_giai,
					ton_kho=self.so_luong,
					don_gia=self.gia_nhap,
				)
			else:
				thuoc.ton_kho += self.so_luong
			thuoc.save()
			self.trang_thai == 'da_nhap'
		super(LoThuoc, self).save(*args, **kwargs)

	so_luong = models.IntegerField(verbose_name=u'số lượng')
	ma_phi = models.CharField(max_length=20, verbose_name=u'mã thuốc')
	dien_giai = models.CharField(max_length=100, verbose_name=u'tên thuốc')
	het_han = models.DateField(blank=True, null=True, verbose_name=u'ngày hết hạn')
	ngay_sx = models.DateField(blank=True, null=True, verbose_name=u'ngày sản xuất')
	ngay_nhap = models.DateField(blank=True, null=True, verbose_name=u'ngày nhập')
	nguoi_nhap = models.CharField(max_length=50, verbose_name=u'người nhập')
	cung_cap = models.CharField(max_length=100, blank=True, verbose_name=u'nhà cung cấp')
	san_xuat = models.CharField(max_length=100, blank=True, verbose_name=u'nhà sản xuất')
	ghi_chu = models.TextField(blank=True, verbose_name=u'ghi chú')
	trang_thai = models.CharField(default=u'chua_nhap', max_length=20, choices=LOTHUOC_STATUS,
								  verbose_name='trạng thái')
	gia_nhap = models.FloatField(default=0, verbose_name=u'giá nhập')


class MauToa(models.Model):
	class Meta:
		verbose_name = u'mẫu toa'
		verbose_name_plural = u'các mẫu toa'

	def __unicode__(self):
		return ' '.join([self.ma_thuoc, self.so_luong, self.dv_dung, \
						 self.tan_suat, self.thoi_diem, self.so_lan, u'lần'])

	ma_thuoc = models.CharField(max_length=50)
	duong_dung = models.CharField(max_length=20, choices=DRUGROUTE_CHOICES, default=u'uong', verbose_name=u'đường dùng')
	tan_suat = models.CharField(max_length=20, choices=DRUGFREQ_CHOICES, default=u'ngay_hai')
	thoi_diem = models.CharField(max_length=20, choices=DRUGTIME_CHOICES, default=u'sau_an')
	dv_dung = models.CharField(max_length=20, choices=DRUGUSE_CHOICES, default=u'vien', verbose_name=u'đơn vị dùng')
	so_luong = models.FloatField(default=1)
	so_lan = models.IntegerField(default=2)


class ToaThuoc(models.Model):
	class Meta:
		verbose_name = u'toa thuốc'
		verbose_name_plural = u'các toa thuốc'

	def __unicode__(self):
		return '-'.join(filter(None, (self.ma_bn, self.ho_ten, self.cuoc_hen.__unicode__(), self.ghi_chu)))

	bac_sy = models.ForeignKey(User, blank=True, null=True, verbose_name=u'bác sỹ')
	ma_bn = models.CharField(max_length=20, verbose_name=u'mã BN')
	ho_ten = models.CharField(max_length=50, verbose_name=u'bệnh nhân')
	cuoc_hen = models.ForeignKey('CuocHen', blank=True, null=True, verbose_name=u'Cuộc hẹn')
	ghi_chu = models.TextField(verbose_name=u'lời dặn', blank=True)
	ngay_ghi = models.DateTimeField(verbose_name=u'ngày ghi', auto_now_add=True)
	cap_nhat = models.DateTimeField(verbose_name=u'cập nhật', auto_now=True)
	trang_thai = models.CharField(max_length=20, verbose_name=u'trạng thái', choices=TOATHUOC_STATUS,
								  default=u'cho_duyet')
	huy = models.BooleanField(verbose_name=u'huỷ', default=False)


class DongToa(models.Model):
	class Meta:
		verbose_name = u'dòng toa'
		verbose_name_plural = u'các dòng toa'

	def __unicode__(self):
		return ' '.join(filter(None, [self.ma_thuoc, self.ten_thuoc, str(self.so_luong)]))

	toa_thuoc = models.ForeignKey(ToaThuoc)
	ma_thuoc = models.CharField(verbose_name=u'Mã thuốc', max_length=50, blank=True)
	ten_thuoc = models.CharField(verbose_name=u'Tên thuốc', max_length=200)
	so_luong = models.FloatField(verbose_name=u'số lượng', blank=True, null=True, default=1)
	don_vi = models.CharField(verbose_name=u'Đơn vị', max_length=20, choices=DRUGFORM_CHOICES, default=u'vien')

	duong_dung = models.CharField(verbose_name=u'đường dùng', max_length=20, choices=DRUGROUTE_CHOICES, default=u'uong')
	sang = models.FloatField(verbose_name=u'sáng', blank=True, null=True, default=0)
	trua = models.FloatField(verbose_name=u'trưa', blank=True, null=True, default=0)
	chieu = models.FloatField(verbose_name=u'chiều', blank=True, null=True, default=0)
	toi = models.FloatField(verbose_name=u'tối', blank=True, null=True, default=0)
	ghi_chu = models.TextField(verbose_name=u'ghi chú', blank=True)


class XuatThuoc(models.Model):
	class Meta:
		verbose_name = u'xuất thuốc'
		verbose_name_plural = u'xuất thuốc'

	def __unicode__(self):
		return ' '.join(filter(None, [self.ma_thuoc, self.ten_thuoc, str(self.so_luong)]))

	superbill = models.ForeignKey(SuperBill)
	toa_thuoc = models.ForeignKey('ToaThuoc')
	ma_thuoc = models.CharField(verbose_name=u'Mã thuốc', max_length=50, blank=True)
	ten_thuoc = models.CharField(verbose_name=u'Tên thuốc', max_length=200, blank=True)
	so_luong = models.FloatField(verbose_name=u'số lượng', blank=True, null=True, default=1)
	don_vi = models.CharField(verbose_name=u'Đơn vị', max_length=20, choices=DRUGFORM_CHOICES, default=u'vien')

	duong_dung = models.CharField(verbose_name=u'đường dùng', max_length=20, choices=DRUGROUTE_CHOICES, default=u'uong')
	sang = models.FloatField(verbose_name=u'sáng', blank=True, null=True, default=0)
	trua = models.FloatField(verbose_name=u'trưa', blank=True, null=True, default=0)
	chieu = models.FloatField(verbose_name=u'chiều', blank=True, null=True, default=0)
	toi = models.FloatField(verbose_name=u'tối', blank=True, null=True, default=0)
	ghi_chu = models.TextField(verbose_name=u'ghi chú', blank=True)


class HoaDon(models.Model):
	class Meta:
		verbose_name = u'hoá đơn'
		verbose_name_plural = u'sổ hoá đơn'

	def __unicode__(self):
		return '%s: %s (%s)' % (self.ma_bn, self.ma_hd, self.tong_so)

	def save(self, *args, **kwargs):
		if self.id is None: # First save
			super(HoaDon, self).save(*args, **kwargs)
		if not self.ma_hd:
			now = timezone.now()
			self.ma_hd = 'HD%s%s%04d' % (now.year % 100, now.month, self.id % 10000)
		super(HoaDon, self).save(*args, **kwargs)

	ma_bn = models.CharField(max_length=20, verbose_name=u'mã BN')
	ma_hd = models.CharField(max_length=20, verbose_name=u'mã số hoá đơn')
	tieu_de = models.CharField(max_length=50, verbose_name=u'tiêu đề')
	cuoc_hen = models.ForeignKey('CuocHen', verbose_name=u'cuộc hẹn')
	trang_thai = models.CharField(max_length=20, verbose_name=u'trạng thái', choices=HD_STATUS, default=u'dang_mo')
	luc_ghi = models.DateTimeField(verbose_name=u'lúc ghi', auto_now_add=True)
	ket_thuc = models.DateTimeField(verbose_name=u'kết thúc', blank=True, null=True)
	tong_so = models.FloatField(verbose_name=u'cân đối', default=0.0)
	so_dong = models.IntegerField(verbose_name=u'số dòng', default=0)
	loai_hd = models.CharField(max_length=20, verbose_name=u'loại HĐ', choices=HD_CHOICES, default=u'tam_tinh')
	huy = models.BooleanField(verbose_name=u'huỷ', default=False)


class DongPhi(models.Model):
	class Meta:
		verbose_name = u'dòng chi phí'
		verbose_name_plural = u'toàn bộ chi phí'

	def thanh_tien(self):
		return thanh_tien(
			unit_price=self.unit_price,
			item_qty=self.item_qty,
			item_discount=self.item_discount,
			discount_base=self.discount_base
		)

	def __unicode__(self):
		return '%s: %s %s (%s)' % (self.item_code, self.item_description, self.item_qty, self.unit_price)

	hoa_don = models.ForeignKey(HoaDon, verbose_name=u'hoá đơn')
	# Item code link toi Billable
	item_code = models.CharField(max_length='20', verbose_name=u'mã phí')
	superbill = models.ForeignKey('SuperBill', verbose_name=u'superbill ref', blank=True, null=True)
	item_description = models.CharField(max_length='100', verbose_name=u'mục chi')
	unit_price = models.FloatField(verbose_name=u'đơn giá', default=0)
	item_qty = models.IntegerField(verbose_name=u'số lượng', default=1)
	item_unit = models.CharField(max_length=20, verbose_name=u'đơn vị', choices=UNIT_CHOICES, blank=True)
	item_discount = models.IntegerField(verbose_name=u'giảm giá', default=0)
	discount_base = models.CharField(max_length=20, verbose_name=u'đv giảm', choices=DISCOUNT_CHOICES,
									 default=u'phan_tram')


class ChiTra(models.Model):
	class Meta:
		verbose_name = u'chi trả'
		verbose_name_plural = u'các khoản chi trả'

	hoa_don = models.ForeignKey(HoaDon, verbose_name=u'hoá đơn')
	khoan_tien = models.FloatField(verbose_name=u'khoản tiền', default=0)
	ngay_ghi = models.DateTimeField(u'ngày ghi', auto_now_add=True)
	ghi_chu = models.CharField(max_length='100', verbose_name=u'ghi chú')
	bao_hiem = models.BooleanField(verbose_name=u'BHYT', default=False)


class BenhNhan(models.Model):
	class Meta:
		verbose_name = _(u'bệnh nhân')
		verbose_name_plural = _(u'danh sách bệnh nhân')

	def __unicode__(self):
		return '%s-%s' % (self.ma_bn, self.ho_ten)

	@property
	def last_visit(self):
		qs = CuocHen.objects.all().order_by('-bat_dau')[:1]
		return qs[0].bat_dau if qs else None

	@property
	def weeks_old(self):
		"""Return how many week old is this"""
		bat_dau = self.last_visit
		if bat_dau is None:
			return None
		if timezone.is_naive(bat_dau):
			timezone.make_aware(bat_dau, timezone.get_default_timezone())
		weeks = int((timezone.now() - bat_dau).days / 7) + 1
		return weeks

	@property
	def days_old(self):
		"""Return how many week old is this"""
		bat_dau = self.last_visit
		if bat_dau is None:
			return None
		if timezone.is_naive(bat_dau):
			timezone.make_aware(bat_dau, timezone.get_default_timezone())
		days = int((timezone.now() - bat_dau).days) + 1
		return days

	def save(self, *args, **kwargs):
		if self.id is None: # First save
			ma_thang = month_encode(timezone.now())
			# self.ma_bn = ''
			super(BenhNhan, self).save(*args, **kwargs)
		if not self.ma_bn:
			self.ma_bn = '%s%04d' % (ma_thang, self.id % 10000)
		super(BenhNhan, self).save(*args, **kwargs)

	objects = models.Manager()
	objects_search = SearchManager()
	"""Migrate with
	manage.py schemamigration int --empty

	def forwards(self, orm):
		db.execute('CREATE FULLTEXT INDEX book_text_index ON books_book (title, summary)')

	def backwards(self, orm):
		db.execute('DROP INDEX book_text_index on books_book')
	"""
	ma_bn = models.CharField(max_length=20, verbose_name=u'mã bệnh nhân', blank=True, db_index=True)
	ho_ten = models.CharField(max_length=50, verbose_name=u'họ tên', db_index=True)
	gioi_tinh = models.CharField(max_length=10, verbose_name=u'giới tính', choices=SEX_CHOICES, blank=True)
	ngay_sinh = models.DateField(verbose_name=u'ngày sinh', blank=True, null=True)
	nam_sinh = models.BooleanField(verbose_name=u'năm sinh', default=False)
	cmnd = models.CharField(max_length=10, verbose_name=u'Số CMND', blank=True)
	sdt = models.CharField(max_length=20, verbose_name=u'điện thoại', blank=True)
	bhyt = models.CharField(max_length=20, verbose_name=u'Số Thẻ BHYT', blank=True)
	bhyt_start = models.DateField(blank=True, null=True, verbose_name=u'Ngày bắt đầu BHYT')
	bhyt_expire = models.DateField(blank=True, null=True, verbose_name=u'Ngày kết thúc BHYT')
	noi_dk = models.CharField(max_length=50, verbose_name=u'nơi đăng ký', blank=True, default=u'nơi đk CKB')
	ma_kcb = models.CharField(max_length=10, verbose_name=u'mã CSKCBBĐ', blank=True, default='00000')
	dia_chi = models.TextField(verbose_name=u'địa chỉ', blank=True)
	ghi_chu = models.TextField(verbose_name=u'ghi chú', blank=True)
	ngay_ghi = models.DateTimeField(verbose_name=u'ngày ghi', auto_now_add=True)
	cap_nhat = models.DateTimeField(verbose_name=u'cập nhật', auto_now=True)


class CoSo(models.Model):
	class Meta:
		verbose_name = u'cơ sở'
		verbose_name_plural = u'danh sách các cơ sở'

	def __unicode__(self):
		return '%s-%s' % (self.ma_so, self.ten_goi)

	ma_so = models.CharField(max_length=5)
	ten_goi = models.CharField(max_length=100, verbose_name=u'tên đơn vị', db_index=True)
	dia_chi = models.TextField(verbose_name=u'địa chỉ', blank=True)
	kich_hoat = models.BooleanField(verbose_name=u'kích hoạt', default=True)


"TEST save()""""
from bv.models import BenhNhan
from django.db import transaction
import traceback
@transaction.commit_manually
def test_save():
	try:
		bn1 = BenhNhan.objects.create(ho_ten='TEST 1')
		bn2 = BenhNhan.objects.create(ho_ten='TEST 2')
		print bn2.stt_thang - bn1.stt_thang, bn2.ma_thang == bn1.ma_thang
		print bn1, bn2
		bn1.delete()
		bn2.delete()
		transaction.commit()
	except Exception, e:
		traceback.print_exc()
		transaction.rollback()

test_save()
"1,True"""


class Station(models.Model):
	class Meta:
		verbose_name = u'nhật trình'
		verbose_name_plural = u'hồ sơ nhật trình'

	def __unicode__(self):
		return u'%s-%s: %s %s' % (self.station, self.luc_ghi.strftime('%H:%M'), \
								  self.ma_bn, self.ho_ten)

	ma_bn = models.CharField(max_length=20, verbose_name=u'mã BN')
	ho_ten = models.CharField(max_length=50, verbose_name=u'bệnh nhân')
	cuoc_hen = models.ForeignKey('CuocHen')
	station = models.CharField(max_length=20, verbose_name=u'station', choices=STATION_CHOICES, default=u'tiep_tan')
	trang_thai = models.CharField(max_length=20, verbose_name=u'trạng thái', choices=STATION_STATUS,
								  default=u'cho_duyet')
	luc_ghi = models.DateTimeField(verbose_name=u'lúc ghi', auto_now_add=True)
	bat_dau = models.DateTimeField(verbose_name=u'bắt đầu', blank=True, null=True)
	ket_thuc = models.DateTimeField(verbose_name=u'kết thúc', help_text=u'bỏ trống=kéo dài 15p', blank=True, null=True)
	opts = JSONField(default={})


class CuocHen(models.Model):
	"Ghi lại những cuộc hẹn"

	class Meta:
		verbose_name = u'cuộc hẹn'
		verbose_name_plural = u'ds cuộc hẹn'
		get_latest_by = 'bat_dau'

	def __unicode__(self):
		return '%(patient)s %(title)s: %(start)s-%(end)s' % {
		'patient': self.ho_ten,
		'title': self.tieu_de + ' ' if self.tieu_de else '',
		'start': datetime.strftime(self.bat_dau, '%H:%M %d/%m/%y'),
		'end': datetime.strftime(self.ket_thuc, '%H:%M %d/%m/%y'),
		}

	def save(self, *args, **kwargs):
		if not self.bat_dau: self.bat_dau = timezone.now()
		if not self.ket_thuc: self.ket_thuc = self.bat_dau + timedelta(minutes=15)
		super(CuocHen, self).save(*args, **kwargs)

	ma_bn = models.CharField(max_length=20, verbose_name=u'mã BN')
	ho_ten = models.CharField(max_length=50, verbose_name=u'bệnh nhân')
	bat_dau = models.DateTimeField(verbose_name=u'bắt đầu', blank=True)
	ket_thuc = models.DateTimeField(verbose_name=u'kết thúc', help_text=u'bỏ trống=kéo dài 15p', blank=True, null=True)
	tieu_de = models.CharField(verbose_name=u'tiêu đề', max_length=255)
	# tien_trien = models.ForeignKey(Station, verbose_name=u'tiến triển', blank=True, null=True)
	bac_sy = models.ForeignKey(User, blank=True, null=True, verbose_name=u'bác sỹ', related_name='lich_lam_viec')
	ghi_chu = models.TextField(verbose_name=u'ghi chú', blank=True)
	nguoi_ghi = models.ForeignKey(User, blank=True, null=True, verbose_name=u'người ghi')
	ngay_ghi = models.DateTimeField(u'ngày ghi', auto_now_add=True)
	service_line = models.CharField(max_length=20, choices=SERVICE_LINES, default='dich_vu')

	"Xem thêm để mở rộng: https://github.com/bartekgorny/django-schedule"
# rule = models.ForeignKey(Rule, null = True, blank = True, verbose_name=u'lặp lại', help_text=_("Select '----' for a one time only event."))
# end_recurring_period = models.DateTimeField(u'ngưng lặp lại lúc', null = True, blank = True, help_text=_("This date is ignored for one time only events."))
# calendar = models.ForeignKey(Calendar, blank=True, null=True)


from django.db.models.signals import post_save
from django.dispatch import receiver
import django.dispatch

station_done = django.dispatch.Signal(providing_args=["cuoc_hen", "hoa_don"])


def thanh_tien(unit_price, item_qty, item_discount, discount_base):
	if discount_base == 'so_tien':
		res = (unit_price * item_qty) - item_discount
	elif discount_base == 'phan_tram':
		res = (unit_price * item_qty) * (100 - item_discount) / 100
	else:
		raise Exception('Discount base %s is not supported' % discount_base)
	return res


def xuatthuoc_factory(toa_thuoc):
	res = []
	if toa_thuoc:
		for dong_toa in toa_thuoc.dongtoa_set.all():
			thuoc_list = Thuoc.objects.filter(ma_phi=dong_toa.ma_thuoc)[:1]
			thuoc = thuoc_list[0] if thuoc_list else None
			if thuoc and thuoc.service_line:
				service_line = thuoc.service_line
			else:
				service_line = toa_thuoc.cuoc_hen.service_line

			superbill = SuperBill(
				item_code=dong_toa.ma_thuoc,
				item_description=dong_toa.ten_thuoc,
				unit_price=getattr(thuoc, 'don_gia', 0.0),
				item_qty=dong_toa.so_luong,
				item_unit=dong_toa.don_vi,
				service_line=service_line,
				cuoc_hen=toa_thuoc.cuoc_hen,
				loai_phi='thuoc',
			)
			superbill.save()
			xuat_thuoc = XuatThuoc(
				toa_thuoc=toa_thuoc,
				superbill=superbill,
				ma_thuoc=dong_toa.ma_thuoc,
				ten_thuoc=dong_toa.ten_thuoc,
				so_luong=dong_toa.so_luong,
				don_vi=dong_toa.don_vi,
				duong_dung=dong_toa.duong_dung,
				sang=dong_toa.sang,
				trua=dong_toa.trua,
				chieu=dong_toa.chieu,
				toi=dong_toa.toi,
				ghi_chu=dong_toa.ghi_chu,
			)
			xuat_thuoc.save()
			res.append(xuat_thuoc)
	return res


@receiver(station_done)
def refresh_workflow(sender, **kwargs):
	if sender == 'quay_thuoc':
		cuoc_hen = kwargs.pop('cuoc_hen')
		# Check how many Billables are created and setup station
		ket_thuc = ToaThuoc.objects.filter(
			trang_thai='ket_thuc', #da kiem tra
			cuoc_hen=cuoc_hen,
			huy=False
		)
		for tt in ket_thuc: #Deduct the quantity
			for xt in tt.xuatthuoc_set.filter():
				try:
					thuoc = Thuoc.objects.get(ma_phi=xt.ma_thuoc)
				except Thuoc.DoesNotExist:
					print u'Không tìm thấy: mã thuốc "%s", tên thuốc "%s"' % (xt.ma_thuoc, xt.ten_thuoc)
				else:
					thuoc.ton_kho -= xt.so_luong
					thuoc.save()

		kiem_tra = ToaThuoc.objects.filter(
			trang_thai='kiem_tra', #da kiem tra
			cuoc_hen=cuoc_hen,
			huy=False
		)
		cho_duyet = []
		for tt in kiem_tra:
			cho_duyet.extend(
				[xt.superbill for xt in tt.xuatthuoc_set.all() \
				 if xt.superbill.huy == False and xt.superbill.trang_thai == 'cho_duyet'
				])

		if cho_duyet:
			hoa_don = HoaDon(
				ma_bn=cuoc_hen.ma_bn,
				# ma_hd=cuoc_hen.ma_bn, 
				cuoc_hen=cuoc_hen,
				tieu_de=u'Thanh Toán Tiền Thuốc',
				tong_so=0.0,
				so_dong=0,
			)
			hoa_don.save()

			for superbill in cho_duyet:
				dong_phi = DongPhi(
					hoa_don=hoa_don,
					item_code=superbill.item_code,
					item_description=superbill.item_description,
					unit_price=superbill.unit_price,
					item_qty=superbill.item_qty,
					item_unit=superbill.item_unit,
					item_discount=superbill.item_discount,
					discount_base=superbill.discount_base,
					superbill=superbill,
				)
				dong_phi.save()
				superbill.trang_thai = 'cho_tra'
				superbill.save()
				hoa_don.tong_so += thanh_tien(dong_phi.unit_price, dong_phi.item_qty, dong_phi.item_discount,
											  dong_phi.discount_base)
				hoa_don.so_dong += 1
			hoa_don.save()
			kiem_tra.update(trang_thai='kiem_tra')
	elif sender == 'tiep_tan':
		cuoc_hen = kwargs.pop('cuoc_hen')
		# Check how many Billables are created and setup station
		cho_duyet = SuperBill.objects.filter(
			trang_thai='cho_duyet',
			cuoc_hen=cuoc_hen,
			huy=False,
		)
		if cho_duyet:
			hoa_don = HoaDon(
				ma_bn=cuoc_hen.ma_bn,
				# ma_hd=cuoc_hen.ma_bn, 
				cuoc_hen=cuoc_hen,
				tieu_de=u'Thanh Toán Viện Phí',
				tong_so=0.0,
				so_dong=0,
			)
			hoa_don.save()

			for superbill in cho_duyet:
				if superbill.station:
					station = Station(
						station=superbill.station,
						cuoc_hen=cuoc_hen,
						ma_bn=cuoc_hen.ma_bn,
						ho_ten=cuoc_hen.ho_ten,
					)
					station.save()
					superbill.tien_trien = station
				dong_phi = DongPhi(
					hoa_don=hoa_don,
					item_code=superbill.item_code,
					item_description=superbill.item_description,
					unit_price=superbill.unit_price,
					item_qty=superbill.item_qty,
					item_unit=superbill.item_unit,
					item_discount=superbill.item_discount,
					discount_base=superbill.discount_base,
					superbill=superbill,
				)
				dong_phi.save()
				superbill.trang_thai = 'cho_tra'
				superbill.save()
				hoa_don.tong_so += thanh_tien(dong_phi.unit_price, dong_phi.item_qty, dong_phi.item_discount,
											  dong_phi.discount_base)
				hoa_don.so_dong += 1
			hoa_don.save()
	elif sender == 'thu_ngan':
		cuoc_hen = kwargs.pop('cuoc_hen')
		hoa_don = kwargs.pop('hoa_don')
		# Get SuperBill list
		superbill_id_list = [dp.superbill.pk for dp in hoa_don.dongphi_set.all() if
							 dp.superbill and dp.superbill.trang_thai == 'cho_tra']
		superbill_list = SuperBill.objects.filter(pk__in=superbill_id_list).select_related('tien_trien')
		superbill_list.update(trang_thai='dang_cho')
		for superbill in superbill_list:
			superbill.tien_trien.trang_thai = 'dang_cho'
			superbill.tien_trien.save()

	elif sender in MODALITY_STATIONS: # Lam Sang
		# Update trang_thai cho quay thuoc
		cuoc_hen = kwargs.pop('cuoc_hen')
		ttl = ToaThuoc.objects.filter(
			cuoc_hen=cuoc_hen,
			trang_thai=u'cho_duyet',
			huy=False)

		ttl.update(trang_thai=u'dang_cho')
		cho_duyet = SuperBill.objects.filter(
			cuoc_hen=cuoc_hen,
			trang_thai=u'cho_duyet',
			tien_trien__station__in=PROCEDURE_STATIONS,
			huy=False)
		if cho_duyet:
			hoa_don = HoaDon(
				ma_bn=cuoc_hen.ma_bn,
				# ma_hd=cuoc_hen.ma_bn, 
				cuoc_hen=cuoc_hen,
				tieu_de=u'Thanh Toán Thủ Thuật/CLS',
				tong_so=0.0,
				so_dong=0,
			)
			hoa_don.save()
			for superbill in cho_duyet:
				dong_phi = DongPhi(
					hoa_don=hoa_don,
					item_code=superbill.item_code,
					item_description=superbill.item_description,
					unit_price=superbill.unit_price,
					item_qty=superbill.item_qty,
					item_unit=superbill.item_unit,
					item_discount=superbill.item_discount,
					discount_base=superbill.discount_base,
					superbill=superbill,
				)
				dong_phi.save()
				hoa_don.tong_so += thanh_tien(dong_phi.unit_price, dong_phi.item_qty, dong_phi.item_discount,
											  dong_phi.discount_base)
				hoa_don.so_dong += 1

				superbill.trang_thai = 'cho_tra'
				if superbill.station:
					station = Station(
						cuoc_hen=cuoc_hen,
						station=superbill.station,
						opts={},
						ma_bn=cuoc_hen.ma_bn,
						ho_ten=cuoc_hen.ho_ten,
					)
					station.save()
					superbill.tien_trien = station
				superbill.save()
			hoa_don.save()
		dang_cho = SuperBill.objects.filter(
			cuoc_hen=cuoc_hen,
			trang_thai=u'dang_cho',
			station=sender,
			huy=False,
		)
		dang_cho.update(trang_thai=u'ket_thuc')
		Station.objects.filter(
			cuoc_hen=cuoc_hen,
			station=sender,
			trang_thai='dang_cho'
		).update(trang_thai='bat_dau')
	elif sender in PROCEDURE_STATIONS:
		cuoc_hen = kwargs.pop('cuoc_hen')
		dang_cho = SuperBill.objects.filter(
			cuoc_hen=cuoc_hen,
			trang_thai=u'dang_cho',
			station=sender,
			huy=False,
		)
		dang_cho.update(trang_thai=u'ket_thuc')
		Station.objects.filter(
			cuoc_hen=cuoc_hen,
			station=sender,
			trang_thai='dang_cho'
		).update(trang_thai='ket_thuc')
		Station.objects.filter(
			cuoc_hen=cuoc_hen,
			station__in=MODALITY_STATIONS,
			trang_thai='dang_cho'
		).update(trang_thai='dang_cho')
	elif sender == 'superbill':
		cuoc_hen = kwargs.pop('cuoc_hen')
		hoa_don = kwargs.pop('hoa_don')
		sum_dong_phi = 0
		for dong_phi in DongPhi.objects.filter(hoa_don=hoa_don):
			sum_dong_phi += dong_phi.thanh_tien()
		sum_chi_tra = ChiTra.objects.filter(hoa_don=hoa_don).aggregate(Sum('khoan_tien'))['khoan_tien__sum']
		if sum_chi_tra is None:
			sum_chi_tra = 0
		can_doi = sum_dong_phi - sum_chi_tra
		if can_doi < -1:
			dong_phi = DongPhi(
				hoa_don=hoa_don,
				item_code='chinh_hd',
				item_description=u'Chỉnh sửa HĐ',
				unit_price=-can_doi,
				item_qty=1,
			)
			dong_phi.save()
		hoa_don.tong_so = 0
		hoa_don.trang_thai = 'thanh_toan'
		hoa_don.save()


def tao_phu_thu(superbill_qs):
	superbill_filtered = superbill_qs.filter(phu_thu__gt=1)
	if superbill_filtered:
		tong = sum(
			[thanh_tien(superbill.phu_thu, superbill.item_qty, 0, 'phan_tram') for superbill in superbill_filtered])
		lines = len(superbill_filtered)
		hoa_don = HoaDon(
			ma_bn=superbill_filtered[0].cuoc_hen.ma_bn,
			tieu_de=u'Các khoản phụ thu',
			cuoc_hen=superbill_filtered[0].cuoc_hen,
			tong_so=tong,
			so_dong=lines,
		)
		hoa_don.save()
		for superbill in superbill_filtered:
			dong_phi = DongPhi(
				hoa_don=hoa_don,
				item_code='phuthu',
				item_description=u'%s (%s)' % (superbill.item_description, superbill.unit_price),
				unit_price=superbill.phu_thu,
				item_qty=superbill.item_qty,
				item_unit=superbill.item_unit,
			)
			dong_phi.save()
		return hoa_don.pk
	else:
		return None