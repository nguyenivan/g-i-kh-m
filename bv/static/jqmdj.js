$( document ).bind( 'mobileinit', function(){
  $.mobile.loader.prototype.options.text = "loading";
  $.mobile.loader.prototype.options.textVisible = true;
  $.mobile.loader.prototype.options.theme = "c";
  $.mobile.loader.prototype.options.html = "";
  $.mobile.defaultPageTransition = "none";
  $.mobile.defaultDialogTransition = "none";
});

yourlabs.TextWidget.prototype.getValue = function(choice) {
    return choice.data('value');
};

$.fn.refreshYearOnly = function () {
    var theLabel = $(this).find("label[for=id_ngay_sinh]");
    var theInput = $(this).find("#id_ngay_sinh");
    var theHidden = $(this).find("#id_nam_sinh");
    if (theHidden.val()=="True"){
        //    Saving the original date
        theInput.data("original", theInput.val());
        theLabel.text("Năm sinh");
        theLabel.addClass("yearonly");
        theInput.inputmask("9999");
        theInput.val(theInput.data("original").slice(-4));
    } else {
        theLabel.text("Ngày sinh");
        theLabel.removeClass("yearonly");
        theInput.inputmask("99/99/9999");
        if (theInput.data("original")) {
            theInput.val(theInput.data("original"));
        }
    }
};

(function($){$.notifyBar=function(d){var e=parseInt(1E8*Math.random(),0),b={},a={},a=$.extend({html:"Your message here",delay:2E3,animationSpeed:200,cssClass:"",jqObject:"",close:!1,closeText:"Close [X]",closeOnClick:!0,closeOnOver:!1,onBeforeShow:null,onShow:null,onBeforeHide:null,onHide:null,position:"top"},d);this.fn.showNB=function(){"function"===typeof a.onBeforeShow&&a.onBeforeShow.call();$(this).stop().slideDown(asTime,function(){"function"===typeof a.onShow&&a.onShow.call()})};this.fn.hideNB= function(d){"function"===typeof a.onBeforeHide&&a.onBeforeHide.call();$(this).stop().slideUp(asTime,function(){b.attr("id")==="__notifyBar"+e?$(this).slideUp(asTime,function(){$(this).remove();"function"===typeof a.onHide&&a.onHide.call()}):$(this).slideUp(asTime,function(){"function"===typeof a.onHide&&a.onHide.call()})})};a.jqObject?(b=a.jqObject,a.html=b.html()):b=$("<div></div>").addClass("jquery-notify-bar").addClass(a.cssClass).attr("id","__notifyBar"+e);d=$("<span></span>").addClass("notify-bar-text-wrapper").html(a.html); b.html(d).hide();b.attr("id");switch(a.animationSpeed){case "slow":asTime=600;break;case "default":case "normal":asTime=400;break;case "fast":asTime=200;break;default:asTime=a.animationSpeed}"object"!==b&&$("body").prepend(b);a.close&&(a.delay=Math.pow(10,9),b.append($("<a href='#' class='notify-bar-close'>"+a.closeText+"</a>")),$(".notify-bar-close").click(function(a){a.preventDefault();b.hideNB()}));0<$(".jquery-notify-bar:visible").length?$(".jquery-notify-bar:visible").stop().slideUp(asTime,function(){b.showNB()}): b.showNB();a.closeOnClick&&b.click(function(){b.hideNB()});a.closeOnOver&&b.mouseover(function(){b.hideNB()});setTimeout(function(){b.hideNB(a.delay)},a.delay+asTime);"bottom"===a.position?b.addClass("bottom"):"top"===a.position&&b.addClass("top")}})(jQuery);


