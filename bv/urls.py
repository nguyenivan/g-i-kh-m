from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

urlpatterns = patterns('',
	url(r'^$', 'bv.views.trang_chu' , name='bv_trang_chu'),
	url(r'^test/$', 'bv.views.test' , name='bv_test'),
	url(r'^nhanbenh/$', 'bv.views.nhan_benh' , name='bv_nhan_benh'),
	url(r'^nhanbenh/(?P<benhnhan_id>\d+)/$', 'bv.views.nhan_benh' , name='bv_nhan_benh_update'),
	url(r'^nhanbenh/(?P<benhnhan_id>\d+)/(?P<cuochen_id>\d+)/$', 'bv.views.nhan_benh' , name='bv_nhan_benh_history'),
	url(r'^thungan/$', 'bv.views.thu_ngan' , name='bv_thu_ngan'),
	url(r'^thungan/(?P<benhnhan_id>\d+)/$', 'bv.views.thu_ngan' , name='bv_thu_ngan_update'),
	url(r'^thungan/(?P<benhnhan_id>\d+)/(?P<cuochen_id>\d+)/$', 'bv.views.thu_ngan' , name='bv_thu_ngan_history'),
	url(r'^(?P<station>ngoaikhoa|mat|nhi|noikhoa|capcuu|khoamat|rhm|tmh|dalieu|phukhoa)/$', 'bv.views.lam_sang' , name='bv_lam_sang'),
	url(r'^(?P<station>ngoaikhoa|mat|nhi|noikhoa|capcuu|khoamat|rhm|tmh|dalieu|phukhoa)/(?P<benhnhan_id>\d+)/$', 'bv.views.lam_sang' , name='bv_lam_sang_update'),
	url(r'^(?P<station>ngoaikhoa|mat|nhi|noikhoa|capcuu|khoamat|rhm|tmh|dalieu|phukhoa)/(?P<benhnhan_id>\d+)/(?P<cuochen_id>\d+)/$', 'bv.views.lam_sang' , name='bv_lam_sang_history'),
	
	url(r'^(?P<station>xquang|xetnghiem|sieuam|tieuphau|luubenh)/$', 'bv.views.thu_thuat' , name='bv_thu_thuat'),
	url(r'^(?P<station>xquang|xetnghiem|sieuam|tieuphau|luubenh)/(?P<benhnhan_id>\d+)/$', 'bv.views.thu_thuat' , name='bv_thu_thuat_update'),
	url(r'^(?P<station>xquang|xetnghiem|sieuam|tieuphau|luubenh)/(?P<benhnhan_id>\d+)/(?P<cuochen_id>\d+)/$', 'bv.views.thu_thuat' , name='bv_thu_thuat_history'),
	url(r'^toathuoc/(?P<station>\w+)/(?P<benhnhan_id>\d+)/$', 'bv.views.toa_thuoc' , name='bv_toa_thuoc'),
	url(r'^toathuoc/(?P<station>\w+)/(?P<benhnhan_id>\d+)/(?P<toathuoc_id>\d+)/$', 'bv.views.toa_thuoc' , name='bv_toa_thuoc_update'),
	url(r'^nhathuoc/$', 'bv.views.nha_thuoc' , name='bv_nha_thuoc'),
	url(r'^nhathuoc/(?P<benhnhan_id>\d+)/$', 'bv.views.nha_thuoc' , name='bv_nha_thuoc_update'),
	url(r'^nhathuoc/(?P<benhnhan_id>\d+)/(?P<cuochen_id>\d+)/$', 'bv.views.nha_thuoc' , name='bv_nha_thuoc_history'),
	url(r'^quytrinh/$', 'bv.views.quy_trinh' , name='bv_quy_trinh'),
	url(r'^quytrinh/(?P<benhnhan_id>\d+)/$', 'bv.views.quy_trinh' , name='bv_quy_trinh_update'),
	url(r'^quytrinh/(?P<benhnhan_id>\d+)/(?P<cuochen_id>\d+)/$', 'bv.views.quy_trinh' , name='bv_quy_trinh_history'),
	url(r'^superbill/$', 'bv.views.superbill' , name='bv_superbill'),
	url(r'^superbill/(?P<benhnhan_id>\d+)/$', 'bv.views.superbill' , name='bv_superbill_update'),
	url(r'^superbill/(?P<benhnhan_id>\d+)/(?P<cuochen_id>\d+)/$', 'bv.views.superbill' , name='bv_superbill_history'),
	url(r'^in/hoadon/(?P<hoadon_id>\d+)/$', 'bv.views.in_hoadon' , name='bv_in_hoadon'),
	url(r'^in/toathuoc/(?P<toathuoc_id>\d+)/$', 'bv.views.in_toathuoc' , name='bv_in_toathuoc'),
	url(r'^in/xuatthuoc/(?P<toathuoc_id>\d+)/$', 'bv.views.in_xuatthuoc' , name='bv_in_xuatthuoc'),
	url(r'^in/superbill/(?P<benhnhan_id>\d+)/(?P<cuochen_id>\d+)/$', 'bv.views.in_superbill' , name='bv_in_superbill'),

	
)

