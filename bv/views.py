# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponseServerError, HttpResponse,\
	HttpResponseNotFound
from django.shortcuts import render, get_object_or_404
from bv.models import *
from bv.forms import *
from bv.utils import *
import re
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.utils import timezone
from bv.utils import generate_superbill
import logging
import urllib
import pdb

match_patterns = {
	'PATTERN_STT_FULL' : re.compile(r'^(?P<ma_thang>[a-zA-Z]{2})(?P<stt>\d{4}$)'),
	'PATTERN_STT_DIGIT' : re.compile(r'^(?P<stt>\d{4}$)'),
	'PATTERN_BHYT_FULL' : re.compile(r'^(?P<bhyt>[a-zA-Z]{2}\d{13}$)'),
	'PATTERN_BHYT_DIGIT' : re.compile(r'^(?P<bhyt>\d{5,}$)'),
	'PATTERN_TEN_BN' : re.compile(r'(?=^[\w\s]+$)^(?P<ho_ten>[^0-9]{2,})$', re.UNICODE),
}

PATTERN_ZIPPED = re.compile(r'\s+')


class CuocHenTodayNotFound(Exception):
	pass


def get_cuoc_hen_today(bn):
	"""Tìm và trả về cuoc_hen của ngày hôm nay"""
	if bn is None: return None
	today = timezone.now().day
	benh_nhan = bn if isinstance(bn, BenhNhan) else BenhNhan.objects.get(pk=bn)
	cuochen_today_list = CuocHen.objects.filter(ma_bn=benh_nhan.ma_bn,
		bat_dau__day=today).order_by('-bat_dau')[:1]
	if cuochen_today_list:
		return cuochen_today_list[0]
	else:
		return None


def get_or_create_ket_qua(cls, cuoc_hen, commit=False):
	if not issubclass(cls, KetQuaBase):
		raise Exception('%s type is not derrived from KetQuaBase class.' % cls)
	qs= cls.objects.filter(cuoc_hen=cuoc_hen).order_by('-luc_ghi')[:1]
	if qs:
		return qs[0]
	else:
		kq = cls(cuoc_hen=cuoc_hen)
		if commit:
			kq.save()
		return kq

def get_or_create_cuoc_hen(bn):
	"""Tìm và trả về cuoc_hen của ngày hôm nay"""
	if bn is None: return None
	today = timezone.now().day
	benh_nhan = bn if isinstance(bn, BenhNhan) else BenhNhan.objects.get(pk=bn)
	cuochen_today_list = CuocHen.objects.filter(ma_bn=benh_nhan.ma_bn,
		bat_dau__day=today).order_by('-bat_dau')[:1]
	if cuochen_today_list:
		cuochen_today = cuochen_today_list[0]
	else:
		cuochen_today = CuocHen(
			ma_bn = benh_nhan.ma_bn,
			ho_ten = benh_nhan.ho_ten,
			bat_dau = timezone.now(),
			tieu_de = u'Khám Bệnh (được tạo tự động)',
		)
		cuochen_today.save()
	return cuochen_today

def tim_benh(thong_tin):
	"""Ba loai thong tin: 
		So Thu Tu :		XX1234 hoac 1234
		So BHYT (15):	XX 1 22 22 333 55555 hoac 12345
		Ten Benh Nhan:	XXX YY ZZZ
	"""
	zipped = PATTERN_ZIPPED.sub('', thong_tin)
	for pattern in match_patterns:
		m = match_patterns[pattern].match(zipped)
		if m:
			if pattern == 'PATTERN_STT_FULL':
				object_list = BenhNhan.objects.filter(ma_bn=m.group('ma_thang')+m.group('stt'))
			elif pattern == 'PATTERN_STT_DIGIT':
				object_list = BenhNhan.objects.filter(ma_bn__endswith=m.group('stt'))
			elif pattern == 'PATTERN_BHYT_FULL':
				object_list = BenhNhan.objects.filter(bhyt=m.group('bhyt'))
			elif pattern == 'PATTERN_BHYT_DIGIT':
				object_list = BenhNhan.objects.filter(bhyt__endswith=m.group('bhyt'))
			elif pattern == 'PATTERN_TEN_BN':
				m = match_patterns[pattern].match(thong_tin)
				object_list = BenhNhan.objects_search.search(m.group('ho_ten'),('ho_ten',) )
			return (pattern, object_list)
	return (None,[])

def test(request):
	CHIDINH_LIST = [
u'HUYẾT HỌC',u'SINH HÓA',u'MIỄN DỊCH',u'TUMOR MARKERS',u'NƯỚC TIỂU',u'PHÂN',u'XÉT NGHIỆM GIUN',u'ĐÀM VÀ CÁC DỊCH SINH HỌC KHÁC',u'ĐIỆN CHUẨN ĐOÁN',u'SIÊU ÂM',u'X-QUANG']

	template = 'bv/test.html'
	form = ChiDinhForm()
	data = {
		'form': form,
		'chidinh_list':CHIDINH_LIST
	}
	return render(request, template,data)

def trang_chu(request):
	template = 'bv/trangchu.html'
	data= {'station_list': (
				(u'tiep_tan', u'Tiếp Tân', reverse('bv_nhan_benh')),
				(u'thu_ngan', u'Thu Ngân', reverse('bv_thu_ngan')),
				(u'noi_khoa', u'Khoa Nội', reverse('bv_lam_sang', kwargs={'station':'noikhoa'})),
				(u'cap_cuu', u'Cấp Cứu', reverse('bv_lam_sang', kwargs={'station':'capcuu'})),
				(u'phu_khoa', u'Phụ Khoa', reverse('bv_lam_sang', kwargs={'station':'phukhoa'})),
				(u'da_lieu', u'Da Liễu', reverse('bv_lam_sang', kwargs={'station':'dalieu'})),
				(u'nha_thuoc', u'Nhà Thuốc', reverse('bv_nha_thuoc')),
				(u'sieu_am', u'Siêu Âm', reverse('bv_thu_thuat', kwargs={'station':'sieuam'})),
				(u'x_quang', u'X-Quang', reverse('bv_thu_thuat', kwargs={'station':'xquang'})),
				(u'xet_nghiem', u'Xét Nghiệm', reverse('bv_thu_thuat', kwargs={'station':'xetnghiem'})),
				(u'ngoai_khoa', u'Khoa Ngoại', reverse('bv_lam_sang', kwargs={'station':'ngoaikhoa'})),
				(u'tai_mui_hong', u'Tai Mũi Họng', reverse('bv_lam_sang', kwargs={'station':'tmh'})),
				(u'rang_ham_mat', u'Răng Hàm Mặt', reverse('bv_lam_sang', kwargs={'station':'rhm'})),
				(u'khoa_nhi', u'Khoa Nhi', reverse('bv_lam_sang', kwargs={'station':'nhi'})),
				(u'khoa_mat', u'Khoa Mắt', reverse('bv_lam_sang', kwargs={'station':'mat'})),
				(u'tieu_phau', u'Tiểu Phẫu', reverse('bv_thu_thuat', kwargs={'station':'tieuphau'})),
				(u'luu_benh', u'Lưu Bệnh', reverse('bv_thu_thuat', kwargs={'station':'luubenh'})),
				(u'ke_toan', u'Kế Toán', reverse('bv_superbill')),
			)
	}
	return render(request, template, data)

def quy_trinh(request, benhnhan_id=None, cuochen_id=None):
	template = 'bv/quytrinh.html'
	form = NhanBenhForm(request.REQUEST)
	view_name = 'bv_quy_trinh'

	benh_nhan = get_object_or_404(BenhNhan, pk=benhnhan_id) \
					if benhnhan_id is not None else None
	cuoc_hen = get_object_or_404(CuocHen, pk=cuochen_id) \
					if cuochen_id is not None else get_cuoc_hen_today(benh_nhan)
	data = {
		'form': form,
		'view_name': view_name,
		'cuochen_list': CuocHen.objects.filter(ma_bn=benh_nhan.ma_bn) if benh_nhan else [],
		'benhnhan_id': benhnhan_id,
	}
	thong_tin = request.REQUEST.get('thong_tin','').strip()

	if request.method == 'POST': # Update
		if benh_nhan and cuoc_hen: # Display form
			action = request.POST.get('action')
			master = CuocHenForm(request.POST, instance = cuoc_hen)
			detail = QuyTrinhFormSet(request.POST, instance=cuoc_hen, prefix = 'detail')
			if action=='ghi':
				if all([master.is_valid(), detail.is_valid()]):
					master.save()
					detail.save()
					master = CuocHenForm(instance=cuoc_hen)
					detail = QuyTrinhFormSet(instance = cuoc_hen, prefix = 'detail')
			elif action=='xong':
				if all([master.is_valid(), detail.is_valid()]):
					master.save()
					detail.save()
					station_done.send(sender='tiep_tan', cuoc_hen=cuoc_hen)
					return HttpResponseRedirect(reverse(view_name))

			data['master'] = master
			data['detail'] = detail
		else: # 404
			return HttpResponseNotFound()
	elif request.method == 'GET':
		if benh_nhan:
			data['master'] = CuocHenForm(instance=cuoc_hen)
			data['detail'] = QuyTrinhFormSet(instance=cuoc_hen, prefix = 'detail')
		elif request.REQUEST.get('search'): # Search
			data['search']=True
			if  thong_tin:
				ket_qua = tim_benh(thong_tin)
				object_list=ket_qua[1]
				data['object_list'] = object_list
				instance = None
				if ket_qua[0] in ['PATTERNS_STT_FULL', 'PATTERN_BHYT_FULL'] and len(object_list) == 1:
					instance=object_list[0]
				elif not object_list:
					if ket_qua[0] == 'PATTERN_BHYT_FULL':
						instance = BenhNhan(bhyt=PATTERN_ZIPPED.sub('', thong_tin).upper())
						instance.save()
				if instance:
					return HttpResponseRedirect(reverse('bv_nhan_benh_update', kwargs={
						'benhnhan_id': instance.pk,
					}))
	return render(request, template, data)

def thu_thuat(request, station, benhnhan_id=None, cuochen_id=None):
	template = 'bv/thuthuat.html'
	form = NhanBenhForm()
	this_station = {
		'xquang':'x_quang',
		'sieuam': 'sieu_am',
		'xetnghiem': 'xet_nghiem',
		'tieuphau': 'tieu_phau',
		'luubenh': 'luu_benh',
	}.get(station)
	cls ={
		'xquang': KetQuaXQuang,
		'sieuam': KetQuaSieuAm,
		'xetnghiem': KetQuaXetNghiem,
	}.get(station)
	ThuThuatForm = modelform_factory(cls, fields=['ghi_chu'])
	view_name = 'bv_thu_thuat'
	benh_nhan = get_object_or_404(BenhNhan, pk=benhnhan_id) \
				if benhnhan_id is not None else None
	cuoc_hen = get_object_or_404(CuocHen, pk=cuochen_id) \
					if cuochen_id is not None else get_cuoc_hen_today(benh_nhan)
	data = {
		'form': form,
		'view_name': view_name,
		'cuochen_list': CuocHen.objects.filter(ma_bn=benh_nhan.ma_bn) if benh_nhan else [],
		'benhnhan_id': benhnhan_id,
		'this_station': this_station,
		'station': station,
		'station_label': dict(STATION_CHOICES)[this_station],
	}
	thong_tin = request.REQUEST.get('thong_tin','').strip()

	if request.method == 'POST': # To be replaced
		if benh_nhan and cuoc_hen:
			action = request.POST.get('action')
			ketqua_id = request.POST.get('ketqua_id')
			if ketqua_id:
				data['ketqua_id'] = ketqua_id
				data['master2']	= ThuThuatForm(request.POST, instance=get_object_or_404(cls, pk=ketqua_id), prefix='master2')
			else:
				data['master2']	= ThuThuatForm(request.POST, prefix='master2')
			data['master3']	= BenhNhanForm(request.POST, prefix='master3')
			data['master']	= CuocHenForm(request.POST, prefix='master')

			if data['master2'].is_valid():
				ket_qua = data['master2'].save(commit = False)
				if ket_qua.ghi_chu or ketqua_id:
					if action == 'ghi' or action=='xong': # Return to current view w/ updated data
						ket_qua.station = this_station
						ket_qua.cuoc_hen = cuoc_hen
						# TODO: Assign tien_trien to this
						ket_qua.save()
						data['master2'] = ThuThuatForm(instance=ket_qua, prefix='master2')
						data['ketqua_id'] = ket_qua.pk
					if action == 'xong': #Move to next station and return Home
						station_done.send(sender=this_station, cuoc_hen=cuoc_hen)
						return HttpResponseRedirect(reverse(view_name, kwargs={'station':station}))
		else:
			return HttpResponseNotFound()
	elif request.method == 'GET':
		if benh_nhan and cuoc_hen: # Display current CuocHen of the requested BenhNhan
			ket_qua = get_or_create_ket_qua(cls, cuoc_hen)
			ket_qua.station = this_station
			if ket_qua.pk:
				data['ketqua_id'] = ket_qua.pk
			data['master3']	= BenhNhanForm(prefix='master3', instance = benh_nhan)
			data['master2']	= ThuThuatForm(prefix='master2', instance = ket_qua)
			data['master']	= CuocHenForm(instance=cuoc_hen, prefix = 'master')
		else: #Display waiting list and search result (opt)
			if request.REQUEST.get('search'): # Search
				data['search']=True
				if  thong_tin:
					ket_qua = tim_benh(thong_tin)
					object_list=ket_qua[1]
					data['object_list'] = object_list
			# Return Waiting list
			today = timezone.now().day
			cac_cuochen_today = CuocHen.objects.filter(
				bat_dau__day =today,
			)
			cuochen_id_list = [b.cuoc_hen.pk for b in Station.objects.filter(
				cuoc_hen_id__in = [cuoc_hen.id for cuoc_hen in cac_cuochen_today],
				trang_thai__in = ['cho_duyet', 'dang_cho', 'bat_dau'],
				station=this_station,
			)]
			waiting_list = filter(lambda x: x.pk in cuochen_id_list, cac_cuochen_today )
			data['waiting_list'] = BenhNhan.objects.filter(ma_bn__in = [ch.ma_bn for ch in waiting_list])
	if cuoc_hen:
		chi_dinh = SuperBill.objects.filter(
			cuoc_hen=cuoc_hen,
			loai_phi__in = ['xet_nghiem', 'hinh_anh', 'tham_do', 'thu_thuat', 'ky_thuat'],
			huy= False
		)
		bao_hiem= chi_dinh.filter(service_line='bao_hiem')
		dich_vu =chi_dinh.filter(service_line='dich_vu')
		data['chi_dinh'] = []
		for x in xrange(0,max( len(bao_hiem), len(dich_vu) )):
			if x<len(bao_hiem):
				data['chi_dinh'].append(bao_hiem[x])
			else:
				data['chi_dinh'].append('')

			if x<len(dich_vu):
				data['chi_dinh'].append(dich_vu[x])
			else:
				data['chi_dinh'].append('')

		data['cuochen_id'] =  cuoc_hen.pk
		data['station'] = station
	return render(request, template, data)


def superbill(request, benhnhan_id=None, cuochen_id=None):
	template = 'bv/superbill.html'
	form = NhanBenhForm()
	view_name = 'bv_superbill'
	benh_nhan = get_object_or_404(BenhNhan, pk=benhnhan_id) \
					if benhnhan_id is not None else None
	cuoc_hen = get_object_or_404(CuocHen, pk=cuochen_id) \
					if cuochen_id is not None else get_cuoc_hen_today(benh_nhan)
	if cuoc_hen:
		cuochenid = cuoc_hen.pk
	else:
		cuochenid = cuochen_id
	data = {
		'form': form,
		'view_name': view_name,
		'cuochen_list': CuocHen.objects.filter(ma_bn=benh_nhan.ma_bn) if benh_nhan else [],
		'benhnhan_id': benhnhan_id,
		'cuochen_id': cuochenid,
	}
	total_superbill = 0.0
	if cuoc_hen:
		superbill_qs = SuperBill.objects.filter(cuoc_hen=cuoc_hen,huy=False)
		for superbill in superbill_qs:
			total_superbill += thanh_tien(
				superbill.unit_price,
				superbill.item_qty,
				superbill.item_discount,
				superbill.discount_base
			)
		data['sticky'] = '<ul>%s</ul>' % ''.join(filter(None,[
						'<li>%s</li>' % m for t,m in generate_superbill(superbill_qs, benh_nhan.bhyt, benh_nhan.ma_kcb) if t in ['message', 'warning']
					]))

	thong_tin = request.REQUEST.get('thong_tin','').strip()

	if request.method == 'POST':  # Update
		if benh_nhan and cuoc_hen: # Display form
			action = request.POST.get('action')
			hoadon_id = request.POST.get('hoadon_id')
			hoa_don = get_object_or_404(HoaDon, pk=hoadon_id)
			data['master'] = CuocHenFullForm(request.POST, instance = cuoc_hen, prefix='master')
			data['detail'] = SuperBillFormSet(request.POST, instance=cuoc_hen, prefix = 'detail')
			data['detail2'] = ChiTraFormSet(request.POST, instance=hoa_don, prefix='detail2')
			data['master3']=ChiTraReadonlyFormSet(request.POST, prefix='master3')
			data['hoadon_id'] = hoa_don.pk
			detail = data['detail']
			detail2 = data['detail2']
			master = data['master']
			if detail.is_valid() and detail2.is_valid() and master.is_valid():
				if action == 'ghi' or action == 'xong' or action == 'chinh':
					detail.save()
					detail2.save()
				can_doi = total_superbill
				for chitra in ChiTra.objects.filter(hoa_don__cuoc_hen=cuoc_hen):
					can_doi -= chitra.khoan_tien

				if action == 'chinh':
					superbill_qs = SuperBill.objects.filter(cuoc_hen=cuoc_hen, huy=False)
					hoadon_id = tao_phu_thu(superbill_qs)
					if hoadon_id is not None:
						return HttpResponseRedirect('%s?hoadon_id=%s' % (reverse('bv_thu_ngan_history', kwargs={
							'benhnhan_id': str(benhnhan_id),
							'cuochen_id': str(cuoc_hen.pk),
						}), hoadon_id))
					else:
						return HttpResponseRedirect(reverse('bv_thu_ngan_history', kwargs={
							'benhnhan_id': str(benhnhan_id),
							'cuochen_id': str(cuoc_hen.pk),
						}))
				elif action == 'ghi' or can_doi > 1:  # Won't finish if can_doi is not zero
					superbill_qs = SuperBill.objects.filter(cuoc_hen=cuoc_hen, huy=False)
					data['sticky'] = '<ul>%s</ul>' % ''.join(filter(None, [
						'<li>%s</li>' % m for t, m in generate_superbill(superbill_qs, benh_nhan.bhyt, benh_nhan.ma_kcb)
						if t in ['message', 'warning']
					]))
				elif action == 'xong':
					station_done.send(sender='superbill', cuoc_hen=cuoc_hen, hoa_don=hoa_don)
					return HttpResponseRedirect(reverse(view_name))
				data['detail'] = SuperBillFormSet(instance=cuoc_hen, prefix='detail')
				data['detail2'] = ChiTraFormSet(instance=hoa_don, prefix='detail2')
				data['master'] = CuocHenFullForm(instance=cuoc_hen, prefix='master')
				data['can_doi'] = can_doi
			else:
				form_list = [master] + list(detail) + list(detail2)
				data['error'] = error_summary(form_list)
		else:  # 404
			return HttpResponseNotFound()
	elif request.method == 'GET':
		if benh_nhan and cuoc_hen:
			can_doi = total_superbill
			for chitra in ChiTra.objects.filter(hoa_don__cuoc_hen=cuoc_hen):
				can_doi -= chitra.khoan_tien
			hoadon_id = request.GET.get('hoadon_id')
			if not hoadon_id:
				hoadon_qs = HoaDon.objects.filter(
					loai_hd='thanh_toan',
					trang_thai='dang_mo',
					cuoc_hen=cuoc_hen,
					huy=False,
				)[:1]
				if hoadon_qs:
					hoa_don = hoadon_qs[0]
				else:
					hoa_don = HoaDon(
						loai_hd='thanh_toan',
						ma_bn=cuoc_hen.ma_bn,
						cuoc_hen=cuoc_hen,
						tieu_de=u'Thanh Toán Viện Phí (%s)' % can_doi,
						tong_so=can_doi,
						so_dong=1,
					)
					hoa_don.save()
			else:
				hoa_don = get_object_or_404(HoaDon, pk=hoadon_id)
			data['master'] = CuocHenFullForm(instance = cuoc_hen, prefix='master')
			data['detail'] = SuperBillFormSet(instance=cuoc_hen, prefix = 'detail')
			data['detail2'] = ChiTraFormSet(instance=hoa_don, prefix='detail2')
			data['master3'] = ChiTraReadonlyFormSet(queryset=ChiTra.objects.filter(
				hoa_don__huy=False,
				hoa_don__cuoc_hen=cuoc_hen,
			), prefix = 'master3')
			data['hoadon_id'] = hoa_don.pk
			data['can_doi']= can_doi
		elif request.REQUEST.get('search'): # Search
			data['search']=True
			if  thong_tin:
				ket_qua = tim_benh(thong_tin)
				object_list=ket_qua[1]
				data['object_list'] = object_list
				instance = None
				if ket_qua[0] in ['PATTERNS_STT_FULL', 'PATTERN_BHYT_FULL'] and len(object_list) == 1:
					instance=object_list[0]
				elif not object_list:
					if ket_qua[0] == 'PATTERN_BHYT_FULL':
						instance = BenhNhan(bhyt=PATTERN_ZIPPED.sub('', thong_tin).upper())
						instance.save()
				if instance:
					return HttpResponseRedirect(reverse('bv_superbill_update', kwargs={
						'benhnhan_id': instance.pk,
					}))
	if cuoc_hen:
		data['hoadon_url'] = reverse('bv_thu_ngan_history',
			kwargs={'benhnhan_id': benhnhan_id, 'cuochen_id': cuoc_hen.id})
	return render(request, template, data)


def nha_thuoc(request, benhnhan_id=None, cuochen_id=None):
	template = 'bv/nhathuoc.html'
	form = NhanBenhForm()
	view_name = 'bv_nha_thuoc'
	this_station = 'nha_thuoc'
	benh_nhan = get_object_or_404(BenhNhan, pk=benhnhan_id) \
				if benhnhan_id is not None else None
	cuoc_hen = get_object_or_404(CuocHen, pk=cuochen_id) \
					if cuochen_id is not None else get_cuoc_hen_today(benh_nhan)
	data = {
		'form': form,
		'view_name': view_name,
		'this_station': this_station,
		'benhnhan_id': benhnhan_id,
		'cuochen_list': CuocHen.objects.filter(ma_bn=benh_nhan.ma_bn) if benh_nhan else [],
	}
	thong_tin = request.REQUEST.get('thong_tin','').strip()
	if request.method == 'POST': # Saving
		action = request.POST.get('action')
		new = request.POST.get('new')
		if new:
			tt = ToaThuoc(cuoc_hen=cuoc_hen, ma_bn = benh_nhan.ma_bn, ho_ten=benh_nhan.ho_ten, trang_thai='dang_cho')
			tt.save()
			toathuoc_id = tt.pk
			ttf = ToaThuocForm(instance=tt, prefix=str(toathuoc_id))
			xtfs = XuatThuocFormSet(instance=tt, prefix='xuatthuoc_%s' % toathuoc_id)
			dtfs = DongToaReadonlyFormSet(instance=tt, prefix='dongtoa_%s' % toathuoc_id )
		else:
			toathuoc_id = request.POST.get('toathuoc_id')
			tt = get_object_or_404(ToaThuoc, pk=toathuoc_id)
			ttf = ToaThuocForm(request.POST, instance=tt, prefix=str(toathuoc_id))
			xtfs = XuatThuocFormSet(request.POST, instance=tt, prefix='xuatthuoc_%s' % toathuoc_id)
			dtfs = DongToaReadonlyFormSet(instance=tt, prefix='dongtoa_%s' % toathuoc_id )
			if action == 'tao':
				xuatthuoc_factory(tt)
				xtfs = XuatThuocFormSet(instance=tt, prefix='xuatthuoc_%s' % toathuoc_id)

			if xtfs.is_valid() and ttf.is_valid():
				if action=='ghi' or action=='xong':
						tt = ttf.save()
						xtfs.save()
				if action=='xong':
					tt.trang_thai = {
						'dang_cho': 'kiem_tra',
						'thanh_toan': 'ket_thuc',
					}.get(tt.trang_thai, tt.trang_thai)
					station_done.send(sender='quay_thuoc', cuoc_hen=cuoc_hen)
					return HttpResponseRedirect(reverse(view_name))
				ttf = ToaThuocForm(instance=tt, prefix=str(toathuoc_id))
				xtfs = XuatThuocFormSet(instance=tt, prefix='xuatthuoc_%s' % toathuoc_id)
				data['message'] = u'Ghi Xong'
			else:
				form_list = [ttf] + list(xtfs)
				data['error'] = error_summary(form_list)

		data['inline' ] = [
			(
				toathuoc_id,
				ttf,
				dtfs,
				xtfs,
			)
		]
		data['toathuoc_id'] = toathuoc_id
	elif request.method == 'GET':
		if benh_nhan and cuoc_hen: # Display current CuocHen of the requested BenhNhan
			bn = get_object_or_404(BenhNhan,pk=benhnhan_id)
			# cuoc_hen = get_or_create_cuoc_hen(bn)
			ttl = ToaThuoc.objects.filter(
				cuoc_hen=cuoc_hen,
				trang_thai__in=['dang_cho', 'kiem_tra', 'thanh_toan', 'ket_thuc'],
				huy=False,
			)
			data['inline' ] = [
				(
					ttl[x].pk,
					ToaThuocForm(instance=ttl[x], prefix=str(ttl[x].pk)),
					DongToaReadonlyFormSet(instance=ttl[x], prefix='dongtoa_%s' % ttl[x].pk ),
					XuatThuocFormSet(instance=ttl[x], prefix='xuatthuoc_%s' % ttl[x].pk ),
				) for x in xrange(0,len(ttl))
			]

		else: #Display waiting list and search result (opt)
			if request.REQUEST.get('search'): # Search
				data['search']=True
				if  thong_tin:
					ket_qua = tim_benh(thong_tin)
					object_list=ket_qua[1]
					data['object_list'] = object_list
			# Return Waiting list
			this_station = 'nha_thuoc'
			today = timezone.now().day

			cac_toathuoc_today= ToaThuoc.objects.filter(
				ngay_ghi__day =today,
				trang_thai__in = ['dang_cho', 'thanh_toan'],
				huy=False,
			)

			data['waiting_list'] = BenhNhan.objects.filter(ma_bn__in = [ch.ma_bn for ch in cac_toathuoc_today])

	return render(request, template, data)

def toa_thuoc(request, station, benhnhan_id, toathuoc_id=None):
	if toathuoc_id is not None:
		view_name = 'bv_lam_sang_update'
		this_station= station
		data={
		'toathuoc_id': toathuoc_id,
		'this_station': this_station,
		}
		template = 'bv/toathuoc.html'
		toa_thuoc = get_object_or_404(ToaThuoc, pk=toathuoc_id)
		if request.method == 'POST': # To be replaced
			action = request.POST.get('action')
			ttf = ToaThuocForm(request.POST, instance=toa_thuoc)
			dtfs = DongToaFormSet(request.POST, instance=toa_thuoc)
			if all([ttf.is_valid(), dtfs.is_valid()]):
				toa_thuoc = ttf.save()
				dtfs.save()
				if action == 'ghi':
					ttf = ToaThuocForm(instance=toa_thuoc)
					dtfs = DongToaFormSet(instance=toa_thuoc)
				elif action == 'xong':
					return HttpResponseRedirect(reverse(view_name,  \
						kwargs={'station': station, 'benhnhan_id':benhnhan_id,}))
			data['ttf'] = ttf
			data['dtfs'] = dtfs
		elif request.method == 'GET':
			data['ttf'] = ToaThuocForm(instance=toa_thuoc)
			data['dtfs'] = DongToaFormSet(instance=toa_thuoc)
		return render(request, template, data)
	else: # Tao ra Toa moi va redirect
		if request.method=='POST':
			cuoc_hen = get_or_create_cuoc_hen(benhnhan_id)
			toa_thuoc = ToaThuoc(
				cuoc_hen = cuoc_hen,
				ma_bn = cuoc_hen.ma_bn,
				ho_ten = cuoc_hen.ho_ten,
			)
			toa_thuoc.save()
			return HttpResponseRedirect(reverse('bv_toa_thuoc_update', \
				kwargs={'station':station,'benhnhan_id':benhnhan_id, 'toathuoc_id':toa_thuoc.pk}))
		elif request.method=='GET':
			return HttpResponseRedirect(reverse(view_name, \
				kwargs={'benhnhan_id':benhnhan_id}))


# def noi_khoa(request, benhnhan_id=None, cuochen_id=None):
# 	return lam_sang(request,station='noi_khoa', \
# 		benhnhan_id=benhnhan_id, cuochen_id=cuochen_id)

def lam_sang(request, station, benhnhan_id=None, cuochen_id=None):
	CHIDINH_LIST = [
u'HUYẾT HỌC',u'SINH HÓA',u'MIỄN DỊCH',u'TUMOR MARKERS',u'NƯỚC TIỂU',u'PHÂN',u'XÉT NGHIỆM GIUN',u'ĐÀM VÀ CÁC DỊCH SINH HỌC KHÁC',u'ĐIỆN CHUẨN ĐOÁN',u'SIÊU ÂM',u'X-QUANG']

	template = 'bv/lamsang.html'
	form = NhanBenhForm()
	view_name = 'bv_lam_sang'
	this_station = {
		'noikhoa':'noi_khoa',
		'capcuu': 'cap_cuu',
		'dalieu': 'da_lieu',
		'phukhoa': 'phu_khoa',
		'khoamat': 'khoa_mat',
		'ngoaikhoa': 'ngoai_khoa',
		'tmh': 'tai_mui_hong',
		'rhm': 'rang_ham_mat',
		'nhi': 'khoa_nhi',
		'mat': 'khoa_mat',

	}.get(station)
	benh_nhan = get_object_or_404(BenhNhan, pk=benhnhan_id) \
				if benhnhan_id is not None else None
	cuoc_hen = get_object_or_404(CuocHen, pk=cuochen_id) \
					if cuochen_id is not None else get_cuoc_hen_today(benh_nhan)
	data = {
		'form': form,
		'form1':ChiDinhForm(),
		'view_name': 'bv_lam_sang',
		'cuochen_list': CuocHen.objects.filter(ma_bn=benh_nhan.ma_bn) if benh_nhan else [],
		'cuoc_hen':cuoc_hen,
		'benh_nhan':benh_nhan,
		'benhnhan_id': benhnhan_id,
		'this_station': this_station,
		'station': station,
		'chidinh_list':CHIDINH_LIST
	}
	thong_tin = request.REQUEST.get('thong_tin','').strip()
	if request.method == 'POST': # To be replaced
		if benh_nhan and cuoc_hen:
			action = request.POST.get('action')
			detail3 = KetQuaLamSangFormSet(request.POST, instance=cuoc_hen, prefix='detail3')
			detail1	= ChiDinhThuThuatFormSet(request.POST, instance=cuoc_hen, prefix='detail1')
			detail2	= DiUngFormSet(request.POST, instance=cuoc_hen, prefix='detail2')
			if all([detail3.is_valid(), detail1.is_valid(),detail2.is_valid()]):
				for kqf in detail3:
					if kqf.instance:
						kqf.instance.station = this_station
						kqf.cleaned_data['station'] = this_station
				if action == 'ghi' or action == 'xong': # Return to current view w/ updated data
					detail3.save()
					detail2.save()
					detail1.save()
				if action == 'xong': #Move to next station and return Home
					station_done.send(sender=this_station, cuoc_hen=cuoc_hen)
					return HttpResponseRedirect(reverse(view_name, kwargs={'station':station}))
				detail3	= KetQuaLamSangFormSet(instance=cuoc_hen, prefix='detail3')
				detail1	= ChiDinhThuThuatFormSet(instance=cuoc_hen, prefix='detail1')
				detail2	= DiUngFormSet(instance=cuoc_hen, prefix='detail2')
				data['message'] = u'Ghi Xong'
			else:
				form_list = list(detail3) + list(detail1) + list(detail2)
				data['error'] = error_summary(form_list)
			data['master']	= BenhNhanForm(prefix='master', instance = benh_nhan)
			data['master2']	= CuocHenForm(prefix='master2', instance = cuoc_hen)
			data['detail3'] = detail3
			data['detail1'] = detail1
			data['detail2'] = detail2
			data['toathuoc_list'] =  cuoc_hen.toathuoc_set.filter(huy=False)

			tt_dict = {'kqxn': KetQuaXetNghiem, 'kqxq': KetQuaXQuang, 'kqsa': KetQuaSieuAm}
			for tt, cls in tt_dict.iteritems():
				ket_qua = get_or_create_ket_qua(cls, cuoc_hen)
				if ket_qua.pk:
					data[tt] = modelform_factory(cls, fields=['ghi_chu'])\
							(instance=ket_qua, prefix=tt)
		else:
			return HttpResponseNotFound()

	elif request.method == 'GET':
		if benh_nhan and cuoc_hen: # Display current CuocHen of the requested BenhNhan
			data['master']	= BenhNhanForm(prefix='master', instance = benh_nhan)
			data['master2']	= CuocHenForm(prefix='master2', instance = cuoc_hen)
			data['detail3']	= KetQuaLamSangFormSet(instance=cuoc_hen, prefix='detail3')
			data['detail1']	= ChiDinhThuThuatFormSet(instance=cuoc_hen, prefix='detail1')
			data['detail2']	= DiUngFormSet(instance=cuoc_hen, prefix='detail2')
			data['toathuoc_list'] =  cuoc_hen.toathuoc_set.filter(huy=False)

			tt_dict = {'kqxn': KetQuaXetNghiem, 'kqxq': KetQuaXQuang, 'kqsa': KetQuaSieuAm}
			for tt, cls in tt_dict.iteritems():
				ket_qua = get_or_create_ket_qua(cls, cuoc_hen)
				if ket_qua.pk:
					data[tt] = modelform_factory(cls, fields=['ghi_chu'])\
							(instance=ket_qua, prefix=tt)
		else:  # Display waiting list and search result (opt)
			if request.REQUEST.get('search'): # Search
				data['search']=True
				if  thong_tin:
					ket_qua = tim_benh(thong_tin)
					object_list=ket_qua[1]
					data['object_list'] = object_list
			# Return Waiting list
			today = timezone.now().day
			cac_cuochen_today = CuocHen.objects.filter(
				bat_dau__day =today,
			)
			cuochen_id_list = [b.cuoc_hen.pk for b in Station.objects.filter(
				cuoc_hen_id__in = [cuoc_hen.id for cuoc_hen in cac_cuochen_today],
				trang_thai__in = ['cho_duyet', 'dang_cho', 'bat_dau'],
				station=this_station,
			)]
			waiting_list = filter(lambda x: x.pk in cuochen_id_list, cac_cuochen_today )
			data['waiting_list'] = BenhNhan.objects.filter(ma_bn__in = [ch.ma_bn for ch in waiting_list])
	return render(request, template, data)


def thu_ngan(request, benhnhan_id=None, cuochen_id=None):
	template = 'bv/thungan.html'
	form = NhanBenhForm()
	view_name = 'bv_thu_ngan'
	benh_nhan = get_object_or_404(BenhNhan, pk=benhnhan_id) if benhnhan_id is not None else None
	cuoc_hen = get_object_or_404(CuocHen, pk=cuochen_id) if cuochen_id is not None else get_cuoc_hen_today(benh_nhan)
	this_station = 'thu_ngan'
	data = {
		'form': form,
		'view_name': view_name,
		'benhnhan_id': benhnhan_id,
		'this_station': this_station,
		'cuochen_list': CuocHen.objects.filter(ma_bn=benh_nhan.ma_bn) if benh_nhan else [],
	}
	thong_tin = request.REQUEST.get('thong_tin', '').strip()
	if request.method == 'POST':  # Saving
		action = request.POST.get('action')
		hoadon_id = int(request.POST.get('hoadon_id'))
		hd = get_object_or_404(HoaDon, pk=hoadon_id)
		hdf = HoaDonForm(request.POST, instance=hd, prefix=str(hoadon_id))
		dpfs = DongPhiFormSet(request.POST, instance=hd, prefix='dongphi_%s' % hoadon_id)
		ctfs = ChiTraFormSet(request.POST, instance=hd, prefix='chitra_%s' % hoadon_id)
		if all([hdf.is_valid(), dpfs.is_valid(), ctfs.is_valid()]):
			dpfs.save()
			ctfs.save()
			hd = hdf.save()
			tong_so = 0
			so_dong = 0
			for dp in dpfs.cleaned_data:
				if dp:
					tong_so += thanh_tien(
						dp['unit_price'],
						dp['item_qty'],
						dp['item_discount'],
						dp['discount_base'],
						)
					so_dong += 1
			for ct in ctfs.cleaned_data:
				if ct:
					tong_so -= ct['khoan_tien']
			hd.tong_so = tong_so
			hd.so_dong = so_dong
			hd.save()
			if action == 'ghi' or action == 'xong':
				hdf = HoaDonForm(instance=hd, prefix=str(hoadon_id))
				dpfs = DongPhiFormSet(instance=hd, prefix='dongphi_%s' % hoadon_id)
				ctfs = ChiTraFormSet(instance=hd, prefix='chitra_%s' % hoadon_id)
			if action == 'xong':
				if hd.tong_so <= 1:
					hd.trang_thai = 'thanh_toan'
					hd.save()
					station_done.send(
						sender='thu_ngan',
						cuoc_hen=hd.cuoc_hen,
						hoa_don=hd
					)

		# hdl = HoaDon.objects.filter(cuoc_hen=cuoc_hen)
		# data['hdforms'] = [(
		# 	hdl[x].pk,
		# 	HoaDonForm(instance=hdl[x], prefix=str(hdl[x].pk)),
		# 	DongPhiFormSet(instance=hdl[x], prefix='dongphi_%s' % hdl[x].pk),
		# 	ChiTraFormSet(instance=hdl[x], prefix='chitra_%s' % hdl[x].pk),
		# ) for x in xrange(0, len(hdl))]

		data['hdforms'] = [(
			hoadon_id,
			hdf,
			dpfs,
			ctfs,
		)]

		data['hoadon_id'] = hoadon_id
	elif request.method == 'GET':
		if benhnhan_id:  # Display current CuocHen of the requested BenhNhan
			benh_nhan = get_object_or_404(BenhNhan, pk=benhnhan_id)
			hoadon_id = request.GET.get('hoadon_id', '')
			try:
				data['hoadon_id'] = int(hoadon_id)
			except ValueError:
				pass
			if request.REQUEST.get('all') is not None:
				hdl = HoaDon.objects.filter(ma_bn=benh_nhan.ma_bn, huy=False)
			else:
				hdl = HoaDon.objects.filter(
					cuoc_hen=cuoc_hen
				).order_by('-luc_ghi')
			data['hdforms' ] = [
				(
					hdl[x].pk,
					HoaDonForm(instance=hdl[x], prefix=str(hdl[x].pk)),
					DongPhiFormSet(instance=hdl[x], prefix='dongphi_%s' % hdl[x].pk ),
					ChiTraFormSet(instance=hdl[x], prefix='chitra_%s' % hdl[x].pk),
				) for x in xrange(0,len(hdl))
			]

		else:  # Display waiting list and search result (opt)
			if request.REQUEST.get('search'): # Search
				data['search'] = True
				if thong_tin:
					ket_qua = tim_benh(thong_tin)
					object_list = ket_qua[1]
					data['object_list'] = object_list
			# Return Waiting list
			this_station = 'thu_ngan'
			today = timezone.now().day
			cac_cuochen_today = CuocHen.objects.filter(
				bat_dau__day=today,
			)
			cuochen_id_list = [b.cuoc_hen.pk for b in SuperBill.objects.filter(
				cuoc_hen_id__in=[ch.id for ch in cac_cuochen_today],
				trang_thai='cho_tra',
			)]
			waiting_list = filter(lambda x: x.pk in cuochen_id_list, cac_cuochen_today)
			data['waiting_list'] = BenhNhan.objects.filter(ma_bn__in=[ch.ma_bn for ch in waiting_list])
	if cuoc_hen:
		data['superbill_url'] = reverse('bv_superbill_history',
			kwargs={'benhnhan_id': benhnhan_id, 'cuochen_id': cuoc_hen.id})
	return render(request, template, data)


def nhan_benh(request, benhnhan_id=None, cuochen_id=None):
	template = 'bv/nhanbenh.html'
	form = NhanBenhForm(request.REQUEST)
	benh_nhan = get_object_or_404(BenhNhan, pk=benhnhan_id) \
					if benhnhan_id is not None else None
	cuoc_hen = get_object_or_404(CuocHen, pk=cuochen_id) \
					if cuochen_id is not None else get_or_create_cuoc_hen(benh_nhan)
	view_name = 'bv_nhan_benh'
	this_station = 'tiep_tan'
	data = {
		'form': form,
		'view_name': view_name,
		'cuochen_list': CuocHen.objects.filter(ma_bn=benh_nhan.ma_bn) if benh_nhan else [],
		'benhnhan_id': benhnhan_id,
		'this_station': this_station,
	}

	thong_tin = request.REQUEST.get('thong_tin','').strip()
	if request.method == 'POST': # Update
		if benh_nhan and cuoc_hen:
			action = request.POST.get('action')
			master = BenhNhanForm(request.POST, instance=benh_nhan)
			detail = CuocHenForm(request.POST, instance = cuoc_hen, prefix = 'detail')
			detail2 = ChiDinhThuThuatFormSet(request.POST, instance=cuoc_hen, prefix='detail2')
			if all([master.is_valid(), detail.is_valid(), detail2.is_valid()]):
				if action=='ghi' or action=='xong':
					master.save()
					detail.save()
					detail2.save()
				if action=='xong':
					station_done.send(sender='tiep_tan', cuoc_hen=cuoc_hen)
					return HttpResponseRedirect(reverse(view_name))
				master = BenhNhanForm(instance=benh_nhan)
				detail = CuocHenForm(instance = cuoc_hen, prefix = 'detail')
				detail2 = ChiDinhThuThuatFormSet(instance=cuoc_hen, prefix='detail2')
				data['message'] = u'Ghi Xong'
			else:
				form_list = [master, detail] + list(detail2)
				data['error'] = error_summary(form_list)
			data['master'] = master
			data['detail'] = detail
			data['detail2'] = detail2
		else:
			return HttpResponseNotFound()

	elif request.method == 'GET':
		if benh_nhan:
			data['master'] = BenhNhanForm(instance=benh_nhan)
			data['detail'] = CuocHenForm(instance=cuoc_hen, prefix = 'detail')
			data['detail2'] = ChiDinhThuThuatFormSet(instance=cuoc_hen, prefix='detail2')
		if request.REQUEST.get('new'):
			instance = BenhNhan(ho_ten=thong_tin)
			instance.save()
			return HttpResponseRedirect(reverse('bv_nhan_benh_update', kwargs={
				'benhnhan_id': instance.pk,
			}))

		elif request.REQUEST.get('search'): # Search
			data['search']=True
			if  thong_tin:
				ket_qua = tim_benh(thong_tin)
				object_list=ket_qua[1]
				data['object_list'] = object_list
				instance = None
				if ket_qua[0] in ['PATTERNS_STT_FULL', 'PATTERN_BHYT_FULL'] and len(object_list) == 1:
					instance=object_list[0]
				elif not object_list:
					if ket_qua[0] == 'PATTERN_BHYT_FULL':
						instance = BenhNhan(bhyt=PATTERN_ZIPPED.sub('', thong_tin).upper())
						instance.save()
				if instance:
					return HttpResponseRedirect(reverse('bv_nhan_benh_update', kwargs={
						'benhnhan_id': instance.pk,
					}))

	return render(request, template, data)


def in_hoadon(request, hoadon_id):
	view_name = 'bv_in_hoadon'
	hoa_don = get_object_or_404(HoaDon, pk = hoadon_id)

	template = 'bv/in/hoadon.html'
	tongtien = 0.0
	tonggiam = 0.0
	for dongphi in DongPhi.objects.filter(hoa_don_id=hoadon_id):
		tongtien+= dongphi.thanh_tien()
		tonggiam+= dongphi.unit_price - dongphi.thanh_tien()

	# DONG CHI TRA
	chi_tra = ChiTra.objects.filter(hoa_don_id=hoadon_id)
	tongtientra = tongtien
	for o in chi_tra:
		tongtientra-= o.khoan_tien



	cuoc_hen_data = hoa_don.cuoc_hen
	if cuoc_hen_data:
		bacsy = cuoc_hen_data.bac_sy
		nguoighi = cuoc_hen_data.nguoi_ghi

	if not bacsy:
		bacsy = ""
	if not nguoighi:
		nguoighi = ""

	discount_detail =[]


	dongdv= hoa_don.so_dong/2

	conlai = hoa_don.tong_so
	data = {
		'view_name': view_name,
		'hoadon_id': hoadon_id,
		'hd_data': HoaDon.objects.filter(pk=hoadon_id),
		'bn_data': BenhNhan.objects.filter(ma_bn=hoa_don.ma_bn),
		'dongphi_data': DongPhi.objects.filter(hoa_don_id=hoa_don.pk),
		'bacsy':bacsy,
		'nguoighi':nguoighi,
		'tongtien':tongtien,
		'tonggiam': tonggiam,
		'conlai': conlai,
		'dongdv':dongdv,
		'chi_tra':chi_tra,
		'tongtientra':tongtientra,


	}
	return render(request, template, data)


def in_toathuoc(request, toathuoc_id):
	toa_thuoc = get_object_or_404(ToaThuoc, pk = toathuoc_id)

	dt_chandoan = KetQuaLamSang.objects.filter(cuoc_hen=toa_thuoc.cuoc_hen)
	bacsy = ''
	benhly = ','.join([chandoan.benh_ly for chandoan in dt_chandoan])

	template = 'bv/in/toathuoc.html'
	view_name='bv_in_toathuoc'
	if request.method == 'POST': # Saving
		toathuoc_id = request.POST.get('toathuoc_id')
	data = {
		'view_name': view_name,
		'toathuoc_id': toathuoc_id,
		'dt_data': DongToa.objects.filter(toa_thuoc = toathuoc_id),
		'bn_data': BenhNhan.objects.filter(ma_bn = toa_thuoc.ma_bn),
		'dt_bacsy': "",
		'dt_benhly' : benhly,
		'toa_thuoc':toa_thuoc,
	}
	return render(request, template, data)
def in_xuatthuoc(request,toathuoc_id):
	toa_thuoc = get_object_or_404(ToaThuoc, pk = toathuoc_id)

	dt_chandoan = KetQuaLamSang.objects.filter(cuoc_hen=toa_thuoc.cuoc_hen)
	bacsy = ''
	benhly = ','.join([chandoan.benh_ly for chandoan in  dt_chandoan])

	template = 'bv/in/xuatthuoc.html'
	view_name='bv_in_xuatthuoc'
	if request.method == 'POST': # Saving
		toathuoc_id = request.POST.get('toathuoc_id')
	data = {
		'view_name': view_name,
		'toathuoc_id': toathuoc_id,
		'dt_data': DongToa.objects.filter(toa_thuoc = toathuoc_id),
		'bn_data': BenhNhan.objects.filter(ma_bn = toa_thuoc.ma_bn),
		'xt_data': XuatThuoc.objects.filter(toa_thuoc = toathuoc_id),
		'dt_bacsy': "",
		'dt_benhly' : benhly,
		'toa_thuoc':toa_thuoc,
	}
	return render(request, template, data)

def in_superbill(request,benhnhan_id=None, cuochen_id=None):
	template = 'bv/in/bangKeChiPhi.html'
	view_name='bv_in_superbill'
	if request.method == 'POST': # Saving
		benhnhan_id = request.POST.get('benhnhan_id')
		cuochen_id = request.POST.get('cuochen_id')

	isBHYT = ""
	isnotBHYT = ""

	benhnhan = get_object_or_404(BenhNhan, pk = benhnhan_id)
	if benhnhan:
		if not benhnhan.bhyt:
			isBHYT = "□"
			isnotBHYT = "☑"
			benhnhan.bhyt_expire = ""
		else:
			isBHYT = "☑"
			isnotBHYT = "□"
	cuochen = get_object_or_404(CuocHen, pk = cuochen_id)
	dt_chandoan = KetQuaLamSang.objects.filter(cuoc_hen_id=cuochen_id)
	bacsy = ''
	benhly = ','.join([chandoan.benh_ly for chandoan in  dt_chandoan])
	mabenh = ', '.join([chandoan.ma_benh for chandoan in  dt_chandoan])
	superbill_data = SuperBill.objects.filter(cuoc_hen = cuochen.pk)
	 #
	 # chia loai cong viec
		# (u'kham_benh', u'Khám Bệnh'),
		# (u'xet_nghiem', u'Xét Nghiệm'),
		# (u'hinh_anh', u'Chẩn Đoán Hình Ảnh'),
		# (u'tham_do', u'Thăm Dò Chức Năng'),
		# (u'thu_thuat', u'Thủ Thuật/Phẫu Thuật'),
		# (u'ky_thuat', u'Kỹ Thuật Cao'),
		# (u'khac', u'Khac'),
	phichikhac = []
	khambenh =[]
	xetnghiem =[]
	hinhanh =[]
	thamdo =[]
	thuthuat =[]
	kythuat =[]
	thuoc_bh = []
	thuoc_dv = []
	# cac tong phi chi tiet
	tongkhambenh = 0.0
	tongxetnghiem = 0.0
	tonghinhanh = 0.0
	tongthamdo = 0.0
	tongthuthuat = 0.0
	tongkythuat = 0.0
	tongphichikhac=0.0
	# cac tong quy BHYT chi tiet
	tongquykhambenh = 0.0
	tongquyxetnghiem = 0.0
	tongquyhinhanh = 0.0
	tongquythamdo = 0.0
	tongquythuthuat = 0.0
	tongquykythuat = 0.0
	tongquyphichikhac = 0.0
	# cac tong nguoi benh tra chi tiet
	tongtrakhambenh = 0.0
	tongtraxetnghiem = 0.0
	tongtrahinhanh = 0.0
	tongtrathamdo = 0.0
	tongtrathuthuat = 0.0
	tongtrakythuat = 0.0
	tongtraphichikhac = 0.0

	tongtienthuoc = 0.0
	tongquythuoc = 0.0
	tongtrathuoc = 0.0
	tongphikhac = 0.0
	# 3 dong tong tien cuoi
	tongtien = 0.0
	tongquybhyt = 0.0
	tongchitra = 0.0
	tongbntra_coBHYT = 0.0
	tongbntra_khongBHYT = 0.0

	for o in superbill_data:
		if o.loai_phi == 'thuoc':
			tongtienthuoc += o.thanh_tien()
			tongquythuoc += o.quy_bhyt
			tongtrathuoc += o.thanh_tien() - o.quy_bhyt
			if o.service_line == 'bao_hiem':
				thuoc_bh.append(o)
			else :
				thuoc_dv.append(o)
		elif o.loai_phi == 'kham_benh':
			tongkhambenh += o.thanh_tien()
			tongquykhambenh += o.quy_bhyt
			tongtrakhambenh += o.thanh_tien() - o.quy_bhyt
			khambenh.append(o)
		elif o.loai_phi == 'xet_nghiem':
			tongxetnghiem += o.thanh_tien()
			tongquyxetnghiem += o.quy_bhyt
			tongtraxetnghiem += o.thanh_tien() - o.quy_bhyt
			xetnghiem.append(o)
		elif o.loai_phi == 	'hinh_anh':
			tonghinhanh += o.thanh_tien()
			tongquyhinhanh += o.quy_bhyt
			tongtrahinhanh += o.thanh_tien() - o.quy_bhyt
			hinhanh.append(o)
		elif o.loai_phi == 'tham_do':
			tongthamdo += o.thanh_tien()
			tongquythamdo += o.quy_bhyt
			tongtrathamdo += o.thanh_tien() - o.quy_bhyt
			thamdo.append(o)
		elif o.loai_phi == 'thu_thuat':
			tongthuthuat += o.thanh_tien()
			tongquythuthuat += o.quy_bhyt
			tongtrathuthuat += o.thanh_tien() - o.quy_bhyt
			thuthuat.append(o)
		elif o.loai_phi == 'ky_thuat':
			tongkythuat += o.thanh_tien()
			tongquykythuat += o.quy_bhyt
			tongtrakythuat += o.thanh_tien() - o.quy_bhyt
			kythuat.append(o)
		elif o.loai_phi == 'khac':
			tongphichikhac += o.thanh_tien()
			tongquyphichikhac += o.quy_bhyt
			tongtraphichikhac += o.thanh_tien() - o.quy_bhyt
			phichikhac.append(o)


		tongtien += o.thanh_tien()
		tongquybhyt += o.quy_bhyt
		if o.quy_bhyt == 0:
			tongbntra_khongBHYT += o.thanh_tien() - o.quy_bhyt
		else:
			tongbntra_coBHYT += o.thanh_tien() - o.quy_bhyt
		tongchitra += o.thanh_tien() - o.quy_bhyt


	chu_tongtien = tien_chu(tongtien)
	chu_tongquybhyt = tien_chu(tongquybhyt)
	chu_tongchitra = tien_chu(tongchitra)
	# ma bhyt
	a = benhnhan.bhyt
	if a and len(a)==15:
		a0 = a[0]+a[1]
		a1 = a[2]
		a2 = a[3]+a[4]
		a3 = a[5]+a[6]
		a4 = a[7]+a[8]+a[9]
		a5 = a[10]+a[11]+a[12]+a[13]+a[14]
		ma_bhyt = [a0,a1,a2,a3,a4,a5]
	else:
		ma_bhyt=' '*15
	# 
	isdungtuyen = ""
	istraituyen = ""
	if benhnhan.ma_kcb == MA_BAO_HIEM:
		isdungtuyen = "☑"
		istraituyen = "□"
	else:
		isdungtuyen = "□"
		istraituyen = "☑"

	data={
	'benhnhan_id':benhnhan_id,
	'cuochen_id':cuochen_id,
	'phichikhac':phichikhac,
	'benhnhan':benhnhan,
	'cuochen':cuochen,
	'isBHYT':isBHYT,
	'isnotBHYT':isnotBHYT,
	'tongtien':tongtien,
	'isdungtuyen':isdungtuyen,
	'istraituyen':istraituyen,
	'tongtienthuoc':tongtienthuoc,
	'tongphikhac':tongphikhac,
	'benhly':benhly,
	'mabenh':mabenh,
	# 'bhyt': list(benhnhan.bhyt),
	'ma_bhyt':ma_bhyt,
	'thuoc_bh': thuoc_bh,
	'thuoc_dv': thuoc_dv,
	'tongquybhyt':tongquybhyt,
	'tongchitra':tongchitra,
	'chu_tongtien' :chu_tongtien,
	'chu_tongquybhyt' :chu_tongquybhyt,
	'chu_tongchitra' :chu_tongchitra,
	'khambenh':khambenh,
	'xetnghiem' :xetnghiem,
	'hinhanh' :hinhanh,
	'thamdo' :thamdo,
	'thuthuat' :thuthuat,
	'kythuat' :kythuat,
	'tongkhambenh' :tongkhambenh,
	'tongxetnghiem' :tongxetnghiem,
	'tonghinhanh' :tonghinhanh,
	'tongthamdo' :tongthamdo,
	'tongthuthuat':tongthuthuat,
	'tongkythuat' :tongkythuat,
	# ---
	'tongquykhambenh':tongquykhambenh,
	'tongquyxetnghiem':tongquyxetnghiem,
	'tongquyhinhanh':tongquyhinhanh,
	'tongquythamdo':tongquythamdo,
	'tongquythuthuat':tongquythuthuat,
	'tongquykythuat':tongquykythuat,
	# cac tong nguoi benh tra chi tiet
	'tongtrakhambenh':tongtrakhambenh,
	'tongtraxetnghiem':tongtraxetnghiem,
	'tongtrahinhanh':tongtrahinhanh,
	'tongtrathamdo':tongtrathamdo,
	'tongtrathuthuat':tongtrathuthuat,
	'tongtrakythuat':tongtrakythuat,
	'tongquythuoc':tongquythuoc,
	'tongtrathuoc' :tongtrathuoc,
	'tongphichikhac' :tongphichikhac,
	'tongquyphichikhac' :tongquyphichikhac,
	'tongtraphichikhac':tongtraphichikhac,
	'phichikhac':phichikhac,
	'tongbntra_coBHYT':tongbntra_coBHYT,
	'tongbntra_khongBHYT':tongbntra_khongBHYT,
	}
	# ☑
	return render(request, template, data)
#######
