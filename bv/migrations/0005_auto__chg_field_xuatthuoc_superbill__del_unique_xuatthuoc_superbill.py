# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'XuatThuoc', fields ['superbill']
        db.delete_unique(u'bv_xuatthuoc', ['superbill_id'])


        # Changing field 'XuatThuoc.superbill'
        db.alter_column(u'bv_xuatthuoc', 'superbill_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['bv.SuperBill']))

    def backwards(self, orm):

        # Changing field 'XuatThuoc.superbill'
        db.alter_column(u'bv_xuatthuoc', 'superbill_id', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['bv.SuperBill'], unique=True))
        # Adding unique constraint on 'XuatThuoc', fields ['superbill']
        db.create_unique(u'bv_xuatthuoc', ['superbill_id'])


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'bv.benhly': {
            'Meta': {'object_name': 'BenhLy'},
            'icd10_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.ICD10Group']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loai_ma': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'ma_benh': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ten_benh': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'tieng_anh': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'bv.benhnhan': {
            'Meta': {'object_name': 'BenhNhan'},
            'bhyt': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'bhyt_expire': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'cap_nhat': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cmnd': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'dia_chi': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'ghi_chu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'gioi_tinh': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'ho_ten': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ma_bn': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '20', 'blank': 'True'}),
            'ngay_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ngay_sinh': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'bv.billable': {
            'Meta': {'object_name': 'Billable'},
            'dien_giai': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'don_gia': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ma_phi': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20', 'db_index': 'True'}),
            'service_line': ('django.db.models.fields.CharField', [], {'default': "'dich_vu'", 'max_length': '20'})
        },
        u'bv.chitra': {
            'Meta': {'object_name': 'ChiTra'},
            'bao_hiem': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ghi_chu': ('django.db.models.fields.CharField', [], {'max_length': "'100'"}),
            'hoa_don': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.HoaDon']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'khoan_tien': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'ngay_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'bv.congviec': {
            'Meta': {'object_name': 'CongViec', '_ormbases': [u'bv.Billable']},
            u'billable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['bv.Billable']", 'unique': 'True', 'primary_key': 'True'}),
            'loai_cv': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'station': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        u'bv.cuochen': {
            'Meta': {'object_name': 'CuocHen'},
            'bac_sy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'lich_lam_viec'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'bat_dau': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'ghi_chu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'ho_ten': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ket_thuc': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'ma_bn': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'ngay_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'nguoi_ghi': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'service_line': ('django.db.models.fields.CharField', [], {'default': "'dich_vu'", 'max_length': '20'}),
            'tieu_de': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'bv.diung': {
            'Meta': {'object_name': 'DiUng', '_ormbases': [u'bv.KetQuaBase']},
            u'ketquabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['bv.KetQuaBase']", 'unique': 'True', 'primary_key': 'True'}),
            'ma_thuoc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'ten_thuoc': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'bv.dongphi': {
            'Meta': {'object_name': 'DongPhi'},
            'discount_base': ('django.db.models.fields.CharField', [], {'default': "u'phan_tram'", 'max_length': '20'}),
            'hoa_don': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.HoaDon']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_code': ('django.db.models.fields.CharField', [], {'max_length': "'20'"}),
            'item_description': ('django.db.models.fields.CharField', [], {'max_length': "'100'"}),
            'item_discount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'item_qty': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'item_unit': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'superbill': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.SuperBill']", 'null': 'True', 'blank': 'True'}),
            'unit_price': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        u'bv.dongtoa': {
            'Meta': {'object_name': 'DongToa'},
            'don_vi': ('django.db.models.fields.CharField', [], {'default': "u'vien'", 'max_length': '20'}),
            'duong_dung': ('django.db.models.fields.CharField', [], {'default': "u'uong'", 'max_length': '20'}),
            'hd_dv_dung': ('django.db.models.fields.CharField', [], {'default': "u'vien'", 'max_length': '20', 'blank': 'True'}),
            'hd_so_lan': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'hd_so_luong': ('django.db.models.fields.FloatField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'hd_tan_suat': ('django.db.models.fields.CharField', [], {'default': "u'ngay_hai'", 'max_length': '20', 'blank': 'True'}),
            'hd_thoi_diem': ('django.db.models.fields.CharField', [], {'default': "u'sau_an'", 'max_length': '20', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ma_thuoc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'so_luong': ('django.db.models.fields.FloatField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'ten_thuoc': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'toa_thuoc': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.ToaThuoc']"})
        },
        u'bv.hoadon': {
            'Meta': {'object_name': 'HoaDon'},
            'cuoc_hen': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.CuocHen']"}),
            'huy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ket_thuc': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'luc_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ma_bn': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'ma_hd': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'so_dong': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tieu_de': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tong_so': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'trang_thai': ('django.db.models.fields.CharField', [], {'default': "u'dang_mo'", 'max_length': '20'})
        },
        u'bv.icd10chapter': {
            'Meta': {'object_name': 'ICD10Chapter'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'en_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vi_name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'bv.icd10group': {
            'Meta': {'object_name': 'ICD10Group'},
            'chapter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.ICD10Chapter']"}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'en_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vi_name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'bv.ketquabase': {
            'Meta': {'object_name': 'KetQuaBase'},
            'bac_sy': ('django.db.models.fields.related.ForeignKey', [], {'default': '1L', 'to': u"orm['auth.User']"}),
            'cap_nhat': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cuoc_hen': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.CuocHen']"}),
            'ghi_chu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'huy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'luc_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'station': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.Station']", 'null': 'True', 'blank': 'True'}),
            'superbill': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'bv.ketqualamsang': {
            'Meta': {'object_name': 'KetQuaLamSang', '_ormbases': [u'bv.KetQuaBase']},
            'benh_ly': ('django.db.models.fields.TextField', [], {}),
            u'ketquabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['bv.KetQuaBase']", 'unique': 'True', 'primary_key': 'True'}),
            'ma_benh': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        u'bv.ketquasieuam': {
            'Meta': {'object_name': 'KetQuaSieuAm', '_ormbases': [u'bv.KetQuaBase']},
            u'ketquabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['bv.KetQuaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'bv.ketquaxetnghiem': {
            'Meta': {'object_name': 'KetQuaXetNghiem', '_ormbases': [u'bv.KetQuaBase']},
            'huyet_hoc': ('jsonfield.fields.JSONField', [], {'blank': 'True'}),
            u'ketquabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['bv.KetQuaBase']", 'unique': 'True', 'primary_key': 'True'}),
            'nuoc_tieu': ('jsonfield.fields.JSONField', [], {'blank': 'True'}),
            'sinh_hoa': ('jsonfield.fields.JSONField', [], {'blank': 'True'})
        },
        u'bv.ketquaxquang': {
            'Meta': {'object_name': 'KetQuaXQuang', '_ormbases': [u'bv.KetQuaBase']},
            u'ketquabase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['bv.KetQuaBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'bv.khoanphi': {
            'Meta': {'object_name': 'KhoanPhi'},
            'dien_giai': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'huy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'khoan_tien': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'kich_hoat': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ma_phi': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '20'})
        },
        u'bv.lothuoc': {
            'Meta': {'object_name': 'LoThuoc'},
            'cung_cap': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'dien_giai': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ghi_chu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'het_han': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ma_phi': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'ngay_nhap': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'ngay_sx': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'nguoi_nhap': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'san_xuat': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'so_luong': ('django.db.models.fields.IntegerField', [], {})
        },
        u'bv.mautoa': {
            'Meta': {'object_name': 'MauToa'},
            'duong_dung': ('django.db.models.fields.CharField', [], {'default': "u'uong'", 'max_length': '20'}),
            'dv_dung': ('django.db.models.fields.CharField', [], {'default': "u'vien'", 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ma_thuoc': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'so_lan': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'so_luong': ('django.db.models.fields.FloatField', [], {'default': '1'}),
            'tan_suat': ('django.db.models.fields.CharField', [], {'default': "u'ngay_hai'", 'max_length': '20'}),
            'thoi_diem': ('django.db.models.fields.CharField', [], {'default': "u'sau_an'", 'max_length': '20'})
        },
        u'bv.station': {
            'Meta': {'object_name': 'Station'},
            'bat_dau': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'cuoc_hen': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.CuocHen']"}),
            'ho_ten': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ket_thuc': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'luc_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ma_bn': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'opts': ('jsonfield.fields.JSONField', [], {'default': '{}'}),
            'station': ('django.db.models.fields.CharField', [], {'default': "u'tiep_tan'", 'max_length': '20'})
        },
        u'bv.superbill': {
            'Meta': {'object_name': 'SuperBill'},
            'cuoc_hen': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.CuocHen']"}),
            'discount_base': ('django.db.models.fields.CharField', [], {'default': "u'phan_tram'", 'max_length': '20'}),
            'huy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_code': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            'item_description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'item_discount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'item_qty': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'item_unit': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'ketquabase_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'loai_phi': ('django.db.models.fields.CharField', [], {'default': "'kham_benh'", 'max_length': '20'}),
            'service_line': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'station': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'tien_trien': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.Station']", 'null': 'True', 'blank': 'True'}),
            'trang_thai': ('django.db.models.fields.CharField', [], {'default': "u'cho_duyet'", 'max_length': '20'}),
            'unit_price': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        u'bv.thuoc': {
            'Meta': {'object_name': 'Thuoc', '_ormbases': [u'bv.Billable']},
            u'billable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['bv.Billable']", 'unique': 'True', 'primary_key': 'True'}),
            'don_vi': ('django.db.models.fields.CharField', [], {'default': "u'vien'", 'max_length': '20'}),
            'dv_hl': ('django.db.models.fields.CharField', [], {'default': "u'mg'", 'max_length': '20'}),
            'gia_nhap': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'ham_luong': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'hoat_chat': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nha_cc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nha_sx': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nuoc_sx': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'ton_kho': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'bv.toathuoc': {
            'Meta': {'object_name': 'ToaThuoc'},
            'bac_sy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'cap_nhat': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'cuoc_hen': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.CuocHen']", 'null': 'True', 'blank': 'True'}),
            'ghi_chu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'ho_ten': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'huy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ma_bn': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'ngay_ghi': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trang_thai': ('django.db.models.fields.CharField', [], {'default': "u'cho_duyet'", 'max_length': '20'})
        },
        u'bv.vattu': {
            'Meta': {'object_name': 'VatTu', '_ormbases': [u'bv.Billable']},
            u'billable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['bv.Billable']", 'unique': 'True', 'primary_key': 'True'}),
            'loai_vt': ('django.db.models.fields.CharField', [], {'default': "u'kho'", 'max_length': '10'}),
            'ma_vt': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ten_vt': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'bv.xuatthuoc': {
            'Meta': {'object_name': 'XuatThuoc'},
            'don_vi': ('django.db.models.fields.CharField', [], {'default': "u'vien'", 'max_length': '20'}),
            'duong_dung': ('django.db.models.fields.CharField', [], {'default': "u'uong'", 'max_length': '20'}),
            'hd_dv_dung': ('django.db.models.fields.CharField', [], {'default': "u'vien'", 'max_length': '20', 'blank': 'True'}),
            'hd_so_lan': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'hd_so_luong': ('django.db.models.fields.FloatField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'hd_tan_suat': ('django.db.models.fields.CharField', [], {'default': "u'ngay_hai'", 'max_length': '20', 'blank': 'True'}),
            'hd_thoi_diem': ('django.db.models.fields.CharField', [], {'default': "u'sau_an'", 'max_length': '20', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ma_thuoc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'so_luong': ('django.db.models.fields.FloatField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'superbill': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['bv.SuperBill']", 'unique': 'True'}),
            'ten_thuoc': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'toa_thuoc': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['bv.ToaThuoc']"})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['bv']
