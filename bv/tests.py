"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase


class SimpleTest(TestCase):
	def test_basic_addition(self):
		"""
		Tests that 1 + 1 always equals 2.
		"""
		self.assertEqual(1 + 1, 2)


"""
from bv.models import BenhNhan
from bv.forms import BenhNhanForm
f = BenhNhanForm(instance=BenhNhan.objects.get(pk=1))
f.cleaned_data={}
f.save()

"""
