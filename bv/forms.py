# -*- coding: utf-8 -*-
from bv.models import BenhNhan, CuocHen, HoaDon, DongPhi, ChiTra, SuperBill, Station, CongViec, KetQuaBase, \
	DiUng, KetQuaXetNghiem, KetQuaSieuAm, KetQuaXQuang, KetQuaLamSang, ToaThuoc, DongToa, Thuoc, XuatThuoc, Billable
from bv.models import STATION_CLASS, STATION_CHOICES, MODALITY_LIST, MODALITY_CHOICES, PROCEDURE_CHOICES, \
	DRUGFORM_CHOICES, DISCOUNT_CHOICES, SERVICE_LINES, thanh_tien
from django import forms
from django.forms import extras, widgets
from django.forms.models import inlineformset_factory, modelformset_factory, modelform_factory
from django.forms.widgets import TextInput
from django.db.models.fields import AutoField
import autocomplete_light
import locale
from django.template.defaultfilters import mark_safe


def formfield_callback(field):
	if isinstance(field, AutoField):
		return forms.CharField(widget=forms.HiddenInput())
	else:
		return field.formfield()


def error_summary(f):
	form_set = f
	if isinstance(f, forms.Form) or isinstance(f, forms.ModelForm):
		form_set = [f]
	ret = []
	for form in form_set:
		l = [(form[k].label, ', '.join(v)) for k, v in form.errors.iteritems() if form.fields.get(k)]
		errors = ''.join(['%s: %s' % (t[0], t[1]) for t in l])
		non_field_errors = ''.join(form.non_field_errors())
		ret.append(''.join(filter(None, [errors, non_field_errors])))
	return ''.join(filter(None, ret))


class SmartFloatWidget(widgets.TextInput):
	def render(self, name, value, attrs=None):
		# Prevent the <input> to display value as 0.0
		if isinstance(value, float) and abs(value) % 1 < 0.1:
			return super(SmartFloatWidget, self).render(name, int(value), attrs)
		else:
			return super(SmartFloatWidget, self).render(name, value, attrs)

	def value_from_datadict(self, data, files, name):
		tmp = data.get(name, None)
		if tmp and tmp.strip():
			locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
			f = locale.atof(tmp)
			if isinstance(f, float) and abs(f) % 1 < 0.1:
				tmp = str(int(f))
			else:
				tmp = str(f)
		return tmp


class XuatThuocForm(forms.ModelForm):
	service_line = forms.ChoiceField(label=u'service line', choices=SERVICE_LINES)
	item_discount = forms.FloatField(label=u'giảm giá', required=False)
	discount_base = forms.ChoiceField(label=u'đv giảm', choices=DISCOUNT_CHOICES)
	unit_price = forms.FloatField(label=u'đơn giá')
	phu_thu = forms.FloatField(label=u'phụ thu')

	class Meta:
		model = XuatThuoc
		exclude = ['superbill']

	def save(self, *args, **kwargs):
		xuat_thuoc = super(XuatThuocForm, self).save(*args, **kwargs)
		# superbill = None
		is_new = False
		try:
			superbill = getattr(xuat_thuoc, 'superbill')
		except SuperBill.DoesNotExist:
			superbill = SuperBill()
			is_new = True
		superbill.item_code = self.cleaned_data['ma_thuoc']
		superbill.item_description = self.cleaned_data['ten_thuoc']
		superbill.unit_price = self.cleaned_data['unit_price']
		superbill.item_qty = self.cleaned_data['so_luong']
		superbill.item_unit = self.cleaned_data['don_vi']
		superbill.item_discount = self.cleaned_data['item_discount']
		superbill.phu_thu = self.cleaned_data['phu_thu']
		superbill.discount_base = self.cleaned_data['discount_base']
		superbill.cuoc_hen_id = xuat_thuoc.toa_thuoc.cuoc_hen_id
		superbill.service_line = self.cleaned_data['service_line']
		superbill.loai_phi = 'thuoc'
		superbill.save()
		if is_new:
			xuat_thuoc.superbill = superbill
			xuat_thuoc.save()
		return xuat_thuoc

	def __init__(self, *args, **kwargs):
		super(XuatThuocForm, self).__init__(*args, **kwargs)
		instance = kwargs.get('instance')
		self.fields['ten_thuoc'].widget = autocomplete_light.TextWidget('ThuocAutocomplete')
		self.fields['ma_thuoc'].widget.attrs['style'] = 'width:3em'
		self.fields['ten_thuoc'].widget.attrs['style'] = 'width:17em'
		self.fields['unit_price'].widget.attrs['style'] = 'width:10em'
		self.fields['item_discount'].widget.attrs['style'] = 'width:3em; text-align:right'
		self.fields['ghi_chu'].widget = autocomplete_light.TextWidget('GhiChuAutocomplete')
		self.fields['ghi_chu'].widget.attrs['style'] = 'width:10em'
		self.fields['phu_thu'].widget = SmartFloatWidget()
		self.fields['phu_thu'].widget.attrs['class'] = 'float_format'

		self.fields['sang'].widget = SmartFloatWidget()
		self.fields['sang'].widget.attrs['style'] = 'width:2em; text-align:right'
		self.fields['trua'].widget = SmartFloatWidget()
		self.fields['trua'].widget.attrs['style'] = 'width:2em; text-align:right'
		self.fields['chieu'].widget = SmartFloatWidget()
		self.fields['chieu'].widget.attrs['style'] = 'width:2em; text-align:right'
		self.fields['toi'].widget = SmartFloatWidget()
		self.fields['toi'].widget.attrs['style'] = 'width:2em; text-align:right'
		self.fields['so_luong'].widget = SmartFloatWidget()
		self.fields['so_luong'].widget.attrs['style'] = 'width:3em; text-align:right'
		self.fields['unit_price'].widget = SmartFloatWidget()
		self.fields['unit_price'].widget.attrs['class'] = 'float_format'
		self.fields['unit_price'].widget.attrs['style'] = 'width:5em;'
		thuoc = getattr(instance, 'superbill', None)
		if thuoc:
			self.fields['unit_price'].initial = thuoc.unit_price
			self.fields['item_discount'].initial = thuoc.item_discount
			self.fields['phu_thu'].initial = thuoc.phu_thu
			self.fields['discount_base'].initial = thuoc.discount_base
			self.fields['service_line'].initial = thuoc.service_line
		else:
			self.fields['unit_price'].initial = 0
			self.fields['item_discount'].initial = 0
			self.fields['phu_thu'].initial = 0
			self.fields['discount_base'].initial = 'phan_tram'
			self.fields['service_line'].initial = 'dich_vu'

class ToaThuocForm(forms.ModelForm):
	class Meta:
		model = ToaThuoc
		fields = ['bac_sy', 'ma_bn', 'ho_ten', 'cuoc_hen', 'trang_thai', 'huy', 'ghi_chu']

	def __init__(self, *args, **kwargs):
		super(ToaThuocForm, self).__init__(*args, **kwargs)


class DongToaForm(forms.ModelForm):
	class Meta:
		model = DongToa

	def __init__(self, *args, **kwargs):
		super(DongToaForm, self).__init__(*args, **kwargs)
		self.fields['ten_thuoc'].widget = autocomplete_light.TextWidget('ThuocAutocomplete')
		self.fields['ma_thuoc'].widget.attrs['style'] = 'width:3em'
		self.fields['ten_thuoc'].widget.attrs['style'] = 'width:25em'
		self.fields['so_luong'].widget.attrs['style'] = 'width:3em'
		self.fields['sang'].widget = SmartFloatWidget()
		self.fields['trua'].widget = SmartFloatWidget()
		self.fields['chieu'].widget = SmartFloatWidget()
		self.fields['toi'].widget = SmartFloatWidget()
		self.fields['so_luong'].widget = SmartFloatWidget()
		self.fields['ghi_chu'].widget = autocomplete_light.TextWidget('GhiChuAutocomplete')
		self.fields['ghi_chu'].widget.attrs['style'] = 'width:10em'
		self.fields['sang'].widget = SmartFloatWidget()
		self.fields['sang'].widget.attrs['style'] = 'width:4em; text-align:right'
		self.fields['trua'].widget = SmartFloatWidget()
		self.fields['trua'].widget.attrs['style'] = 'width:4em; text-align:right'
		self.fields['chieu'].widget = SmartFloatWidget()
		self.fields['chieu'].widget.attrs['style'] = 'width:4em; text-align:right'
		self.fields['toi'].widget = SmartFloatWidget()
		self.fields['toi'].widget.attrs['style'] = 'width:4em; text-align:right'
		self.fields['so_luong'].widget = SmartFloatWidget()
		self.fields['so_luong'].widget.attrs['style'] = 'width:5em; text-align:right'


class KhamLamSangForm(forms.ModelForm):
	class Meta:
		model = CuocHen

	def __init__(self, *args, **kwargs):
		super(KhamLamSangForm, self).__init__(*args, **kwargs)
		self.fields['ghi_chu'].widget.attrs['style'] = 'height:37px!important;'


class KetQuaLamSangForm(forms.ModelForm):
	class Meta:
		model = KetQuaLamSang
		fields = ['ma_benh', 'benh_ly', 'ghi_chu', 'huy', 'station']

	def __init__(self, *args, **kwargs):
		super(KetQuaLamSangForm, self).__init__(*args, **kwargs)
		self.fields['benh_ly'].widget = autocomplete_light.TextWidget('BenhLyAutocomplete')
		self.fields['ma_benh'].widget.attrs['style'] = 'width:7em;'
		self.fields['benh_ly'].widget.attrs['style'] = 'width:25em;'
		self.fields['ghi_chu'].widget.attrs['style'] = 'width:32em;'
		self.fields['station'].widget = forms.HiddenInput()


class SuperBillForm(forms.ModelForm):
	class Meta:
		model = SuperBill
		exclude = ['ketquabase_id', 'item_unit', 'tien_trien', 'station', 'trang_thai']

	# unit_price = forms.FloatField(localize=True, label=u'đơn giá')
	# quy_bhyt = forms.FloatField(localize=True, label=u'quỹ BHYT')

	def __init__(self, *args, **kwargs):
		super(SuperBillForm, self).__init__(*args, **kwargs)
		self.fields['item_description'].widget = autocomplete_light.TextWidget('CongViecAutocomplete')
		self.fields['item_code'].widget.attrs['style'] = 'width:3em;'
		self.fields['item_description'].widget.attrs['style'] = 'width:20em;'
		self.fields['unit_price'].widget.attrs['style'] = 'width:4em;'
		self.fields['item_qty'].widget.attrs['style'] = 'width:3em;text-align: right;'
		self.fields['item_discount'].widget.attrs['style'] = 'width:3em;text-align: right;'
		self.fields['unit_price'].widget = SmartFloatWidget()
		self.fields['unit_price'].widget.attrs['class'] = 'float_format'
		self.fields['quy_bhyt'].widget = SmartFloatWidget()
		self.fields['quy_bhyt'].widget.attrs['class'] = 'float_format'
		self.fields['phu_thu'].widget = SmartFloatWidget()
		self.fields['phu_thu'].widget.attrs['class'] = 'float_format'

class ChiDinhThuThuatForm(forms.ModelForm):
	class Meta:
		model = SuperBill
		fields = ['item_code', 'item_description', 'unit_price', 'phu_thu', 'service_line', 'station', 'trang_thai', 'loai_phi',
			'huy']

	# unit_price = forms.FloatField(localize=True,label=u'đơn giá')

	def __init__(self, *args, **kwargs):
		super(ChiDinhThuThuatForm, self).__init__(*args, **kwargs)
		self.fields['item_description'].widget = autocomplete_light.TextWidget('CongViecAutocomplete')
		self.fields['item_description'].widget.attrs['style'] = 'width:24em;'
		self.fields['item_code'].widget.attrs['style'] = 'width:5em;'
		self.fields['unit_price'].widget.attrs['style'] = 'width:5em;'
		self.fields['station'].choices = [('', '---------')]
		self.fields['station'].choices.extend(PROCEDURE_CHOICES)
		self.fields['station'].choices.extend(MODALITY_CHOICES)
		self.fields['unit_price'].widget = SmartFloatWidget()
		self.fields['unit_price'].widget.attrs['class'] = 'float_format'
		self.fields['phu_thu'].widget = SmartFloatWidget()
		self.fields['phu_thu'].widget.attrs['class'] = 'float_format'


class BaseChiDinhThuThuatFormSet(forms.models.BaseInlineFormSet):
	def get_queryset(self):
		## See get_queryset method of django.forms.models.BaseModelFormSet
		if not hasattr(self, '_queryset'):
			self._queryset = self.queryset.exclude(loai_phi='thuoc')
			if not self._queryset.ordered:
				self._queryset = self._queryset.order_by('-service_line')
		return self._queryset


class DiUngForm(forms.ModelForm):
	class Meta:
		model = DiUng
		fields = ['ma_thuoc', 'ten_thuoc', 'ghi_chu', 'huy']

	def __init__(self, *args, **kwargs):
		super(DiUngForm, self).__init__(*args, **kwargs)
		self.fields['ma_thuoc'].widget.attrs['style'] = 'width:12em;'
		self.fields['ghi_chu'].widget.attrs['style'] = 'width:40em;'
		self.fields['ten_thuoc'].widget.attrs['style'] = 'width:30em;'


class KetQuaXetNghiemForm(forms.ModelForm):
	class Meta:
		model = KetQuaXetNghiem


class KetQuaSieuAmForm(forms.ModelForm):
	class Meta:
		model = KetQuaSieuAm


class KetQuaXQuangForm(forms.ModelForm):
	class Meta:
		model = KetQuaXQuang


class NhanBenhForm(forms.Form):
	"""Nhận Bệnh form  :) """
	thong_tin = forms.CharField(label=u'mã bệnh nhân', max_length=50)
	thong_tin.widget.attrs = {'placeholder': u'Nhập Tên, STT hoặc BHYT', }


class CuocHenFullForm(forms.ModelForm):
	class Meta:
		model = CuocHen
		exclude = []

	def __init__(self, *args, **kwargs):
		super(CuocHenFullForm, self).__init__(*args, **kwargs)
		self.fields['tieu_de'].initial = u'Khám Bệnh'
		self.fields['ma_bn'].widget.attrs['readonly'] = True
		self.fields['bat_dau'].widget.input_formats = ['%d/%m/%y %H:%M']
		self.fields['bat_dau'].widget.attrs = {'placeholder': u'NN/TT/NN GG:PP', }


class CuocHenForm(forms.ModelForm):
	# chuyen_khoa = forms.ChoiceField(choices=MODALITY_CHOICES, label=u'Chuyên Khoa')
	class Meta:
		model = CuocHen
		fields = ['bat_dau', 'tieu_de', 'service_line', 'ghi_chu']

	def __init__(self, *args, **kwargs):
		super(CuocHenForm, self).__init__(*args, **kwargs)
		self.fields['tieu_de'].initial = u'Khám Bệnh'
		self.fields['bat_dau'].widget.input_formats = ['%d/%m/%y %H:%M']
		self.fields['bat_dau'].widget.attrs = {'placeholder': u'NN/TT/NN GG:PP', }


class QuyTrinhForm(forms.ModelForm):
	# hien_tai = forms.BooleanField(label=u'Hiện Tại')
	thu_tuc = forms.CharField(label=u'Thủ Tục', widget=forms.Textarea)

	class Meta:
		model = Station
		exclude = ['opts']

	def __init__(self, *args, **kwargs):
		super(QuyTrinhForm, self).__init__(*args, **kwargs)
		instance = kwargs.get('instance')
		if instance:
			self.fields['thu_tuc'].initial = ' + '.join([
				s.__unicode__() for s in instance.superbill_set.all()
			])


class BenhNhanForm(forms.ModelForm):
	ghi_chu = forms.CharField(label=u'Lý do đến khám', required=False);
	class Meta:
		model = BenhNhan
		fields = ['ma_bn', 'ho_ten', 'ngay_sinh', 'gioi_tinh', 'bhyt', 'ma_kcb', 'noi_dk','sdt', 'ghi_chu', 'bhyt_start',
			'bhyt_expire', 'dia_chi', 'nam_sinh']

	def __init__(self, *args, **kwargs):
		super(BenhNhanForm, self).__init__(*args, **kwargs)
		# instance = kwargs.get('instance')
		self.fields['ma_bn'].widget.attrs['readonly'] = True
		self.fields['bhyt_expire'] = forms.DateField(input_formats=['%d/%m/%y', '%d/%m/%Y', '%Y', '%y'], required=False)
		self.fields['bhyt_expire'].widget.format = '%d/%m/%Y'
		self.fields['bhyt_start'] = forms.DateField(input_formats=['%d/%m/%y', '%d/%m/%Y', '%Y', '%y'], required=False)
		self.fields['bhyt_start'].widget.format = '%d/%m/%Y'
		self.fields['bhyt_start'].widget.attrs['placeholder'] = u'dd/mm/yy'

		self.fields['ngay_sinh'] = forms.DateField(input_formats=['%d/%m/%y', '%d/%m/%Y', '%Y', '%y'])
		self.fields['ngay_sinh'].widget.format = '%d/%m/%Y'
		self.fields['ngay_sinh'].widget.attrs['placeholder'] = u'dd/mm/yy'
		# self.fields['ngay_sinh'].widget.attrs['class'] = 'maskeddate'
		self.fields['nam_sinh'].widget = forms.HiddenInput()
		self.fields['ma_kcb'].widget = autocomplete_light.TextWidget('CoSoAutocomplete')

		self.fields['bhyt_expire'].widget.attrs['class'] = 'maskeddate'
		self.fields['bhyt_start'].widget.attrs['class'] = 'maskeddate'
		self.fields['bhyt'].widget.attrs['class'] = 'bhyt_format'
		self.fields['bhyt'].widget.attrs['placeholder'] = u'Nhap 2 ky tu va 13 chu so'
		self.fields['bhyt'].widget.attrs['maxlength'] = 30


	def clean(self):
		if self.cleaned_data['bhyt'] and len(self.cleaned_data['bhyt']) != 15:
			self._errors['bhyt'] = self.error_class(
				[u'Nhập đúng 15 ký tự (đã nhập %s).' % len(self.cleaned_data['bhyt'])])
		return self.cleaned_data


class HoaDonForm(forms.ModelForm):
	class Meta:
		model = HoaDon
		exclude = ['loai_hd']

	# tong_so = forms.FloatField(localize=True, label="Cân đối")
	def __init__(self, *args, **kwargs):
		super(HoaDonForm, self).__init__(*args, **kwargs)
		self.fields['tong_so'].widget = SmartFloatWidget()
		self.fields['tong_so'].widget.attrs['class'] = 'float_format'
		self.fields['so_dong'].widget.attrs['style'] = 'text-align: right;'


class ChiTraForm(forms.ModelForm):
	class Meta:
		model = ChiTra
		exclude = ['hoa_don']

	# khoan_tien = forms.FloatField(localize=True)
	def __init__(self, *args, **kwargs):
		super(ChiTraForm, self).__init__(*args, **kwargs)
		self.fields['ghi_chu'].widget.attrs['style'] = 'width:41em;'
		self.fields['khoan_tien'].widget.attrs['style'] = 'width:14em;'
		self.fields['khoan_tien'].widget = SmartFloatWidget()
		self.fields['khoan_tien'].widget.attrs['class'] = 'float_format'


class DongPhiForm(forms.ModelForm):
	thanh_tien = forms.FloatField(label=u'đơn giá')
	# unit_price = forms.FloatField(localize=True,label=u'đơn giá')
	class Meta:
		model = DongPhi


	def __init__(self, *args, **kwargs):
		super(DongPhiForm, self).__init__(*args, **kwargs)
		instance = kwargs.get('instance')
		self.fields['thanh_tien'].widget.attrs['readonly'] = True
		self.fields['item_code'].widget.attrs['style'] = 'width:4em;'
		self.fields['item_description'].widget.attrs['style'] = 'width:15em;'
		self.fields['item_discount'].widget.attrs['style'] = 'width:3em;'
		self.fields['unit_price'].widget.attrs['style'] = 'width:3em;'
		self.fields['item_qty'].widget.attrs['style'] = 'width:3em;'
		self.fields['thanh_tien'].widget.attrs['style'] = 'width:5em;'
		self.fields['unit_price'].widget = SmartFloatWidget()
		self.fields['unit_price'].widget.attrs['class'] = 'float_format'
		self.fields['thanh_tien'].widget = SmartFloatWidget()
		self.fields['thanh_tien'].widget.attrs['class'] = 'float_format'
		self.fields['item_qty'].widget = SmartFloatWidget()
		self.fields['item_qty'].widget.attrs['style'] = 'text-align: right;'
		self.fields['item_discount'].widget = SmartFloatWidget()
		self.fields['item_discount'].widget.attrs['style'] = 'text-align: right;'
		if instance:
			self.fields['thanh_tien'].initial = thanh_tien(
				instance.unit_price,
				instance.item_qty,
				instance.item_discount,
				instance.discount_base
			)
			if instance.superbill:
				instance.superbill.item_code = instance.item_code
				instance.superbill.item_description = instance.item_description
				instance.superbill.unit_price = instance.unit_price
				instance.superbill.item_qty = instance.item_qty
				instance.superbill.item_unit = instance.item_unit
				instance.superbill.item_discount = instance.item_discount
				instance.superbill.discount_base = instance.discount_base
				instance.superbill.save()



class BaseInlineHoaDonFormSet(forms.models.BaseInlineFormSet):
	def get_queryset(self):
		## See get_queryset method of django.forms.models.BaseModelFormSet
		if not hasattr(self, '_queryset'):
			self._queryset = self.queryset.filter(huy=False)
			if not self._queryset.ordered:
				self._queryset = self._queryset.order_by('-luc_ghi')
		return self._queryset
CHIDINH_LIST = { 
u'huyet_hoc': (u'HUYẾT HỌC',),
u'sinh_hoa': (u'SINH HÓA',),
u'mien_dich': (u'MIỄN DỊCH',),
u'tumor_markers': (u'TUMOR MARKERS',),
u'nuoc_tieu': (u'NƯỚC TIỂU',),
u'phan': (u'PHÂN',),
u'xet_nghiem_giun': (u'XÉT NGHIỆM GIUN',),
u'dam_dich': (u'ĐÀM VÀ CÁC DỊCH SINH HỌC KHÁC',),
u'dien_chuan_doan': (u'ĐIỆN CHUẨN ĐOÁN',),
u'sieu_am': (u'SIÊU ÂM',),
u'x_quang': (u'X-QUANG',),
}

HUYET_HOC_LIST = {
u'XN-ctm': (u'NGFL',),
u'XN-vs01': (u'VS 1 giờ/2 giờ',),
u'XN-sotret': (u'KSTSR',),
u'XN-mauabo': (u'nhóm máu ABO',),
u'XN-maurh': (u'nhóm máu RH',),
u'XN-proth': (u'tỉ lệ PROTHROMBINE',),
u'HH7': (u'IRN',),
u'XN-tqtck': (u'TQ-TCK-TS-TC',),
u'XN-fibr': (u'FIBRINOGENE',), 
}

class ChiDinhForm(forms.Form): 
	# loai_chi_dinh = forms.CharField(label=u'Loại chỉ định', max_length=50)
	# ten_chi_dinh = forms.CharField(label=u'Tên chỉ định', max_length=50)
	def __init__(self, *args, **kwargs):
		super(ChiDinhForm, self).__init__(*args, **kwargs)
		for v in HUYET_HOC_LIST.values():
			self.fields['%s' %(v)] = forms.BooleanField()
			print HUYET_HOC_LIST.items()

		





# Các Forms cho view thu_ngan
DongPhiFormSet = inlineformset_factory(HoaDon, DongPhi, form=DongPhiForm, extra=2)
ChiTraFormSet = inlineformset_factory(HoaDon, ChiTra, form=ChiTraForm, extra=2)

# Các Forms cho view lam_sang
ChiDinhThuThuatFormSet = inlineformset_factory(CuocHen, SuperBill, form=ChiDinhThuThuatForm,
											   formset=BaseChiDinhThuThuatFormSet, extra=2)
KetQuaLamSangFormSet = inlineformset_factory(CuocHen, KetQuaLamSang, form=KetQuaLamSangForm, extra=1)
DiUngFormSet = inlineformset_factory(CuocHen, DiUng, form=DiUngForm, extra=1)
KetQuaXetNghiemFormSet = inlineformset_factory(CuocHen, KetQuaXetNghiem, form=KetQuaXetNghiemForm, extra=0)
KetQuaXQuangFormSet = inlineformset_factory(CuocHen, KetQuaXQuang, form=KetQuaXQuangForm, extra=0)
KetQuaSieuAmFormSet = inlineformset_factory(CuocHen, KetQuaSieuAm, form=KetQuaSieuAmForm, extra=0)

# Các Forms cho view toa_thuoc
DongToaFormSet = inlineformset_factory(ToaThuoc, DongToa, form=DongToaForm, extra=10)

# Các Form cho view nha_thuoc
DongToaReadonlyFormSet = inlineformset_factory(ToaThuoc, DongToa, form=DongToaForm, extra=0)
XuatThuocFormSet = inlineformset_factory(ToaThuoc, XuatThuoc, form=XuatThuocForm, extra=2)

# Các Form cho view quy_trinh
QuyTrinhFormSet = inlineformset_factory(CuocHen, Station, form=QuyTrinhForm, extra=2)

# Các Form cho view superbill
ChiTraReadonlyFormSet = modelformset_factory(ChiTra, form=ChiTraForm, extra=0)
SuperBillFormSet = inlineformset_factory(CuocHen, SuperBill, form=SuperBillForm, extra=1)

# Các Forms cho view thu_thuat
KetQuaXQuangForm = modelform_factory(KetQuaXQuang, fields=['ghi_chu'])
KetQuaXetNghiemForm = modelform_factory(KetQuaXetNghiem, fields=['ghi_chu'])
KetQuaSieuAmForm = modelform_factory(KetQuaSieuAm, fields=['ghi_chu'])
ThuThuatFormSet = inlineformset_factory(CuocHen, SuperBill, form=ChiDinhThuThuatForm,
										formset=BaseChiDinhThuThuatFormSet, extra=0)
DiUngReadonlyFormSet = inlineformset_factory(CuocHen, DiUng, form=DiUngForm, extra=0)
