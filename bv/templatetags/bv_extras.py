from django import template
from bv.models import STATION_CHOICES 
register = template.Library()
@register.filter
def to_class_name(value):
	return value.__class__.__name__

@register.filter
def to_station_label(value):
	try:
		return filter(lambda x:x[0]==value, STATION_CHOICES)[0][1]+' - '
	except IndexError:
		return ''

@register.filter
def substract(value, arg):
	return (value - arg)