#!/usr/bin/env python
import os
import sys
from onsite import settings_xn

from django.core.management import setup_environ
setup_environ(settings_xn)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "onsite.settings_xn")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
