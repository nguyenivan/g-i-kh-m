Alias /static /var/www/onsite/static
Alias /media /var/www/onsite/media
Alias /phpMyAdmin /var/www/phpMyAdmin

<VirtualHost *:80>
    ServerName anphu
    ErrorLog "/var/log/apache2/anphu.error.log"

    <Directory /var/www/onsite>
        AllowOverride All
        Options Indexes FollowSymLinks
        Order allow,deny
        Allow from all
    </Directory>

    WSGIDaemonProcess anphu processes=1 threads=1 maximum-requests=1  python-path=/home/nguyen/Env/production/local/lib/python2.7/site-packages/
    WSGIProcessGroup anphu
    WSGIScriptAlias / "/var/www/onsite/onsite/wsgi.py"
</VirtualHost>